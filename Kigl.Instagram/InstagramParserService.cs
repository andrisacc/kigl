﻿using System.Linq;
using System.Net;
using Kigl.Common.Helpers;
using Kigl.Domain.Contracts;
using Kigl.Domain.Models.Instaram;
using Kigl.Instagram.Models;
using Newtonsoft.Json;
using Common.Logging;

namespace Kigl.Instagram
{
    public class InstagramParserService : IInstagramParserService
    {
        private static readonly ILog Log = LogManager.GetLogger<InstagramParserService>();

        public FollowersInfo GetFollowers(string userId)
        {
            var reply = GetUserInfo(userId);
            if (!string.IsNullOrWhiteSpace(reply))
            {
                var json = ExtractJson(reply);
                var user = json.EntryData.ProfilePage[0].graphql.User;
                //if (userId == "ivanovovi")
                //{
                //    return new FollowersInfo
                //    {
                //        Follows = 10,
                //        FollowedBy = 10,
                //        IsLastFollowersRequestSuccess = true
                //    };
                //}
                //else
                //{
                    return new FollowersInfo
                    {
                        Follows = user.EdgeFollow.Count,
                        FollowedBy = user.EdgeFollowedBy.Count,
                        IsLastFollowersRequestSuccess = true
                    };
                //}
            }
            return null;
        }
        
        public bool CheckText(string userId, string text, params string[] tags)
        {
            var reply = GetUserInfo(userId);
            if (!string.IsNullOrWhiteSpace(reply))
            {
                var json = ExtractJson(reply);
                var media = json.EntryData.ProfilePage[0].graphql.User.EdgeOwnerToTimelineMedia;

                foreach(var edge in media.Edges)
                {
                    if(edge != null && edge.Node != null && edge.Node.EdgeMediaToCaption != null)
                    {
                        foreach(var textNodeWrapper in edge.Node.EdgeMediaToCaption.Edges)
                        {
                            if(textNodeWrapper != null && textNodeWrapper.Node != null && !string.IsNullOrEmpty(textNodeWrapper.Node.Text))
                            {
                                var caption = textNodeWrapper.Node.Text.ToLower();

                                if(caption.Contains(text.Trim().ToLower()))
                                {
                                    return true;
                                }
                            }
                        }
                    }
                }

                return false;

                //return media.Edges.Any(item => !string.IsNullOrWhiteSpace(item.Node.EdgeMediaToCaption.Edges.FirstOrDefault()!=null? item.Node.EdgeMediaToCaption.Edges.FirstOrDefault().Node.Text:"")
                //    && item.Node.Text.ToLower().Contains(text.Trim().ToLower())
                //    && tags.All(tag => item.Node.Text.Contains(tag)));
            }
            return false;
        }

        private string GetUserInfo(string userId)
        {
            using (var client = new WebClient())
            {
                try
                {
                    var result = client.DownloadString($"https://www.instagram.com/{userId}/");
                    Log.Error($"Followers info has been pulled successfully for user {userId}");
                    return result;
                }
                catch (WebException ex)
                {
                    Log.Error($"Exception during getting followers info for {userId}: {ex.ToString()}");
                    if (ex.Status == WebExceptionStatus.ProtocolError && ex.Response != null)
                    {
                        var resp = (HttpWebResponse)ex.Response;
                        if (resp.StatusCode == HttpStatusCode.NotFound)
                        {
                            return null;
                        }
                    }
                    throw;
                }
            }
        }

        private Json ExtractJson(string reply)
        {
            var script = reply.Substring(reply.IndexOf("window._sharedData"));
            script = script.Substring(0, script.IndexOf("</script>"));
            var jsonStart = script.IndexOf("{");
            var jsonEnd = script.LastIndexOf("}");
            var json = script.Substring(jsonStart, jsonEnd - jsonStart + 1);
            return JsonConvert.DeserializeObject<Json>(json, new JsonSerializerSettings()
            {
                ContractResolver = new UnderscorePropertyNamesContractResolver()
            });
        }
    }
}

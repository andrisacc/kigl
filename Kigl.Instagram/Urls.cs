﻿namespace Kigl.Instagram
{
    public class Urls
    {
        private static string _apiUrl = "https://api.instagram.com";

        public static string AuthUrl
        {
            get
            {
                var section = InstagramSection.GetSection();
                return $"{_apiUrl}/oauth/authorize/?client_id={section.ClientId}&redirect_uri={section.RedirectUrl}&response_type=code&scope=follower_list+public_content";
            }
        }

        internal static string AccessToken => $"{_apiUrl}/oauth/access_token";
    }
}
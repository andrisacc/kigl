﻿namespace Kigl.Instagram.Models
{
    class Node : IdEntity
    {
        //public IdEntity Owner { get; set; }
        //public string DisplaySrc { get; set; }
        //public string Caption { get; set; }
        public string Text { get; set; }
        //public bool IsVideo { get; set; }
        //public CountEntity Comments { get; set; }
        //public CountEntity Likes { get; set; }
        //public int? VideoViews { get; set; }
        
        public EdgeMediaToCaption EdgeMediaToCaption { get; set; }
    }
}

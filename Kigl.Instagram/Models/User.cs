﻿namespace Kigl.Instagram.Models
{
    class User
    {
        public CountEntity EdgeFollowedBy { get; set; }
        public CountEntity EdgeFollow { get; set; }
        public EdgeOwnerToTimelineMedia EdgeOwnerToTimelineMedia { get; set; }
    }
}

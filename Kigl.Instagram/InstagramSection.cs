﻿using System;
using System.Configuration;

namespace Kigl.Instagram
{
    public class InstagramSection : ConfigurationSection
    {
        private const string SectionName = "InstagramSettings";

        private const string ClientIdField = "ClientId";
        private const string ClientSecretField = "ClientSecret";
        private const string RedirectUrlField = "RedirectUrl";


        public static InstagramSection GetSection()
        {
            var section = ConfigurationManager.GetSection(SectionName);
            if (section == null)
            {
                throw new ArgumentException($"No section {SectionName} found in config");
            }
            return (InstagramSection)section;
        }

        [ConfigurationProperty(ClientIdField, IsRequired = true)]
        public string ClientId
        {
            get { return this[ClientIdField].ToString(); }
            set { this[ClientIdField] = value; }
        }

        [ConfigurationProperty(ClientSecretField, IsRequired = true)]
        public string ClientSecret
        {
            get { return this[ClientSecretField].ToString(); }
            set { this[ClientSecretField] = value; }
        }

        [ConfigurationProperty(RedirectUrlField, IsRequired = true)]
        public string RedirectUrl
        {
            get { return this[RedirectUrlField].ToString(); }
            set { this[RedirectUrlField] = value; }
        }
    }
}

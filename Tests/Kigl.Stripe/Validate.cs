﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Configuration;
using LocalKigl = Kigl.Stripe;
using Stripe;

namespace Tests.Kigl.Stripe
{
    [TestClass]
    public class Validate
    {
        [TestMethod]
        public void TestMethod1()
        {
            var service = new LocalKigl.StripeService();
            var json =
@"{
  ""id"": ""evt_1BLBitGvRivCc9uCxper3N2d"",
  ""object"": ""event"",
  ""api_version"": ""2017-08-15"",
  ""created"": 1509979031,
  ""data"": {
                ""object"": {
                    ""id"": ""ch_1BLBisGvRivCc9uCUdoR0CYj"",
      ""object"": ""charge"",
      ""amount"": 50000,
      ""amount_refunded"": 0,
      ""application"": null,
      ""application_fee"": null,
      ""balance_transaction"": ""txn_1BLBisGvRivCc9uC5Dyllo17"",
      ""captured"": true,
      ""created"": 1509979030,
      ""currency"": ""usd"",
      ""customer"": null,
      ""description"": ""Example charge"",
      ""destination"": null,
      ""dispute"": null,
      ""failure_code"": null,
      ""failure_message"": null,
      ""fraud_details"": {
                    },
      ""invoice"": null,
      ""livemode"": false,
      ""metadata"": {
                        ""Email"": ""email"",
        ""AdvertiserId"": ""1de61ffc-47eb-4b86-9a0f-98f164621aef""
      },
      ""on_behalf_of"": null,
      ""order"": null,
      ""outcome"": {
                        ""network_status"": ""approved_by_network"",
        ""reason"": null,
        ""risk_level"": ""normal"",
        ""seller_message"": ""Payment complete."",
        ""type"": ""authorized""
      },
      ""paid"": true,
      ""receipt_email"": null,
      ""receipt_number"": null,
      ""refunded"": false,
      ""refunds"": {
                        ""object"": ""list"",
        ""data"": [

        ],
        ""has_more"": false,
        ""total_count"": 0,
        ""url"": ""/v1/charges/ch_1BLBisGvRivCc9uCUdoR0CYj/refunds""
      },
      ""review"": null,
      ""shipping"": null,
      ""source"": {
        ""id"": ""card_1BLBirGvRivCc9uCTdDOdQ56"",
        ""object"": ""card"",
        ""address_city"": null,
        ""address_country"": null,
        ""address_line1"": null,
        ""address_line1_check"": null,
        ""address_line2"": null,
        ""address_state"": null,
        ""address_zip"": null,
        ""address_zip_check"": null,
        ""brand"": ""Visa"",
        ""country"": ""US"",
        ""customer"": null,
        ""cvc_check"": ""pass"",
        ""dynamic_last4"": null,
        ""exp_month"": 1,
        ""exp_year"": 2019,
        ""fingerprint"": ""801xNkUVt9k9vu0Q"",
        ""funding"": ""credit"",
        ""last4"": ""4242"",
        ""metadata"": {
        },
        ""name"": ""advertiser"",
        ""tokenization_method"": null
      },
      ""source_transfer"": null,
      ""statement_descriptor"": null,
      ""status"": ""succeeded"",
      ""transfer_group"": null
    }
  },
  ""livemode"": false,
  ""pending_webhooks"": 1,
  ""request"": {
    ""id"": ""req_blO3w98FPFI2Hc"",
    ""idempotency_key"": null
  },
  ""type"": ""charge.succeeded""
}";

            var signature = "t=1509979036,v1=33ed6e97a83a11f477a0a9c9edeffd41bf28659d7bcc1b74d671c54abe04cea8,v0=390ae0eb6e90b9f4c08504e8137f8cca59e2fcb0f4794f292fb1d600ee04d411";

            StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["StripeSecretKey"]);

            var result = service.Validate(json, signature);
        }
    }
}

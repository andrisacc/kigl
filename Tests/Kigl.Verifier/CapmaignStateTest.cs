﻿using System;
using System.Data.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using Common.DAL.Contracts;
using Common.DAL;
using Common.DAL.Entities;
using KiglCommon = Kigl.Common;
using Kigl.Domain.Contracts;
using Kigl.Verifier;
using ControlPanel.Domain.Contracts;
using ControlPanel.Services;

namespace Tests.Kigl.Verifier
{
    [TestClass]
    public class CapmaignStateTest
    {
        static DataContext _dataContext;

        [ClassInitialize]
        public static void DbInitialize(TestContext ctx)
        {
            _dataContext = new DataContext();
            ((DbContext)_dataContext).Database.Delete();
            ((DbContext)_dataContext).Database.Create();
        }

        Advertiser advertiser;
        Account account;

        InstagramVerifier instagramVerifier;

        IAccountsService accountService;

        [TestInitialize]
        public void Initiaslise()
        {
            advertiser = new Advertiser()
            {
                Id = Guid.NewGuid(),
                Email = "invitingAdvertiser@test.ru",
                Name = "invitingAdvertiser"
            };

            account = new Account()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = advertiser.Id,
                Balance = 100,
                CurrencyId = new Guid("4a215876-053d-41f3-b282-42b325ee5740"),
            };

            _dataContext.Advertisers.Add(advertiser);
            _dataContext.Accounts.Add(account);
            _dataContext.SaveChanges();

            instagramVerifier = new InstagramVerifier();
            accountService = new AccountsService(_dataContext);
        }

        [TestMethod]
        public void StartDateIsEmptyAndEndDateIsEmpty()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = advertiser.Id,
                Budget = 5,
                IsActive = true,
                IsNew = false,
                State = ControlPanel.Common.Enums.AdState.Activated
            };
            
            _dataContext.Ads.Add(ad);
            _dataContext.SaveChanges();

            //Act
            instagramVerifier.CheckCampaings(_dataContext, accountService);
            _dataContext.SaveChanges();

            //Assert
            var adResult = _dataContext.Ads.FirstOrDefault();

            Assert.AreNotEqual(null, adResult);
            Assert.AreEqual(true, adResult.IsActive);
            Assert.AreEqual(false, adResult.IsNew);
            Assert.AreEqual(5, adResult.Budget);
            Assert.AreEqual(ControlPanel.Common.Enums.AdState.Activated, adResult.State);

            var accountResult = _dataContext.Accounts.FirstOrDefault();
            Assert.AreEqual(100, accountResult.Balance);
        }

        [TestMethod]
        public void StartDateIsNotEmptyInFutureAndEndDateIsEmpty()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = advertiser.Id,
                Budget = 5,
                IsActive = false,
                IsNew = false,
                State = ControlPanel.Common.Enums.AdState.Activated,
                StartDate = DateTime.Now.AddDays(1)
            };

            _dataContext.Ads.Add(ad);
            _dataContext.SaveChanges();

            //Act
            instagramVerifier.CheckCampaings(_dataContext, accountService);
            _dataContext.SaveChanges();

            //Assert
            var adResult = _dataContext.Ads.FirstOrDefault();

            Assert.AreNotEqual(null, adResult);
            Assert.AreEqual(false, adResult.IsActive);
            Assert.AreEqual(false, adResult.IsNew);
            Assert.AreEqual(5, adResult.Budget);
            Assert.AreEqual(ControlPanel.Common.Enums.AdState.Activated, adResult.State);

            var accountResult = _dataContext.Accounts.FirstOrDefault();
            Assert.AreEqual(100, accountResult.Balance);
        }

        [TestMethod]
        public void StartDateIsNotEmptyInPastAndEndDateIsEmpty()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = advertiser.Id,
                Budget = 5,
                IsActive = false,
                IsNew = false,
                State = ControlPanel.Common.Enums.AdState.Activated,
                StartDate = DateTime.Now.AddDays(-1)
            };

            _dataContext.Ads.Add(ad);
            _dataContext.SaveChanges();

            //Act
            instagramVerifier.CheckCampaings(_dataContext, accountService);
            _dataContext.SaveChanges();

            //Assert
            var adResult = _dataContext.Ads.FirstOrDefault();

            Assert.AreNotEqual(null, adResult);
            Assert.AreEqual(true, adResult.IsActive);
            Assert.AreEqual(false, adResult.IsNew);
            Assert.AreEqual(5, adResult.Budget);
            Assert.AreEqual(ControlPanel.Common.Enums.AdState.Activated, adResult.State);

            var accountResult = _dataContext.Accounts.FirstOrDefault();
            Assert.AreEqual(100, accountResult.Balance);
        }

        [TestMethod]
        public void StartDateIsNotEmptyInPastAndEndDateIsEmptyAndAdStopped()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = advertiser.Id,
                Budget = 5,
                IsActive = false,
                IsNew = false,
                State = ControlPanel.Common.Enums.AdState.Stopped,
                StartDate = DateTime.Now.AddDays(-1)
            };

            _dataContext.Ads.Add(ad);
            _dataContext.SaveChanges();

            //Act
            instagramVerifier.CheckCampaings(_dataContext, accountService);
            _dataContext.SaveChanges();

            //Assert
            var adResult = _dataContext.Ads.FirstOrDefault();

            Assert.AreNotEqual(null, adResult);
            Assert.AreEqual(false, adResult.IsActive);
            Assert.AreEqual(false, adResult.IsNew);
            Assert.AreEqual(5, adResult.Budget);
            Assert.AreEqual(ControlPanel.Common.Enums.AdState.Stopped, adResult.State);

            var accountResult = _dataContext.Accounts.FirstOrDefault();
            Assert.AreEqual(100, accountResult.Balance);
        }

        [TestMethod]
        public void StartDateIsNotEmptyInPastAndEndDateIsNotEmptyInPastAndAdStopped()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = advertiser.Id,
                Budget = 5,
                IsActive = false,
                IsNew = false,
                State = ControlPanel.Common.Enums.AdState.Stopped,
                StartDate = DateTime.Now.AddDays(-3),
                EndDate = DateTime.Now.AddDays(-2)
            };

            _dataContext.Ads.Add(ad);
            _dataContext.SaveChanges();

            //Act
            instagramVerifier.CheckCampaings(_dataContext, accountService);
            _dataContext.SaveChanges();

            //Assert
            var adResult = _dataContext.Ads.FirstOrDefault();

            Assert.AreNotEqual(null, adResult);
            Assert.AreEqual(false, adResult.IsActive);
            Assert.AreEqual(false, adResult.IsNew);
            Assert.AreEqual(0, adResult.Budget);
            Assert.AreEqual(ControlPanel.Common.Enums.AdState.Stopped, adResult.State);

            var accountResult = _dataContext.Accounts.FirstOrDefault();
            Assert.AreEqual(105, accountResult.Balance);
        }

        [TestMethod]
        public void StartDateIsNotEmptyInPastAndEndDateIsNotEmptyInPast()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = advertiser.Id,
                Budget = 5,
                IsActive = true,
                IsNew = false,
                State = ControlPanel.Common.Enums.AdState.Activated,
                StartDate = DateTime.Now.AddDays(-3),
                EndDate = DateTime.Now.AddDays(-2)
            };

            _dataContext.Ads.Add(ad);
            _dataContext.SaveChanges();

            //Act
            instagramVerifier.CheckCampaings(_dataContext, accountService);
            _dataContext.SaveChanges();

            //Assert
            var adResult = _dataContext.Ads.FirstOrDefault();

            Assert.AreNotEqual(null, adResult);
            Assert.AreEqual(false, adResult.IsActive);
            Assert.AreEqual(false, adResult.IsNew);
            Assert.AreEqual(0, adResult.Budget);
            Assert.AreEqual(ControlPanel.Common.Enums.AdState.Activated, adResult.State);

            var accountResult = _dataContext.Accounts.FirstOrDefault();
            Assert.AreEqual(105, accountResult.Balance);
        }


        [TestMethod]
        public void StartDateIsEmptyInPastAndEndDateIsNotEmptyInPastAndAdStopped()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = advertiser.Id,
                Budget = 5,
                IsActive = false,
                IsNew = false,
                State = ControlPanel.Common.Enums.AdState.Stopped,
                EndDate = DateTime.Now.AddDays(-1)
            };

            _dataContext.Ads.Add(ad);
            _dataContext.SaveChanges();

            //Act
            instagramVerifier.CheckCampaings(_dataContext, accountService);
            _dataContext.SaveChanges();

            //Assert
            var adResult = _dataContext.Ads.FirstOrDefault();

            Assert.AreNotEqual(null, adResult);
            Assert.AreEqual(false, adResult.IsActive);
            Assert.AreEqual(false, adResult.IsNew);
            Assert.AreEqual(0, adResult.Budget);
            Assert.AreEqual(ControlPanel.Common.Enums.AdState.Stopped, adResult.State);

            var accountResult = _dataContext.Accounts.FirstOrDefault();
            Assert.AreEqual(105, accountResult.Balance);
        }

        [TestMethod]
        public void StartDateIsEmptyInPastAndEndDateIsNotEmptyInPast()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = advertiser.Id,
                Budget = 5,
                IsActive = true,
                IsNew = false,
                State = ControlPanel.Common.Enums.AdState.Activated,
                EndDate = DateTime.Now.AddDays(-1)
            };

            _dataContext.Ads.Add(ad);
            _dataContext.SaveChanges();

            //Act
            instagramVerifier.CheckCampaings(_dataContext, accountService);
            _dataContext.SaveChanges();

            //Assert
            var adResult = _dataContext.Ads.FirstOrDefault();

            Assert.AreNotEqual(null, adResult);
            Assert.AreEqual(false, adResult.IsActive);
            Assert.AreEqual(false, adResult.IsNew);
            Assert.AreEqual(0, adResult.Budget);
            Assert.AreEqual(ControlPanel.Common.Enums.AdState.Activated, adResult.State);

            var accountResult = _dataContext.Accounts.FirstOrDefault();
            Assert.AreEqual(105, accountResult.Balance);
        }

        [TestMethod]
        public void StartDateIsNotEmptyInPastAndEndDateIsNotEmptyInFuture()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = advertiser.Id,
                Budget = 5,
                IsActive = false,
                IsNew = false,
                State = ControlPanel.Common.Enums.AdState.Activated,
                StartDate = DateTime.Now.AddDays(-1),
                EndDate = DateTime.Now.AddDays(1)
            };

            _dataContext.Ads.Add(ad);
            _dataContext.SaveChanges();

            //Act
            instagramVerifier.CheckCampaings(_dataContext, accountService);
            _dataContext.SaveChanges();

            //Assert
            var adResult = _dataContext.Ads.FirstOrDefault();

            Assert.AreNotEqual(null, adResult);
            Assert.AreEqual(true, adResult.IsActive);
            Assert.AreEqual(false, adResult.IsNew);
            Assert.AreEqual(5, adResult.Budget);
            Assert.AreEqual(ControlPanel.Common.Enums.AdState.Activated, adResult.State);

            var accountResult = _dataContext.Accounts.FirstOrDefault();
            Assert.AreEqual(100, accountResult.Balance);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            ((DataContext)_dataContext).Database.ExecuteSqlCommand("delete from Advertisers;");
            ((DataContext)_dataContext).Database.ExecuteSqlCommand("delete from Accounts;");
            ((DataContext)_dataContext).Database.ExecuteSqlCommand("delete from Ads;");
        }

        [ClassCleanup]
        public static void DbCleanup()
        {
            ((DbContext)_dataContext).Database.Delete();
        }
    }
}

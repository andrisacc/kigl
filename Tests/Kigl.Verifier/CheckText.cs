﻿using System;
using Kigl.Instagram;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests.Kigl.Verifier
{
    [TestClass]
    public class CheckText
    {
        [TestMethod]
        public void TestMethod1()
        {
            InstagramParserService parser = new InstagramParserService();
            var textToCheck = "special23";
            var instagramUserId = "gromfromcdrom";
            var res = parser.CheckText(instagramUserId, textToCheck);
        }

        [TestMethod]
        public void TestMethod2()
        {
            InstagramParserService parser = new InstagramParserService();
            var instagramUserId = "gromfromcdrom";
            var res = parser.GetFollowers(instagramUserId);
        }
    }
}

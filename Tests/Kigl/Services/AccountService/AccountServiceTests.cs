﻿using System;
using System.Data.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using Common.DAL.Contracts;
using Common.DAL;
using Common.DAL.Entities;
using KiglCommon = Kigl.Common;
using Kigl.Domain.Contracts;
using KiglServices = Kigl.Services;
using ControlPanel.Domain.Contracts;
using ControlPanel.Services;

namespace Tests.Kigl.Services.RewardService
{
    [TestClass]
    public class AccountServiceTests
    {
        static DataContext _dataContext;

        [ClassInitialize]
        public static void DbInitialize(TestContext ctx)
        {
            _dataContext = new DataContext();
            ((DbContext)_dataContext).Database.Delete();
            ((DbContext)_dataContext).Database.Create();
        }

        Advertiser someAdvertiser;
        Advertiser invitingAdvertiser;
        Advertiser invitedAdvertiser;

        Account someAdvertiserAccount;
        Account invitingAdvertiserAccount;
        Account invitedAdvertiserAccount;

        User someInstagramer;
        User invitingInstagramer;
        User invitedInstagramer;

        User someAdvertiserUser;
        User invitingAdvertiserUser;
        User invitedAdvertiserUser;

        IAccountsService accountService;

        [TestInitialize]
        public void Initiaslise()
        {
            someAdvertiser = new Advertiser()
            {
                Id = Guid.NewGuid(),
                Email = "someAdvertiser@test.ru",
                Name = "someAdvertiser"
            };

            invitingAdvertiser = new Advertiser()
            {
                Id = Guid.NewGuid(),
                Email = "invitingAdvertiser@test.ru",
                Name = "invitingAdvertiser"
            };

            invitedAdvertiser = new Advertiser()
            {
                Id = Guid.NewGuid(),
                Email = "invitedAdvertiser@test.ru",
                Name = "invitedAdvertiser"
            };

            someAdvertiserAccount = new Account()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = someAdvertiser.Id,
                Balance = 0,
                CurrencyId = new Guid("4a215876-053d-41f3-b282-42b325ee5740"),
            };

            invitingAdvertiserAccount = new Account()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = invitingAdvertiser.Id,
                Balance = 1,
                CurrencyId = new Guid("4a215876-053d-41f3-b282-42b325ee5740"),
            };

            invitedAdvertiserAccount = new Account()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = invitedAdvertiser.Id,
                Balance = 0,
                CurrencyId = new Guid("4a215876-053d-41f3-b282-42b325ee5740"),
            };

            someAdvertiserUser = new User()
            {
                Id = Guid.NewGuid(),
                Name = "someInstagramer",
                ReferralCode = "1111111111",
                AdvertiserId = someAdvertiser.Id
            };

            invitingAdvertiserUser = new User()
            {
                Id = Guid.NewGuid(),
                Name = "invitingInstagramer",
                ReferralCode = "2222222222",
                AdvertiserId = invitingAdvertiser.Id
            };

            invitedAdvertiserUser = new User()
            {
                Id = Guid.NewGuid(),
                Name = "invitedInstagramer",
                ReferralCode = "3333333333",
                AdvertiserId = invitedAdvertiser.Id
            };

            someInstagramer = new User()
            {
                Id = Guid.NewGuid(),
                InstagramLogin = "ivanovovi",
                Name = "someInstagramer",
                ReferralCode = "1111111111",
                Email = "syevtushik@yandex.ru"
            };

            invitingInstagramer = new User()
            {
                Id = Guid.NewGuid(),
                InstagramLogin = "ivanovovi",
                Name = "invitingInstagramer", 
                ReferralCode = "2222222222",
                Email = "syevtushik@yandex.ru"
            };

            invitedInstagramer = new User()
            {
                Id = Guid.NewGuid(),
                InstagramLogin = "ivanovovi",
                Name = "invitedInstagramer",
                ReferralCode = "3333333333",
                Email = "syevtushik@yandex.ru"
            };

            _dataContext.Advertisers.Add(someAdvertiser);
            _dataContext.Advertisers.Add(invitingAdvertiser);
            _dataContext.Advertisers.Add(invitedAdvertiser);
            _dataContext.Accounts.Add(someAdvertiserAccount);
            _dataContext.Accounts.Add(invitingAdvertiserAccount);
            _dataContext.Accounts.Add(invitedAdvertiserAccount);
            _dataContext.Users.Add(someInstagramer);
            _dataContext.Users.Add(invitingInstagramer);
            _dataContext.Users.Add(invitedInstagramer);
            _dataContext.Users.Add(someAdvertiserUser);
            _dataContext.Users.Add(invitingAdvertiserUser);
            _dataContext.Users.Add(invitedAdvertiserUser);
            _dataContext.SaveChanges();

            accountService = new AccountsService(_dataContext);
        }

        [TestMethod]
        public void AllInstagrammersForNewActiveAdEmailing()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = someAdvertiser.Id,
                Budget = 5,
                IsActive = true,
                IsNew = false,
                StartDate = DateTime.Now.AddDays(-1)
            };

            _dataContext.Ads.Add(ad);
            _dataContext.SaveChanges();

            //Act
            var users = accountService.GetEmailsForNewAdEmailSending();

            //Assert

            Assert.AreEqual(3, users.Count());
        }

        [TestMethod]
        public void InstagrammersForNewActiveAdEmailingOneOfThemAlreadySent()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = someAdvertiser.Id,
                Budget = 5,
                IsActive = true,
                IsNew = false,
                StartDate = DateTime.Now.AddDays(-1)
            };

            SentEmail se = new SentEmail()
            {
                AdId = ad.Id,
                Id = Guid.NewGuid(),
                IsSuccess = true,
                Timestamp = DateTime.Now,
                UserId = someInstagramer.Id
            };

            _dataContext.Ads.Add(ad);
            _dataContext.SentEmails.Add(se);
            _dataContext.SaveChanges();

            //Act
            var users = accountService.GetEmailsForNewAdEmailSending();

            //Assert

            Assert.AreEqual(2, users.Count());
        }

        [TestCleanup]
        public void TestCleanup()
        {
            ((DataContext)_dataContext).Database.ExecuteSqlCommand("delete from AdTransactions;");
            ((DataContext)_dataContext).Database.ExecuteSqlCommand("delete from ReferralRewards;");
            ((DataContext)_dataContext).Database.ExecuteSqlCommand("delete from Ads;");
        }

        [ClassCleanup]
        public static void DbCleanup()
        {
            ((DbContext)_dataContext).Database.Delete();
        }
    }
}

﻿using System;
using System.Data.Entity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using Common.DAL.Contracts;
using Common.DAL;
using Common.DAL.Entities;
using KiglCommon = Kigl.Common;
using Kigl.Domain.Contracts;
using KiglServices = Kigl.Services;

namespace Tests.Kigl.Services.RewardService
{
    [TestClass]
    public class RewardServiceTests
    {
        static DataContext _dataContext;

        [ClassInitialize]
        public static void DbInitialize(TestContext ctx)
        {
            _dataContext = new DataContext();
            ((DbContext)_dataContext).Database.Delete();
            ((DbContext)_dataContext).Database.Create();
        }

        Advertiser someAdvertiser;
        Advertiser invitingAdvertiser;
        Advertiser invitedAdvertiser;

        Account someAdvertiserAccount;
        Account invitingAdvertiserAccount;
        Account invitedAdvertiserAccount;

        User someInstagramer;
        User invitingInstagramer;
        User invitedInstagramer;

        User someAdvertiserUser;
        User invitingAdvertiserUser;
        User invitedAdvertiserUser;

        IRewardService rewardService;

        [TestInitialize]
        public void Initiaslise()
        {
            someAdvertiser = new Advertiser()
            {
                Id = Guid.NewGuid(),
                Email = "someAdvertiser@test.ru",
                Name = "someAdvertiser"
            };

            invitingAdvertiser = new Advertiser()
            {
                Id = Guid.NewGuid(),
                Email = "invitingAdvertiser@test.ru",
                Name = "invitingAdvertiser"
            };

            invitedAdvertiser = new Advertiser()
            {
                Id = Guid.NewGuid(),
                Email = "invitedAdvertiser@test.ru",
                Name = "invitedAdvertiser"
            };

            someAdvertiserAccount = new Account()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = someAdvertiser.Id,
                Balance = 0,
                CurrencyId = new Guid("4a215876-053d-41f3-b282-42b325ee5740"),
            };

            invitingAdvertiserAccount = new Account()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = invitingAdvertiser.Id,
                Balance = 1,
                CurrencyId = new Guid("4a215876-053d-41f3-b282-42b325ee5740"),
            };

            invitedAdvertiserAccount = new Account()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = invitedAdvertiser.Id,
                Balance = 0,
                CurrencyId = new Guid("4a215876-053d-41f3-b282-42b325ee5740"),
            };

            someAdvertiserUser = new User()
            {
                Id = Guid.NewGuid(),
                Name = "someInstagramer",
                ReferralCode = "1111111111",
                AdvertiserId = someAdvertiser.Id
            };

            invitingAdvertiserUser = new User()
            {
                Id = Guid.NewGuid(),
                Name = "invitingInstagramer",
                ReferralCode = "2222222222",
                AdvertiserId = invitingAdvertiser.Id
            };

            invitedAdvertiserUser = new User()
            {
                Id = Guid.NewGuid(),
                Name = "invitedInstagramer",
                ReferralCode = "3333333333",
                AdvertiserId = invitedAdvertiser.Id
            };

            someInstagramer = new User()
            {
                Id = Guid.NewGuid(),
                InstagramLogin = "ivanovovi",
                Name = "someInstagramer",
                ReferralCode = "1111111111"
            };

            invitingInstagramer = new User()
            {
                Id = Guid.NewGuid(),
                InstagramLogin = "ivanovovi",
                Name = "invitingInstagramer", 
                ReferralCode = "2222222222"
            };

            invitedInstagramer = new User()
            {
                Id = Guid.NewGuid(),
                InstagramLogin = "ivanovovi",
                Name = "invitedInstagramer",
                ReferralCode = "3333333333"
            };

            _dataContext.Advertisers.Add(someAdvertiser);
            _dataContext.Advertisers.Add(invitingAdvertiser);
            _dataContext.Advertisers.Add(invitedAdvertiser);
            _dataContext.Accounts.Add(someAdvertiserAccount);
            _dataContext.Accounts.Add(invitingAdvertiserAccount);
            _dataContext.Accounts.Add(invitedAdvertiserAccount);
            _dataContext.Users.Add(someInstagramer);
            _dataContext.Users.Add(invitingInstagramer);
            _dataContext.Users.Add(invitedInstagramer);
            _dataContext.Users.Add(someAdvertiserUser);
            _dataContext.Users.Add(invitingAdvertiserUser);
            _dataContext.Users.Add(invitedAdvertiserUser);
            _dataContext.SaveChanges();

            rewardService = new KiglServices.RewardService(_dataContext);
        }

        [TestMethod]
        public void InstagramerRewardFromInvitedInstagramer()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = someAdvertiser.Id,
                Budget = 5,
                IsActive = true,
                IsNew = false,
                StartDate = DateTime.Now.AddDays(-1)
            };

            UserAd userAd = new UserAd()
            {
                Id = Guid.NewGuid(),
                AdId = ad.Id,
                VerifyStatus = KiglCommon.Enums.VerifyStatus.Success,
                AdvertiserAmount = 5,
                AdvertiserPercent = 1,
                UserAmount = 4,
                UserPercent = 0.8m,
                VerifyDate = DateTime.Now.AddHours(-6),
                UserId = invitedInstagramer.Id,
            };

            ReferralUser rf = new ReferralUser()
            {
                Id = Guid.NewGuid(),
                UserId = invitingInstagramer.Id,
                InvitedUserId = invitedInstagramer.Id
            };

            _dataContext.Ads.Add(ad);
            _dataContext.UserAds.Add(userAd);
            _dataContext.ReferralUsers.Add(rf);
            _dataContext.SaveChanges();

            //Act
            rewardService.PayReward(userAd.Id, 0.04m, 0.04m);
            _dataContext.SaveChanges();

            //Assert
            var transactions = _dataContext.AdTransactions.ToArray();
            var rewards = _dataContext.ReferralRewards.ToArray();

            Assert.AreEqual(1, transactions.Count());
            Assert.AreEqual(1, rewards.Count());

            var transaction = _dataContext.AdTransactions.FirstOrDefault();
            var reward = _dataContext.ReferralRewards.FirstOrDefault();

            //relationship
            Assert.AreEqual(transaction.Id, reward.TransactionId);
            Assert.AreEqual(invitingInstagramer.Id, reward.InvitingUserId);
            Assert.AreEqual(userAd.Id, reward.UserAdId);

            //amounts
            Assert.AreEqual(0.16m, reward.Reward);
            Assert.AreEqual(0.84m, transaction.KiglAmount);
            Assert.AreEqual(4m, transaction.UserAmount);
            Assert.AreEqual(5m, transaction.AdvertiserAmount);
        }

        [TestMethod]
        public void InstagramerRewardFromInvitedAdvertiser()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = invitedAdvertiser.Id,
                Budget = 5,
                IsActive = true,
                IsNew = false,
                StartDate = DateTime.Now.AddDays(-1)
            };

            UserAd userAd = new UserAd()
            {
                Id = Guid.NewGuid(),
                AdId = ad.Id,
                VerifyStatus = KiglCommon.Enums.VerifyStatus.Success,
                AdvertiserAmount = 5,
                AdvertiserPercent = 1,
                UserAmount = 4,
                UserPercent = 0.8m,
                VerifyDate = DateTime.Now.AddHours(-6),
                UserId = someInstagramer.Id,
            };

            ReferralUser rf = new ReferralUser()
            {
                Id = Guid.NewGuid(),
                UserId = invitingInstagramer.Id,
                InvitedUserId = invitedAdvertiserUser.Id
            };

            _dataContext.Ads.Add(ad);
            _dataContext.UserAds.Add(userAd);
            _dataContext.ReferralUsers.Add(rf);
            _dataContext.SaveChanges();

            //Act
            rewardService.PayReward(userAd.Id, 0.04m, 0.04m);
            _dataContext.SaveChanges();

            //Assert
            var transactions = _dataContext.AdTransactions.ToArray();
            var rewards = _dataContext.ReferralRewards.ToArray();

            Assert.AreEqual(1, transactions.Count());
            Assert.AreEqual(1, rewards.Count());

            var transaction = _dataContext.AdTransactions.FirstOrDefault();
            var reward = _dataContext.ReferralRewards.FirstOrDefault();

            //relationship
            Assert.AreEqual(transaction.Id, reward.TransactionId);
            Assert.AreEqual(invitingInstagramer.Id, reward.InvitingUserId);
            Assert.AreEqual(userAd.Id, reward.UserAdId);

            //amounts
            Assert.AreEqual(0.2m, reward.Reward);
            Assert.AreEqual(0.8m, transaction.KiglAmount);
            Assert.AreEqual(4m, transaction.UserAmount);
            Assert.AreEqual(5m, transaction.AdvertiserAmount);
        }

        [TestMethod]
        public void AdvertiserRewardFromInvitedAdvertiser()
        {
            //Arrange
            Ad ad = new Ad()
            {
                Id = Guid.NewGuid(),
                AdvertiserId = invitedAdvertiser.Id,
                Budget = 5,
                IsActive = true,
                IsNew = false,
                StartDate = DateTime.Now.AddDays(-1)
            };

            UserAd userAd = new UserAd()
            {
                Id = Guid.NewGuid(),
                AdId = ad.Id,
                VerifyStatus = KiglCommon.Enums.VerifyStatus.Success,
                AdvertiserAmount = 5,
                AdvertiserPercent = 1,
                UserAmount = 4,
                UserPercent = 0.8m,
                VerifyDate = DateTime.Now.AddHours(-6),
                UserId = someInstagramer.Id,
            };

            ReferralUser rf = new ReferralUser()
            {
                Id = Guid.NewGuid(),
                UserId = invitingAdvertiserUser.Id,
                InvitedUserId = invitedAdvertiserUser.Id
            };

            _dataContext.Ads.Add(ad);
            _dataContext.UserAds.Add(userAd);
            _dataContext.ReferralUsers.Add(rf);
            _dataContext.SaveChanges();

            //Act
            rewardService.PayReward(userAd.Id, 0.04m, 0.04m);
            _dataContext.SaveChanges();

            //Assert
            var transactions = _dataContext.AdTransactions.ToArray();
            var rewards = _dataContext.ReferralRewards.ToArray();
            var beneficiaryAccount = _dataContext.Accounts.Where(a=>a.AdvertiserId== invitingAdvertiser.Id);

            Assert.AreEqual(1, transactions.Count());
            Assert.AreEqual(1, rewards.Count());
            Assert.AreEqual(1, beneficiaryAccount.Count());

            var transaction = _dataContext.AdTransactions.FirstOrDefault();
            var reward = _dataContext.ReferralRewards.FirstOrDefault();

            //relationship
            Assert.AreEqual(transaction.Id, reward.TransactionId);
            Assert.AreEqual(invitingAdvertiserUser.Id, reward.InvitingUserId);
            Assert.AreEqual(userAd.Id, reward.UserAdId);

            //amounts
            Assert.AreEqual(0.2m, reward.Reward);
            Assert.AreEqual(0.8m, transaction.KiglAmount);
            Assert.AreEqual(4m, transaction.UserAmount);
            Assert.AreEqual(5m, transaction.AdvertiserAmount);

            //account balance
            Assert.AreEqual(1.2m, beneficiaryAccount.FirstOrDefault().Balance);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            ((DataContext)_dataContext).Database.ExecuteSqlCommand("delete from AdTransactions;");
            ((DataContext)_dataContext).Database.ExecuteSqlCommand("delete from ReferralRewards;");
        }

        [ClassCleanup]
        public static void DbCleanup()
        {
            ((DbContext)_dataContext).Database.Delete();
        }
    }
}

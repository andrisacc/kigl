﻿using System;
using System.Collections.Generic;
using Kigl;
using Common.DAL.Entities;
using Kigl.Domain.Contracts;
using Kigl.Controllers;
using Kigl.Models;
using Moq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using System.Security.Principal;

namespace Tests.Kigl.Content
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var userId = Guid.NewGuid();
            var caption = "sdfsdfsdfsd    sdfsdfsdfds";

            Mock<IUserService> userService = new Mock<IUserService>();
            userService.Setup(us => us.GetById(It.IsAny<string>())).Returns(new User() {
                Id = userId,
                InstagramCategories = new List<UserInstagramCategory>()
            });

            Mock<IContentService> contentService = new Mock<IContentService>();
            contentService.Setup(cs => cs.GetUserAds(It.IsAny<Guid>())).Returns(new Ad[] {
                new Ad() {
                    Caption = caption,
                    Budget = 100,
                    UserAds = new UserAd[] {
                        new UserAd() {
                            UserId = userId
                        }
                    },
                    Files = new List<File>()
                }
            });
                
            Mock<IInstagramParserService> instagramParserService = new Mock<IInstagramParserService>();
            Mock<IEmailService> emailService = new Mock<IEmailService>();

            var fakeHttpContext = new Mock<HttpContextBase>();
            var fakeIdentity = new GenericIdentity("User");
            var principal = new GenericPrincipal(fakeIdentity, null);

            fakeHttpContext.Setup(t => t.User).Returns(principal);
            var controllerContext = new Mock<ControllerContext>();
            controllerContext.Setup(t => t.HttpContext).Returns(fakeHttpContext.Object);

            var controller = new ContentController(userService.Object, contentService.Object, instagramParserService.Object, emailService.Object);
            controller.ControllerContext = controllerContext.Object;

            var result = (ViewResult )controller.Choose(false, null);

            Assert.AreEqual(caption, ((ChooseAdModel)result.Model).UserAds[0].Caption);
        }
    }
}

﻿using System.Collections.Generic;
using Amazon;
using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Model;

namespace Kigl.Amazon
{
    public class AmazonClient
    {
        private readonly IAmazonS3 _s3Client;

        public AmazonClient()
        {
            var credentials = new BasicAWSCredentials("AKIAIT5B2FF34PYHDXCA", "UuY0ntYsSOBlC4SRXkVjZh6QuebR4I1R4bbJusCL");
            _s3Client = new AmazonS3Client(credentials, RegionEndpoint.USWest2);
        }

        public void Test()
        {
            var a = ListObjects("kigl");
        }

        public ListObjectsResponse ListObjects(string bucketName, string prefix = "", string marker = "")
        {
            var requestListObject = new ListObjectsRequest
            {
                BucketName = bucketName
            };

            if (!string.IsNullOrWhiteSpace(prefix))
            {
                requestListObject.Prefix = prefix;
            }

            if (!string.IsNullOrWhiteSpace(marker))
            {
                requestListObject.Marker = marker;
            }

            return _s3Client.ListObjects(requestListObject);
        }

        public List<S3Object> GetObjects(string bucketName, string prefix = "")
        {
            var objects = new List<S3Object>();
            ListObjectsResponse listObjects;
            string marker = "";

            do
            {
                listObjects = ListObjects(bucketName, prefix, marker);
                objects.AddRange(listObjects.S3Objects);

                marker = listObjects.NextMarker;
            }
            while (listObjects.IsTruncated);

            return objects;
        }

        public PutObjectResponse PutObject(string bucketName, string keyName, string file, bool isPath = false)
        {
            var request = new PutObjectRequest
            {
                BucketName = bucketName,
                Key = keyName,
            };

            if (isPath)
            {
                request.FilePath = file;
            }
            else
            {
                request.ContentBody = file;
            }

            return _s3Client.PutObject(request);
        }

        public PutObjectResponse PutObject(string bucketName, string keyName, System.IO.Stream stream)
        {
            var request = new PutObjectRequest
            {
                BucketName = bucketName,
                Key = keyName,
                InputStream = stream
            };

            return _s3Client.PutObject(request);
        }
    }
}

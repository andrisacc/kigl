﻿using System;
using Common.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kigl.Domain.Contracts;
using Kigl.Domain.Models.Stripe;
using System.Configuration;
using Stripe;

namespace Kigl.Stripe
{
    public class StripeService : IStripeService
    {
        private static readonly ILog Log = LogManager.GetLogger<StripeService>();

        public ChargeResult Charge(String token, Decimal amount, String email, Guid advertiserId, Guid paymentId)
        {
            try
            {
                var charges = new StripeChargeService();
                var charge = charges.Create(new StripeChargeCreateOptions
                {
                    Amount = (int)amount*100,
                    Currency = "usd",
                    Description = "Example charge",
                    SourceTokenOrExistingSourceId = token,
                    Metadata = new Dictionary<String, String>() { { "Email", email }, { "AdvertiserId", advertiserId.ToString()}, { "PaymentId", paymentId.ToString() } },
                });

                var result = new ChargeResult()
                {
                    JsonResult = charge.StripeResponse.ResponseJson,
                    IsPaid = charge.Paid,
                    Status = charge.Status,
                    ProviderId = charge.Id,
                    Amount = charge.Amount
                };

                return result;
            }
            catch(Exception exc)
            {
                var result = new ChargeResult()
                {
                    JsonResult = exc.ToString(),
                    IsPaid = false,
                    Status = "",
                    ProviderId = "",
                    Amount = 0
                };

                return result;
            }
        }

        public WebHookRequest Validate(String json, String signature)
        {
            Log.Info($"Got webhook request: {json}");
            Log.Info($"Got webhook request signature: {signature}");

            try
            {
                var stripeEvent = StripeEventUtility.ParseEvent(json);
                var endpointSecret = ConfigurationManager.AppSettings["StripeEndpointSecret"];
                //var stripeResult = StripeEventUtility.ParseEvent(json);
                var stripeResult = StripeEventUtility.ConstructEvent(json, signature, endpointSecret);

                var providerPaymentId = stripeResult.Data.Object.id;
                var request = json;
                var status = stripeResult.Data.Object.status;
                var advertiserId = Guid.Parse((String)stripeResult.Data.Object.metadata.AdvertiserId);
                var paymentId = Guid.Parse((String)stripeResult.Data.Object.metadata.PaymentId);
                var amount = ((Decimal)stripeResult.Data.Object.amount / 100);

                var result = new WebHookRequest()
                {
                    ProviderPaymentId = providerPaymentId,
                    Amount = amount,
                    Request = request,
                    Status = status,
                    AdvertiserId = advertiserId,
                    PaymentId = paymentId
                };

                result.IsPaymentSuccesfull = result.Status == "succeeded" && stripeResult.Type == "charge.succeeded";

                return result;
            }
            catch(Exception exc)
            {
                Log.Info($"Exception: {exc.ToString()}");
                return null;
            }
        }
    }
}

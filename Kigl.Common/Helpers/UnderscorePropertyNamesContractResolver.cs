﻿using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Serialization;

namespace Kigl.Common.Helpers
{
    public class UnderscorePropertyNamesContractResolver : DefaultContractResolver
    {
        private readonly string[] _skippedProperties =
        {
            "ProfilePage"
        };

        protected override string ResolvePropertyName(string propertyName)
        {
            if (_skippedProperties.Contains(propertyName))
                return propertyName;
            return Regex.Replace(propertyName, @"(\w)([A-Z])", "$1_$2").ToLower();
        }
    }
}

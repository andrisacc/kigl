﻿using System.Configuration;
using System.Linq;

namespace Kigl.Common.Helpers
{
    public static class Parser
    {
        public static int ParseTimeSpan(string timspanString)
        {
            var parts = timspanString.Split(':').Select(int.Parse).ToArray();
            return parts[0] * 60 + parts[1];
        }
    }
}

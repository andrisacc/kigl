﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kigl.Common.Models
{
    public class Transaction
    {
        public DateTime Date { get; set; }
        public String DateStr { get; set; }
        public String AdId { get; set; }
        public Decimal Amount { get; set; }
        public String AmountStr { get; set; }
        public String PaymentType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kigl.Common.Models
{
    public class TransactionsPage
    {
        public Transaction[] Transactions { get; set; }
        public int PageNum { get; set; }
        public int PagesCount { get; set; }
    }
}

﻿namespace Kigl.Common.Enums
{
    public enum VerifyStatus
    {
        Downloaded,
        InProgress,
        Failed,
        Success,
        CampaignStopped
    }
}

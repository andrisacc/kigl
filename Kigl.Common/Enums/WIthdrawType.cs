﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kigl.Common.Enums
{
    public enum WithdrawType
    {
        Paypal,
        BankTransfer,
        CreditCard
    }
}

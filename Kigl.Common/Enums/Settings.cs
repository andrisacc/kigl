﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kigl.Common.Enums
{
    public class Settings
    {
        public const string CheckInterval = "CheckInterval";
        public const string AdPrice = "AdPrice";
        public const string FollowersLimit = "FollowersLimit";
        public const string InstagramerPercent = "InstagramerPercent";
    }
}

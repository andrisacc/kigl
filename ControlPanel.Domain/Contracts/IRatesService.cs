﻿using System;
using ControlPanel.Domain.Models;

namespace ControlPanel.Domain.Contracts
{
    public interface IRatesService
    {
        Rates GetRates(Guid countryId);
        void UpdateRates(Rates rates);
    }
}

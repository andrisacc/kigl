﻿using System.Drawing;

namespace ControlPanel.Domain.Contracts
{
    public interface IImageService
    {
        void ResizeImage(string sourcePath, string destPath, Size size);
        void GenerateThumb(string sourcePath, string destPath);
    }
}

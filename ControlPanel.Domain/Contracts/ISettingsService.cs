﻿using System;
using ControlPanel.Domain.Models;

namespace ControlPanel.Domain.Contracts
{
    public interface ISettingsService
    {
        Country[] GetCountries();
        Country AddCountry(string countryName);
        City[] GetCities(Guid[] countryIds = null);
        void AddCity(City model);
        Currency[] GetCurrencies();
        Currency AddCurrency(string currencyName);
        Network[] GetNetworks(Guid[] cityIds = null);
        void AddNetwork(Network model);
        InstagramCategory[] GetInstagramCategories();
        decimal GetUserFollowersBalance(int followersCount);
        decimal GetAdvertiserFollowersBalance(int followersCount);
    }
}

﻿using System;
using AdvertiserDo = Common.DAL.Entities.Advertiser;
using AccountDo = Common.DAL.Entities.Account;
using UserDo = Common.DAL.Entities.User;
using ControlPanel.Domain.Models;

namespace ControlPanel.Domain.Contracts
{
    public interface IAccountsService
    {
        Account GetAccount(Guid id);

        AccountDo GetAdvertiserAccount(AdvertiserDo advertiser);

        Account[] GetAccounts();

        Account GetAccountByAdvertiserId(Guid advertiserId);

        void AddOrUpdateAccount(Account model);

        UserNewAdEmail[] GetEmailsForNewAdEmailSending();
    }
}

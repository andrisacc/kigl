﻿using System;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Models;

namespace ControlPanel.Domain.Contracts
{
    public interface IAdsService
    {
        Ad GetAd(Guid id);
        Ad[] GetAds(Guid advertiserId, params AdType[] types);
        void AddOrUpdateAd(Ad model, Guid? networkId = null);
        Boolean IsChangeAdStatusPossible(Guid adId); 
        string ChangeAdStatus(Guid adId);
        string StopCampaign(Guid adId, Guid networkId);
        Models.Statistic.UserAd[] GetUserAds(Guid adId);
        File GetFile(Guid id);
        PortalImage[] GetPortalImages(Guid networkId);
        PortalImage GetPortalImage(Guid id);
        void AddOrUpdatePortalImage(PortalImage portalImage);
        Boolean RemoveAd(Guid adId);
        Boolean AddBudgetForAd(Guid adId, decimal amount);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Domain.Models;
using ControlPanel.Domain.Models.Reports;

namespace ControlPanel.Domain.Contracts
{
    public interface IReportService
    {
        ReportData GenerateReport(string reportName, string format, Dictionary<String, DataTable> dataSources);
    }
}

﻿using System;
using ControlPanel.Domain.Models;

namespace ControlPanel.Domain.Contracts
{
    public interface IDataService
    {
        int GetTimeToShow(string userId, Guid networkId, out bool showTime);
        Ad GetVideoForNetwork(Guid networkId);
        Ad[] GetBanners(Guid networkId, int count);
        void IncrementNumberOfClicks(Guid networkId, Guid adId);
        PortalImage[] GetPortalImages(Guid networkId);
    }
}

﻿using System;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Models;

namespace ControlPanel.Domain.Contracts
{
    public interface IApprovalsService
    {
        Approval[] GetApprovals(Guid networkId);
        void UpdateAdStatus(Guid adId, Guid networkId, AdStatus status);
        void ChangeAdActive(Guid adId, Guid networkId);
    }
}

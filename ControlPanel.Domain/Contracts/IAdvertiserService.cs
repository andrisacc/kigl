﻿using System;
using ControlPanel.Domain.Models;
using ControlPanel.Domain.Models.Reports;

namespace ControlPanel.Domain.Contracts
{
    public interface IAdvertiserService
    {
        Advertiser GetAdvertiser(Guid id);
        Advertiser GetAdvertiserByEmail(string email);
        Advertiser[] GetAdvertisers(bool withoutAccounts = false);
        Advertiser[] GetAdvertisersOrderedByTime(bool withoutAccounts = false);
        bool ValidateAdvertiser(string name);
        bool ValidateEditedAdvertiser(string name, string advertiserId);
        void AddOrUpdateAdvertiser(Advertiser model);
        decimal GetAdvertiserBalance(Guid id);
        ReportData GenerateInvoice(Guid advertiserId, string amount);
        void Remove(Guid advertiserId);
        void UpdateApprovedStatus(Guid advertiserId, Boolean isApproved);
        void UpdateRejectedStatus(Guid advertiserId, Boolean isRejected);
    }
}

﻿using System;
using ControlPanel.Domain.Models;
using Kigl.Common.Enums;
using Common.DAL.Entities;

namespace ControlPanel.Domain.Contracts
{
    public interface IPaymentService
    {
        Payment GetById(Guid id);

        Guid AddPayment(Guid paymentId, Guid advertiserId, Decimal amount, Guid currencyId, String tranId, String syncResult, Kigl.Common.Enums.PaymentState state);

        Boolean UpdatePayment(String providerId, PaymentState state, String asyncResult, Guid paymentId);

        Int32 CheckPayment(String paymentId);
    }
}

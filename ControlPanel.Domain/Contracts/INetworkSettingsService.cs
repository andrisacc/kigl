﻿using System;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Models;

namespace ControlPanel.Domain.Contracts
{
    public interface INetworkSettingsService
    {
        NetworkSettings GetNetworkSettings(Guid networkId);
        void UpdateNetworkSetting(Guid networkId, NetworkSetting setting, bool value);
    }
}

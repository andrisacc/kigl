﻿using System;
using ControlPanel.Domain.Models.Statistic;

namespace ControlPanel.Domain.Contracts
{
    public interface IStatisticService
    {
        AdvertiserStatistic GetStatistic(Guid advertiserId);        
    }
}

﻿using System;

namespace ControlPanel.Domain.Models
{
    public class Advertiser : BaseModel
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string BrandName { get; set; }

        public string ContactPerson { get; set; }

        public string AccountLink { get; set; }

        public bool IsApproved { get; set; }

        public bool IsRejected { get; set; }

        public Guid[] NetworkIds { get; set; }
    }
}

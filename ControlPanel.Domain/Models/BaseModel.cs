﻿using System;

namespace ControlPanel.Domain.Models
{
    public class BaseModel
    {
        public Guid Id { get; set; }
    }
}

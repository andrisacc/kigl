﻿using System;

namespace ControlPanel.Domain.Models
{
    public class City : BaseModel
    {
        public Guid CountryId { get; set; }

        public string Name { get; set; }

        public Country Country { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel.Domain.Models
{
    public class InstagramCategory : BaseModel
    {
        public string Name { get; set; }
    }
}

﻿using System;
using System.IO;

namespace ControlPanel.Domain.Models.Reports
{
    public class ReportData
    {
        public Stream Stream { get; set; }
        public String FileExtension { get; set; }
        public string MimeType { get; set; }
        public string[] Warnings { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel.Domain.Models.Reports
{
    public class InvoiceData
    {
        public string From { get; set; }

        public string CompanyNo { get; set; }
        public string Vat { get; set; }
        public string IbanTitle { get; set; }
        public string Iban1 { get; set; }
        public string Iban2 { get; set; }
        public string Iban3 { get; set; }

        public string Number { get; set; }
        public string Description { get; set; }
        public string Amount { get; set; }
        public string Date { get; set; }
    }
}

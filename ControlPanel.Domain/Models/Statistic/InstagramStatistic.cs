﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Common.Enums;

namespace ControlPanel.Domain.Models.Statistic
{
    public class InstagramStatistic
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public bool IsNew { get; set; }
        public bool ShowStatus { get; set; }
        public bool IsFinished { get; set; }
        public string Status { get; set; }
        public Decimal Amount { get; set; }
        public decimal? Budget { get; set; }
        public String StartDate { get; set; }
        public String EndDate { get; set; }
        public AdType AdType { get; set; }
    }
}

﻿using System;
using ControlPanel.Common.Enums;

namespace ControlPanel.Domain.Models.Statistic
{
    public class Statistic : Ad
    {
        public Guid NetworkId { get; set; }
        public Guid FileId { get; set; }
        public int NumberOfClicks { get; set; }
        public int NumberOfImpressions { get; set; }
        public AdStatus AdStatus { get; set; }
        public bool IsStopped { get; set; }

        public decimal ChargeOfClicks { get; set; }
        public decimal ChargeOfImpressions { get; set; }
        public decimal Total { get; set; }
    }
}

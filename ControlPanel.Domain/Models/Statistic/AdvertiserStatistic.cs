﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel.Domain.Models.Statistic
{
    public class AdvertiserStatistic
    {
        public InstagramStatistic[] InstagramStatistics { get; set; }
        public CountryStatistic[] Statistics { get; set; }
    }
}

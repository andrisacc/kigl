﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel.Domain.Models.Statistic
{
    public class UserAd
    {
        public string Name { get; set; }
        public int FollowersCount { get; set; }
        public Decimal AdvertiserAmount { get; set; }
    }
}

﻿namespace ControlPanel.Domain.Models.Statistic
{
    public class CountryStatistic
    {
        public Country Country { get; set; }

        public NetworkStatistic[] Statistics { get; set; }

        public decimal Total { get; set; }
    }
}

﻿namespace ControlPanel.Domain.Models.Statistic
{
    public class NetworkStatistic
    {
        public Network Network { get; set; }

        public Statistic[] Statistics { get; set; }

        public decimal Total { get; set; }
    }
}

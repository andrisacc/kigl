﻿namespace ControlPanel.Domain.Models
{
    public class Country : BaseModel
    {
        public string Name { get; set; }

        public Network[] Networks { get; set; }
    }
}

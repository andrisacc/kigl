﻿using System;
using ControlPanel.Common.Enums;

namespace ControlPanel.Domain.Models
{
    public class Approval
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public AdType AdType { get; set; }
        public String AdStatus { get; set; }
        public Guid FileId { get; set; }
        public bool IsActive { get; set; }
        public bool IsStopped { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool ShowActive { get; set; }
    }
}
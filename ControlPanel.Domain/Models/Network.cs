﻿using System;

namespace ControlPanel.Domain.Models
{
    public class Network : BaseModel
    {
        public Guid CityId { get; set; }

        public string Name { get; set; }

        public City City { get; set; }
    }
}

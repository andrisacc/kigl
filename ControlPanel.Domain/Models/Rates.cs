﻿namespace ControlPanel.Domain.Models
{
    public class Rates : BaseModel
    {
        public decimal CostPerClick { get; set; }
        public decimal CostPerImpression { get; set; }
        public decimal CostPerInstall { get; set; }
    }
}

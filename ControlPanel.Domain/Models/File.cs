﻿using ControlPanel.Common.Enums;

namespace ControlPanel.Domain.Models
{
    public class File : BaseModel
    {
        public FileAdType FileAdType { get; set; }
        public string FileName { get; set; }
        public string UploadedFileName { get; set; }
        public string SharedLink { get; set; }

        public Ad Ad { get; set; }
    }
}

﻿using ControlPanel.Common.Enums;

namespace ControlPanel.Domain.Models
{
    public class NetworkStatus : BaseModel
    {
        public string Name { get; set; }
        public AdStatus AdStatus { get; set; }
    }
}

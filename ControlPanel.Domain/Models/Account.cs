﻿using System;

namespace ControlPanel.Domain.Models
{
    public class Account : BaseModel
    {
        public Guid AdvertiserId { get; set; }
        public Guid CurrencyId { get; set; }

        public string Number { get; set; }
        public decimal Balance { get; set; }

        public Advertiser Advertiser { get; set; }
        public Currency Currency { get; set; }
    }
}

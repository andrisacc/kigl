﻿using System;
using System.Collections.Generic;

namespace ControlPanel.Domain.Models
{
    public class UserNewAdEmail : User
    {
        public Guid AdId { get; set; }
    }
}

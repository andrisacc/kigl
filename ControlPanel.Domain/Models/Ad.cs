﻿using System;
using ControlPanel.Common.Enums;
using ControlPanel.Common.Helpers;

namespace ControlPanel.Domain.Models
{
    public class Ad : BaseModel
    {
        public Guid AdvertiserId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Caption { get; set; }
        public decimal? Budget { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public DateTime? StartDateValue => DateUtilites.ParseDate(StartDate);
        public DateTime? EndDateValue => DateUtilites.ParseDate(EndDate);
        public AdType AdType { get; set; }
        public File[] Files { get; set; }

        public string FileName { get; set; }

        public Guid[] NetworkIds { get; set; }
        public Guid[] CategoryIds { get; set; }
        public String[] CategoriesNames { get; set; }
        public NetworkStatus[] NetworkStatuses { get; set; }
    }
}

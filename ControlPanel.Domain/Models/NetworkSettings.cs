﻿namespace ControlPanel.Domain.Models
{
    public class NetworkSettings
    {
        public bool ShowHeaderImage { get; set; }
        public bool ShowFooterImage { get; set; }
        public bool ShowAd { get; set; }
    }
}

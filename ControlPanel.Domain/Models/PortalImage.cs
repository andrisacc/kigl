﻿using System;
using ControlPanel.Common.Enums;

namespace ControlPanel.Domain.Models
{
    public class PortalImage : BaseModel
    {
        public PortalImageType PortalImageType { get; set; }

        public string FileName { get; set; }

        public string Url { get; set; }

        public Guid NetworkId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace ControlPanel.Domain.Models
{
    public class User : BaseModel
    {
        public Guid? AdvertiserId { get; set; }
        public Guid? NetworkId { get; set; }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string FacebookId { get; set; }
        public string InstagramLogin { get; set; }
        public string Email { get; set; }
        public string Paypal { get; set; }
        public string Phone { get; set; }
        public Int32 FollowersCnt { get; set; }
        public bool IsLastFollowersRequestSuccess { get; set; }
        public bool Terms { get; set; }
        public string WithdrawAccount { get; set; }
        public DateTime Registred { get; set; }
        public string RegistrationLang { get; set; }

        public Advertiser Advertiser { get; set; }
        public Network Network { get; set; }

        public IEnumerable<string> InstagramCategories { get; set; }
    }
}

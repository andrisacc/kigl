﻿namespace ControlPanel.Domain.ApiModels
{
    public class VideoAd : Ad
    {
        public int TimeToShow { get; set; }
    }
}

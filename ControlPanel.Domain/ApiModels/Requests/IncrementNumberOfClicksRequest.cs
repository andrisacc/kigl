﻿using System;

namespace ControlPanel.Domain.ApiModels.Requests
{
    public class IncrementNumberOfClicksRequest
    {
        public Guid NetworkId { get; set; }
        public Guid AdId { get; set; }
    }
}

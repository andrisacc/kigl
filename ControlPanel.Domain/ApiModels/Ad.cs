﻿using System;

namespace ControlPanel.Domain.ApiModels
{
    public class Ad
    {
        public Guid? Id { get; set; }
        public Guid? FileId { get; set; }
        public string Url { get; set; }
    }
}

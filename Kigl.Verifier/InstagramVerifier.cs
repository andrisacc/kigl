﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kigl.Common.Enums;
using Kigl.Common.Helpers;
using Common.DAL;
using Common.DAL.Entities;
using ControlPanel.Common.Helpers;
using ControlPanel.Domain.Contracts;
using ControlPanel.Services;
using Kigl.Domain.Contracts;
using Kigl.Instagram;
using Kigl.Services;
using System.Configuration;

namespace Kigl.Verifier
{
    public class InstagramVerifier
    {
        private readonly DataContext _dataContext;
        private readonly IInstagramParserService _instagramParserService;
        private readonly IAccountsService _accountsService;
        private readonly IEmailService _emailService;
        private readonly IRewardService _rewardService;
        private DateTime Started;

        public InstagramVerifier()
        {
            _dataContext = new DataContext();
            _instagramParserService = new InstagramParserService();
            _accountsService = new AccountsService(_dataContext);
            _emailService = new EmailService();
            _rewardService = new RewardService(_dataContext);
        }

        public void Start()
        {
            Logger.Log.Info($"Logger started, UTC date: {DateTime.UtcNow}, Date: {DateTime.Now}");
            Started = DateTime.Now;
            try
            {
                Verify();
                CheckCampaings(_dataContext, _accountsService);
                UpdateFollowerCounts();
                SendNewAdEmailNotifications();
                _dataContext.SaveChanges();
                Logger.Log.Info($"Logger finished successfully, UTC date: {DateTime.UtcNow}, Date: {DateTime.Now}");
            }
            catch (Exception exc)
            {
                Logger.Log.Error($"Logger finished with exception, UTC date: {DateTime.UtcNow}, Date: {DateTime.Now}. Exception {exc.ToString()}");
            }
        }

        private void Verify()
        {
            var now = DateTime.UtcNow;
            var checkInterval = Parser.ParseTimeSpan(_dataContext.Settings.FirstOrDefault(item => item.Name == Settings.CheckInterval).Value);

            var userAds = _dataContext.UserAds
                .Include(item => item.User)
                .Include(item => item.Ad.Advertiser.Accounts)
                .Where(item => item.VerifyStatus == VerifyStatus.InProgress && item.NextVerifyDate.Value < now).ToArray();

            foreach (var userAd in userAds)
            {
                if (_instagramParserService.CheckText(userAd.User.InstagramLogin, userAd.Ad.Caption))
                {
                    if ((userAd.NextVerifyDate.Value - userAd.VerifyDate.Value).TotalMinutes < checkInterval)
                    {
                        userAd.NextVerifyDate = userAd.VerifyDate.Value.AddMinutes(checkInterval);
                        Logger.Log.Info($"Verify date change to {userAd.NextVerifyDate}, adId: {userAd.AdId}");
                    }
                    else
                    {
                        userAd.VerifyStatus = VerifyStatus.Success;
                        Logger.Log.Info($"Status changed to success, adId: {userAd.AdId}");

                        SendSuccessEmail(userAd.User.Email, userAd.User.RegistrationLang);
                        Logger.Log.Info($"Success email sent successfully, adId: {userAd.AdId}");

                        _rewardService.PayReward(userAd.Id, 0.04m, 0.04m);
                    }
                }
                else
                {
                    userAd.VerifyStatus = VerifyStatus.Failed;
                    userAd.Ad.Budget += userAd.AdvertiserAmount;
                    Logger.Log.Info($"Status changed to failed, adId: {userAd.AdId}");
                }
            }
        }

        private void SendNewAdEmailNotifications()
        {
            var usersNeedToBeNotified = _accountsService.GetEmailsForNewAdEmailSending();

            foreach (var user in usersNeedToBeNotified)
            {
                var adCategories = _dataContext.Ads.Include(item => item.InstagramCategories).First(item => item.Id == user.AdId).InstagramCategories.Select(item=>item.InstagramCategoryId).ToArray();

                var userCategories = _dataContext.Users.Include(item => item.InstagramCategories).First(item=>item.Id == user.Id).InstagramCategories.Select(item => item.InstagramCategoryId).ToArray();

                var IsCategoriesIntersected = adCategories.Intersect(userCategories).Count() > 0;

                if (IsCategoriesIntersected)
                {
                    SendNewAdEmail(user.Email, user.RegistrationLang);
                    Logger.Log.Info($"SendNewAdEmailNotifications successfully, userId: {user.Id}, adId {user.AdId}");
                }

                _dataContext.SentEmails.Add(new SentEmail() { AdId = user.AdId, Id = Guid.NewGuid(), IsSuccess = IsCategoriesIntersected, Timestamp = DateTime.Now, UserId = user.Id });

                if((DateTime.Now - Started).TotalMinutes >= 4)
                    break;
            }

        }

        public void CheckCampaings(DataContext ctx, IAccountsService accService)
        {
            var ads = ctx.Ads
                .Include(item => item.Advertiser)
                .Include(item => item.Advertiser.Accounts)
                .Where(item => item.StartDate.HasValue || item.EndDate.HasValue).ToArray();
            foreach (var ad in ads)
            {
                if (!ad.IsNew)
                {
                    if (ad.StartDate.HasValue)
                    {
                        if (ad.StartDate <= DateTime.Now)
                        {
                            if (ad.EndDate.HasValue)
                            {
                                if (ad.EndDate.Value.AddDays(1) <= DateTime.Now)
                                {
                                    if(ad.IsActive)
                                        ad.IsActive = false;

                                    if (ad.Budget.HasValue && ad.Budget.Value > 0)
                                    {
                                        var account = accService.GetAdvertiserAccount(ad.Advertiser);
                                        account.Balance += ad.Budget.Value;
                                        Logger.Log.Info($"Ad {ad.Id} returns budget to advertiser account : {ad.Budget}");
                                        ad.Budget = 0;
                                    }
                                }
                                else
                                {
                                    if (ad.State != ControlPanel.Common.Enums.AdState.Stopped)
                                    {
                                        ad.IsActive = true;
                                    }
                                }
                            }
                            else
                            {
                                if (ad.State != ControlPanel.Common.Enums.AdState.Stopped)
                                {
                                    ad.IsActive = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (ad.EndDate.HasValue)
                        {
                            if (ad.IsActive)
                                ad.IsActive = false;

                            if (ad.Budget.HasValue && ad.Budget.Value > 0)
                            {
                                var account = accService.GetAdvertiserAccount(ad.Advertiser);
                                account.Balance += ad.Budget.Value;
                                Logger.Log.Info($"Ad {ad.Id} returns budget to advertiser account : {ad.Budget}");
                                ad.Budget = 0;
                            }
                        }
                    }
                }

                //var shouldBe = Utilites.IsCampaignActive(ad.StartDate, ad.EndDate, DateTime.Now);
                //if (ad.IsActive != shouldBe && ad.State != ControlPanel.Common.Enums.AdState.Stopped && !ad.IsNew)
                //{
                //    ad.IsActive = shouldBe;
                //    Logger.Log.Info($"Ad {ad.Id} IsActive changed status to : {ad.IsActive}");

                //    if (ad.Budget.HasValue && !ad.IsActive)
                //    {
                //        var account = _accountsService.GetAdvertiserAccount(ad.Advertiser);
                //        account.Balance += ad.Budget.Value;
                //        Logger.Log.Info($"Ad {ad.Id} returns budget to advertiser account : {ad.Budget}");
                //        ad.Budget = 0;
                //    }
                //}
            }

            var networkAds = ctx.AdNetworks
                .Include(item => item.Ad.Advertiser.Accounts)
                .Where(item => item.StartDate.HasValue || item.EndDate.HasValue).ToArray();
            foreach (var networkAd in networkAds)
            {
                var status = networkAd.IsActive;
                networkAd.IsActive = Utilites.IsCampaignActive(networkAd.StartDate, networkAd.EndDate, DateTime.Now);
                if (status != networkAd.IsActive)
                {
                    Logger.Log.Info($"NetworkAd  Network: {networkAd.NetworkId} Ad: {networkAd.AdId} IsActive changed status to : {networkAd.IsActive}");
                }
                if (networkAd.Budget.HasValue && !networkAd.IsActive)
                {
                    var account = accService.GetAdvertiserAccount(networkAd.Ad.Advertiser);
                    account.Balance += networkAd.Budget.Value;
                    networkAd.Budget = null;
                    networkAd.IsStopped = true;
                }
            }
        }

        private void UpdateFollowerCounts()
        {
            var startCheckingDate = DateTime.Now.AddDays(-7);
            var usersBite = _dataContext.Users
                .Where(u => !String.IsNullOrEmpty(u.InstagramLogin) && u.FollowersCountLastUpdate < startCheckingDate)
                .Take(100);

            foreach(var user in usersBite)
            {
                try
                {
                    //if (user.InstagramLogin != "ivanovovi")
                    //{
                        var followInfo = _instagramParserService.GetFollowers(user.InstagramLogin);

                        if (followInfo != null)
                        {
                            Logger.Log.Error($"Successfully got instagram info for user for user {user.InstagramLogin}: FollowersCount = {followInfo.FollowedBy}; FollowesCount = {followInfo.Follows}");

                            user.FollowersCount = followInfo.FollowedBy;
                            user.FollowesCount = followInfo.Follows;

                            user.IsLastFollowersRequestSuccess = true;

                            user.FollowersCountLastUpdate = DateTime.Now;
                        }
                        else
                        {
                            if (user.FollowersCountLastUpdate == DateTime.MinValue)//never successfully checked
                            {
                                Logger.Log.Error($"Set next check for user {user.InstagramLogin} in 15 mins");

                                user.FollowersCountLastUpdate = DateTime.Now.AddDays(-7).AddMinutes(5);
                            }
                            else
                            {
                                Logger.Log.Error($"Set next check for user {user.InstagramLogin} in one hour");

                                user.FollowersCountLastUpdate = DateTime.Now.AddDays(-7).AddHours(1);
                            }

                            user.IsLastFollowersRequestSuccess = false;
                        }
                    //}
                }
                catch(Exception exc)
                {
                    Logger.Log.Error($"Exception during get followers count for user {user.InstagramLogin}: {exc.ToString()}");
                }
            }
        }

        private void SendSuccessEmail(String to, String lang)
        {
            var subject = "";
            if(lang == "ru")
            {
                subject = "Заработок от Kigl уже у вас на балансе";
            }
            else
            {
                subject = "Your recent earnings with Kigl have been added to your balance";
            }

            try
            {
                var body = GetHtmlBodyEmail("04_Post(success)", lang);
                _emailService.SendEmail(to, subject, body);
            }
            catch(Exception exc)
            {
                Logger.Log.Error($"Exception during sending email to {to}: {exc.ToString()}");
            }
        }

        private void SendNewAdEmail(String to, String lang)
        {
            var subject = "";
            if (lang == "ru")
            {
                subject = "Вам доступно рекламное задание в Kigl!";
            }
            else
            {
                subject = "Advertising is available for you on Kigl!";
            }

            try
            {
                var body = GetHtmlBodyEmail("06_Advertising-is-available", lang);
                _emailService.SendEmail(to, subject, body);
            }
            catch (Exception exc)
            {
                Logger.Log.Error($"Exception during sending email to {to}: {exc.ToString()}");
            }
        }

        private String GetHtmlBodyEmail(String templateName, String lang)
        {
            string kiglUrl = ConfigurationManager.AppSettings["KiglUrl"];

            string templatePath = kiglUrl + "/EmailTemplates/" + lang + "/" + templateName + ".html";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(templatePath);
            request.AutomaticDecompression = DecompressionMethods.GZip;

            var html = "";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                html = reader.ReadToEnd();
            }

            return html;
        }
    }
}

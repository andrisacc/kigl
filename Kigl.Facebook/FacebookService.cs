﻿using System;
using Common.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Kigl.Facebook
{
    public class FacebookService
    {
        private static readonly ILog Log = LogManager.GetLogger<FacebookService>();

        private static String _appSecret = HttpUtility.UrlEncode(ConfigurationManager.AppSettings["FacebookAppSecret"]);
        private static String _appId = HttpUtility.UrlEncode(ConfigurationManager.AppSettings["FacebookAppId"]);
        private static String _redirectUrl = HttpUtility.UrlEncode(ConfigurationManager.AppSettings["FacebookLoginUrl"]);

        public FacebookService()
        {
        }

        private String GetUserToken(String code)
        {
            WebClient client = new WebClient();
            var request = $"https://graph.facebook.com/v2.11/oauth/access_token?client_id={_appId}&redirect_uri={_redirectUrl}&client_secret={_appSecret}&code={code}";
            String tokenInfo = client.DownloadString(request);

            Log.Info($"GetUserToken request: {request} response {tokenInfo}");

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            dynamic item = serializer.Deserialize<object>(tokenInfo);
            string user_token = item["access_token"];

            return user_token;
        }

        private String GetAppAccessToken()
        {
            WebClient client = new WebClient();
            var request = $"https://graph.facebook.com/v2.11/oauth/access_token?client_id={_appId}&client_secret={_appSecret}&grant_type=client_credentials";
            var access_token_info = client.DownloadString(request);

            Log.Info($"GetAppAccessToken request: {request} response {access_token_info}");

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            dynamic item = serializer.Deserialize<object>(access_token_info);
            string access_token = item["access_token"];

            return access_token;
        }

        private String GetUserInfo(String accessToken, String UserToken)
        {
            WebClient client = new WebClient();
            var request = $"https://graph.facebook.com/v2.11/debug_token?input_token={UserToken}&access_token={accessToken}";
            var user_info = client.DownloadString(request);

            Log.Info($"GetUserInfo request: {request} response {user_info}");

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            dynamic item = serializer.Deserialize<object>(user_info);
            string user_id = item["data"]["user_id"];

            return user_id;
        }

        private String GetUserName(String facebookId, String accessToken)
        {
            WebClient client = new WebClient();
            var request = $"https://graph.facebook.com/v2.11/{facebookId}?access_token={accessToken}";
            var user_name_response = client.DownloadString(request);

            Log.Info($"GetUserName request: {request} response {user_name_response}");

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            dynamic item = serializer.Deserialize<object>(user_name_response);
            string user_name = System.Uri.UnescapeDataString(item["name"]);

            return user_name;
        }

        private String GetPictureUrl(String facebookId, String accessToken)
        {
            WebClient client = new WebClient();
            var picUrl = $"https://graph.facebook.com/v2.11/{facebookId}/picture?type=normal&access_token={accessToken}";

            return picUrl;
        }

        public String Name { get; set; }

        public String PicUrl { get; set; }

        public String GetRedirectUrl()
        {
            var state = HttpUtility.UrlEncode(DateTime.UtcNow.ToShortDateString());

            var url = $"https://www.facebook.com/v2.11/dialog/oauth?client_id={_appId}&state={state}&redirect_uri={_redirectUrl}";

            return url;
        }

        public String GetUserId(String code)
        {
            String fb_user_id = "";
            try
            {
                Log.Info($"GetUserId input code: {code}");

                var user_token = GetUserToken(code);

                Log.Info($"GetUserId user_token: {user_token}");

                var access_token = GetAppAccessToken();

                Log.Info($"GetUserId access_token: {access_token}");

                fb_user_id = GetUserInfo(access_token, user_token);

                Log.Info($"GetUserId user_info: {fb_user_id}");

                Name = GetUserName(fb_user_id, access_token);
                Log.Info($"GetUserName name: {Name}");

                PicUrl = GetPictureUrl(fb_user_id, access_token);
                Log.Info($"GetPictureUrl url: {PicUrl}");
            }
            catch (Exception exc)
            {
                Log.Info($"Facebook login exception: {exc.Message}");
            }

            return fb_user_id;
        }

        public String Share(String targetUrl, String caption, String redirectUrl)
        {
            var encodedCaption = HttpUtility.UrlEncode(caption);
            var encodedTargetUrl = HttpUtility.UrlEncode(targetUrl);

            var redirectRul = $"https://www.facebook.com/dialog/feed?app_id={_appId}&display=page&amp;caption={encodedCaption}&link={encodedTargetUrl}&redirect_uri={redirectUrl}";

            return redirectRul;
        }
    }
}

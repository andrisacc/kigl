﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel.Common.Models
{
    public class Balance
    {
        public decimal UserBalance { get; set; }
        public decimal AdvertiserBalance { get; set; }
    }
}

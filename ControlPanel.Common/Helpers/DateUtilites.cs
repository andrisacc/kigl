﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel.Common.Helpers
{
    public static class DateUtilites
    {
        private const string DateFormat = "dd/MM/yyyy";

        public static DateTime? ParseDate(string date)
        {
            if (!string.IsNullOrWhiteSpace(date))
            {
                DateTime parseResult;
                if (DateTime.TryParseExact(date, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out parseResult))
                {
                    return parseResult;
                }
            }
            return null;
        }

        public static string ToFormattedDate(this DateTime? date)
        {
            return date?.ToString(DateFormat);
        }
    }
}

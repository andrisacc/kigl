﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel.Common.Helpers
{
    public class Utilites
    {
        public static bool IsCampaignActive(DateTime? startDate, DateTime? endDate, DateTime now)
        {
            return (!startDate.HasValue || now > startDate.Value) && (!endDate.HasValue || endDate.Value.AddDays(1) > now);
        }
    }
}

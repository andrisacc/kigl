﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using ControlPanel.Common.Enums;

namespace ControlPanel.Common.Configs
{
    public class ImageSizesSection : ConfigurationSection
    {
        private const string SectionName = "ImageSizesSettings";

        private const string SmallImageSizeField = "SmallImageSize";
        private const string MediumImageSizeField = "MediumImageSize";
        private const string LargeImageSizeField = "LargeImageSize";

        private const char Splitter = 'x';

        public static ImageSizesSection GetSection()
        {
            var section = ConfigurationManager.GetSection(SectionName);
            if (section == null)
            {
                throw new ArgumentException($"No section {SectionName} found in config");
            }
            return (ImageSizesSection)section;
        }

        [ConfigurationProperty(SmallImageSizeField, IsRequired = true)]
        private string SmallImageSize
        {
            get { return this[SmallImageSizeField].ToString(); }
            set { this[SmallImageSizeField] = value; }
        }

        [ConfigurationProperty(MediumImageSizeField, IsRequired = true)]
        private string MediumImageSize
        {
            get { return this[MediumImageSizeField].ToString(); }
            set { this[MediumImageSizeField] = value; }
        }

        [ConfigurationProperty(LargeImageSizeField, IsRequired = true)]
        private string LargeImageSize
        {
            get { return this[LargeImageSizeField].ToString(); }
            set { this[LargeImageSizeField] = value; }
        }

        public Dictionary<FileAdType, Size> ImageSizes => new Dictionary<FileAdType, Size>
        {
            {FileAdType.Small, ParseSize(SmallImageSize)},
            {FileAdType.Medium, ParseSize(MediumImageSize)},
            {FileAdType.Large, ParseSize(LargeImageSize)}
        };


        private Size ParseSize(string value)
        {
            var splitted = value.Split(Splitter);
            return new Size(int.Parse(splitted[0]), int.Parse(splitted[1]));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel.Common.Enums
{
    public class Constants
    {
        public const string Lang = "lang";
        public const string CurrentCulture = "Kigl.CurrentCulture";
        public const string Referral = "Kigl.Referral";

        public const string AccountControllerName = "Account";
        public const string LoginActionName = "Advertisers";
        public const string InfoActionName = "Info";
        public const string InstagramLogoutActionName = "InstagramLogout";
    }
}

﻿namespace ControlPanel.Common.Enums
{
    public enum AdState
    {
        Activated,
        Stopped
    }
}

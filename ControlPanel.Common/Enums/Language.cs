﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ControlPanel.Common.Enums
{
    public enum Language
    {
        English,
        Russian
    }

    public static class Helper
    {
        public static string Localize(this Language language)
        {
            if (language == Language.Russian)
                return "Русский";
            return language.ToString();
        }
    }
}

﻿namespace ControlPanel.Common.Enums
{
    public enum NetworkSetting
    {
        ShowHeaderImage,
        ShowFooterImage,
        ShowAd
    }
}

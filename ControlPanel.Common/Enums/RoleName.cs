﻿namespace ControlPanel.Common.Enums
{
    public class RoleName
    {
        public const string Admin = "Admin";
        public const string Advertiser = "Advertiser";
        public const string NetworkAdmin = "NetworkAdmin";
        public const string InstagramAdmin = "InstagramAdmin";
    }
}

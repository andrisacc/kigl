﻿namespace ControlPanel.Common.Enums
{
    public enum AdType
    {
        Banner,
        Video,
        HeaderImage,
        FooterImage,
        InstagramImage,
        InstagramVideo
    }
}

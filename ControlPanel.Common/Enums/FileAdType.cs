﻿namespace ControlPanel.Common.Enums
{
    public enum FileAdType
    {
        Small,
        Medium,
        Large,
        Video,
        Thumbnail
    }
}

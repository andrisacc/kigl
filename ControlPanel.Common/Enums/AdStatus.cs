﻿namespace ControlPanel.Common.Enums
{
    public enum AdStatus
    {
        Pending,
        Approved,
        Rejected
    }
}

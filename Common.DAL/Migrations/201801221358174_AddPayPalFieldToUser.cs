namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPayPalFieldToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Paypal", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Paypal");
        }
    }
}

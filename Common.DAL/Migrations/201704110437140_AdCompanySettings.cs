namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdCompanySettings : DbMigration
    {
        public override void Up()
        {
            AddColumn("Ads", "Budget", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("Ads", "StartDate", c => c.DateTime(precision: 0));
            AddColumn("Ads", "EndDate", c => c.DateTime(precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("Ads", "EndDate");
            DropColumn("Ads", "StartDate");
            DropColumn("Ads", "Budget");
        }
    }
}

namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdvertiserIsRejectedFieldAddedEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Advertisers", "IsRejected", c => c.Boolean(nullable: false));

            Sql(@"update Advertisers set IsRejected = 0");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Advertisers", "IsRejected");
        }
    }
}

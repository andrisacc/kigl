namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdNetworkFinancialProperties : DbMigration
    {
        public override void Up()
        {
            AddColumn("AdNetworks", "Budget", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("AdNetworks", "StartDate", c => c.DateTime(precision: 0));
            AddColumn("AdNetworks", "EndDate", c => c.DateTime(precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("AdNetworks", "EndDate");
            DropColumn("AdNetworks", "StartDate");
            DropColumn("AdNetworks", "Budget");
        }
    }
}

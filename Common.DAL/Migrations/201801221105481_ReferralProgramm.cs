namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReferralProgramm : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReferralUsers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        InvitedUserId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.InvitedUserId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.InvitedUserId);
            
            CreateTable(
                "dbo.ReferralRewards",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserAdId = c.Guid(nullable: false),
                        TransactionId = c.Guid(nullable: false),
                        InvitingUserId = c.Guid(nullable: false),
                        Reward = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.InvitingUserId, cascadeDelete: true)
                .ForeignKey("dbo.AdTransactions", t => t.TransactionId, cascadeDelete: true)
                .ForeignKey("dbo.UserAds", t => t.UserAdId, cascadeDelete: true)
                .Index(t => t.UserAdId)
                .Index(t => t.TransactionId)
                .Index(t => t.InvitingUserId);
            
            AddColumn("dbo.Users", "ReferralCode", c => c.String(nullable: false, maxLength: 10, storeType: "nvarchar"));

            Sql("update Users set ReferralCode = SUBSTR(MD5(RAND()),22,10);");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReferralRewards", "UserAdId", "dbo.UserAds");
            DropForeignKey("dbo.ReferralRewards", "TransactionId", "dbo.AdTransactions");
            DropForeignKey("dbo.ReferralRewards", "InvitingUserId", "dbo.Users");
            DropForeignKey("dbo.ReferralUsers", "UserId", "dbo.Users");
            DropForeignKey("dbo.ReferralUsers", "InvitedUserId", "dbo.Users");
            DropIndex("dbo.ReferralRewards", new[] { "InvitingUserId" });
            DropIndex("dbo.ReferralRewards", new[] { "TransactionId" });
            DropIndex("dbo.ReferralRewards", new[] { "UserAdId" });
            DropIndex("dbo.ReferralUsers", new[] { "InvitedUserId" });
            DropIndex("dbo.ReferralUsers", new[] { "UserId" });
            DropColumn("dbo.Users", "ReferralCode");
            DropTable("dbo.ReferralRewards");
            DropTable("dbo.ReferralUsers");
        }
    }
}

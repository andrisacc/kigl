namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FileSharedLink : DbMigration
    {
        public override void Up()
        {
            AddColumn("Files", "SharedLink", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("Files", "SharedLink");
        }
    }
}

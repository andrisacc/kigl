namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Invoices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Invoices",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AdvertiserId = c.Guid(nullable: false),
                        Date = c.DateTime(nullable: false, precision: 0),
                        Number = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Advertisers", t => t.AdvertiserId, cascadeDelete: true)
                .Index(t => t.AdvertiserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Invoices", "AdvertiserId", "Advertisers");
            DropIndex("Invoices", new[] { "AdvertiserId" });
            DropTable("Invoices");
        }
    }
}

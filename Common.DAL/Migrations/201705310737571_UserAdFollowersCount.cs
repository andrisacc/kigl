namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserAdFollowersCount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserAds", "FollowersCount", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.UserAds", "FollowersCount");
        }
    }
}

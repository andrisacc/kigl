namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRegistred : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Registred", c => c.DateTime(nullable: false, precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Registred");
        }
    }
}

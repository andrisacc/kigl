namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStateColumnToAd : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Ads", "State", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Ads", "State");
        }
    }
}

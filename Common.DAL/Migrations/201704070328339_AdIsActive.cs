namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdIsActive : DbMigration
    {
        public override void Up()
        {
            AddColumn("Ads", "IsActive", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Ads", "IsActive");
        }
    }
}

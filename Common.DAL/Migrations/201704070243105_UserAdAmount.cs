namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserAdAmount : DbMigration
    {
        public override void Up()
        {
            AddColumn("UserAds", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));

            Sql(@"INSERT INTO Settings
(Id, Name, Value) 
VALUES (uuid(), 'FollowersLimit', '5');");
        }
        
        public override void Down()
        {
            DropColumn("UserAds", "Amount");
        }
    }
}

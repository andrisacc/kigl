namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLastInstaReqToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "IsLastFollowersRequestSuccess", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "IsLastFollowersRequestSuccess");
        }
    }
}

// <auto-generated />
namespace Common.DAL.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class AdvertiserIsApprovedFieldAddedEntity : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AdvertiserIsApprovedFieldAddedEntity));
        
        string IMigrationMetadata.Id
        {
            get { return "201712201357391_AdvertiserIsApprovedFieldAddedEntity"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}

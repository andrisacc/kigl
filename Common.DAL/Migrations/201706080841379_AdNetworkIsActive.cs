namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdNetworkIsActive : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdNetworks", "IsActive", c => c.Boolean(nullable: false, storeType: "bit"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AdNetworks", "IsActive");
        }
    }
}

namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FinancialFlow : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdTransactions",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserAdId = c.Guid(nullable: false),
                        CurrencyId = c.Guid(nullable: false),
                        Timestamp = c.DateTime(nullable: false, precision: 0),
                        UserAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AdvertiserAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        KiglAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UserPercent = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AdvertiserPercent = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId, cascadeDelete: true)
                .ForeignKey("dbo.UserAds", t => t.UserAdId, cascadeDelete: true)
                .Index(t => t.UserAdId)
                .Index(t => t.CurrencyId);
            
            AddColumn("dbo.UserAds", "UserAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.UserAds", "AdvertiserAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.UserAds", "UserPercent", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.UserAds", "AdvertiserPercent", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.UserAds", "Amount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.UserAds", "Amount", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropForeignKey("dbo.AdTransactions", "UserAdId", "dbo.UserAds");
            DropForeignKey("dbo.AdTransactions", "CurrencyId", "dbo.Currencies");
            DropIndex("dbo.AdTransactions", new[] { "CurrencyId" });
            DropIndex("dbo.AdTransactions", new[] { "UserAdId" });
            DropColumn("dbo.UserAds", "AdvertiserPercent");
            DropColumn("dbo.UserAds", "UserPercent");
            DropColumn("dbo.UserAds", "AdvertiserAmount");
            DropColumn("dbo.UserAds", "UserAmount");
            DropTable("dbo.AdTransactions");
        }
    }
}

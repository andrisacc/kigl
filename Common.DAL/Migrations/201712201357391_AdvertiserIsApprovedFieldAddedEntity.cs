namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AdvertiserIsApprovedFieldAddedEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Advertisers", "IsApproved", c => c.Boolean(nullable: false, defaultValue: true));

            Sql(@"update Advertisers set IsApproved = 1");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Advertisers", "IsApproved");
        }
    }
}

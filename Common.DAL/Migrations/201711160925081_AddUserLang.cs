namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserLang : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "RegistrationLang", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "RegistrationLang");
        }
    }
}

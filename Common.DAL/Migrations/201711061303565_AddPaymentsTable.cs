namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AdvertiserId = c.Guid(nullable: false),
                        CurrencyId = c.Guid(nullable: false),
                        ProviderId = c.String(unicode: false),
                        Timestamp = c.DateTime(nullable: false, precision: 0),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        SyncResult = c.String(unicode: false),
                        AsyncResult = c.String(unicode: false),
                        State = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Advertisers", t => t.AdvertiserId, cascadeDelete: true)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId, cascadeDelete: true)
                .Index(t => t.AdvertiserId)
                .Index(t => t.CurrencyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Payments", "CurrencyId", "dbo.Currencies");
            DropForeignKey("dbo.Payments", "AdvertiserId", "dbo.Advertisers");
            DropIndex("dbo.Payments", new[] { "CurrencyId" });
            DropIndex("dbo.Payments", new[] { "AdvertiserId" });
            DropTable("dbo.Payments");
        }
    }
}

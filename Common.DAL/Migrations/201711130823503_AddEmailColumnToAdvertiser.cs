namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEmailColumnToAdvertiser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Advertisers", "Email", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Advertisers", "Email");
        }
    }
}

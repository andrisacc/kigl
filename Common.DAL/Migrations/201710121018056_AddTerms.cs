namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTerms : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Terms", c => c.Boolean(nullable: false, storeType: "bit"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Terms");
        }
    }
}

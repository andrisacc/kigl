namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSentEmails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SentEmails",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        AdId = c.Guid(nullable: false),
                        IsSuccess = c.Boolean(nullable: false),
                        Timestamp = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ads", t => t.AdId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.AdId);

            Sql("insert into SentEmails select UUID() as Id, u.Id as UserId, a.Id as AdId, 1 as IsSuccess, now() as Timestamp from Users u cross join Ads a where u.InstagramLogin is not null order by u.Id, a.Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SentEmails", "UserId", "dbo.Users");
            DropForeignKey("dbo.SentEmails", "AdId", "dbo.Ads");
            DropIndex("dbo.SentEmails", new[] { "AdId" });
            DropIndex("dbo.SentEmails", new[] { "UserId" });
            DropTable("dbo.SentEmails");
        }
    }
}

namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPhotoUrlFieldToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "PhotoUrl", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "PhotoUrl");
        }
    }
}

namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CascadeDeletation : DbMigration
    {
        public override void Up()
        {
            //Sql(@"ALTER TABLE Users DROP FOREIGN KEY Users_ibfk_1;"); 
            DropForeignKey("Users", "AdvertiserId", "Advertisers");
            AddForeignKey("Users", "AdvertiserId", "Advertisers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("Users", "AdvertiserId", "Advertisers");
            AddForeignKey("Users", "AdvertiserId", "Advertisers", "Id");
        }
    }
}

namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Seed : DbMigration
    {
        public override void Up()
        {
            Sql(@"INSERT INTO Settings
(Id, Name, Value) 
VALUES (uuid(), 'AdPrice', '3');

INSERT INTO Settings
(Id, Name, Value) 
VALUES (uuid(), 'CheckInterval', '24:00');

INSERT INTO Roles
(Id, Name) 
VALUES (uuid(), 'Admin');

INSERT INTO Roles
(Id, Name) 
VALUES (uuid(), 'Advertiser');

INSERT INTO Roles
(Id, Name) 
VALUES (uuid(), 'NetworkAdmin');

INSERT INTO Settings
(Id, Name, Value) 
VALUES (uuid(), 'InstagramerPercent', '80');

INSERT INTO Currencies
(Id, Name) 
VALUES ('4a215876-053d-41f3-b282-42b325ee5740', 'USD');

INSERT INTO Currencies
(Id, Name) 
VALUES ('d92678bd-aefb-4697-ada7-42bbc71bfaf9', 'USD');");
        }
        
        public override void Down()
        {
        }
    }
}

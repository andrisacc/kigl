namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InstagramCategory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "InstagramCategories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "AdInstagramCategories",
                c => new
                    {
                        AdId = c.Guid(nullable: false),
                        InstagramCategoryId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.AdId, t.InstagramCategoryId })
                .ForeignKey("Ads", t => t.AdId, cascadeDelete: true)
                .ForeignKey("InstagramCategories", t => t.InstagramCategoryId, cascadeDelete: true)
                .Index(t => t.AdId)
                .Index(t => t.InstagramCategoryId);
            
            AddColumn("Users", "InstagramCategoryId", c => c.Guid());
            CreateIndex("Users", "InstagramCategoryId");
            AddForeignKey("Users", "InstagramCategoryId", "InstagramCategories", "Id");

            Sql(@"INSERT INTO Roles
(Id, Name) 
VALUES (uuid(), 'InstagramAdmin');"); 
        }
        
        public override void Down()
        {
            DropForeignKey("AdInstagramCategories", "InstagramCategoryId", "InstagramCategories");
            DropForeignKey("AdInstagramCategories", "AdId", "Ads");
            DropForeignKey("Users", "InstagramCategoryId", "InstagramCategories");
            DropIndex("AdInstagramCategories", new[] { "InstagramCategoryId" });
            DropIndex("AdInstagramCategories", new[] { "AdId" });
            DropIndex("Users", new[] { "InstagramCategoryId" });
            DropColumn("Users", "InstagramCategoryId");
            DropTable("AdInstagramCategories");
            DropTable("InstagramCategories");
        }
    }
}

namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserInstagramCategories : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Users", "InstagramCategoryId", "InstagramCategories");
            DropIndex("Users", new[] { "InstagramCategoryId" });
            CreateTable(
                "UserInstagramCategories",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        InstagramCategoryId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.InstagramCategoryId })
                .ForeignKey("InstagramCategories", t => t.InstagramCategoryId, cascadeDelete: true)
                .ForeignKey("Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.InstagramCategoryId);
            
            DropColumn("Users", "InstagramCategoryId");
        }
        
        public override void Down()
        {
            AddColumn("Users", "InstagramCategoryId", c => c.Guid());
            DropForeignKey("UserInstagramCategories", "UserId", "Users");
            DropForeignKey("UserInstagramCategories", "InstagramCategoryId", "InstagramCategories");
            DropIndex("UserInstagramCategories", new[] { "InstagramCategoryId" });
            DropIndex("UserInstagramCategories", new[] { "UserId" });
            DropTable("UserInstagramCategories");
            CreateIndex("Users", "InstagramCategoryId");
            AddForeignKey("Users", "InstagramCategoryId", "InstagramCategories", "Id");
        }
    }
}

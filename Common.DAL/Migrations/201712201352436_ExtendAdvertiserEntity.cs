namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendAdvertiserEntity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Advertisers", "BrandName", c => c.String(unicode: false, nullable: true));
            AddColumn("dbo.Advertisers", "ContactPerson", c => c.String(unicode: false, nullable: true));
            AddColumn("dbo.Advertisers", "AccountLink", c => c.String(unicode: false, nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Advertisers", "AccountLink");
            DropColumn("dbo.Advertisers", "ContactPerson");
            DropColumn("dbo.Advertisers", "BrandName");
        }
    }
}

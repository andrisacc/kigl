namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PwdRecovery : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PasswordRecoveryLinks",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        Code = c.String(unicode: false),
                        Timestamp = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PasswordRecoveryLinks", "UserId", "dbo.Users");
            DropIndex("dbo.PasswordRecoveryLinks", new[] { "UserId" });
            DropTable("dbo.PasswordRecoveryLinks");
        }
    }
}

namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AdvertiserId = c.Guid(nullable: false),
                        CurrencyId = c.Guid(nullable: false),
                        Number = c.String(unicode: false),
                        Balance = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Advertisers", t => t.AdvertiserId, cascadeDelete: true)
                .ForeignKey("dbo.Currencies", t => t.CurrencyId, cascadeDelete: true)
                .Index(t => t.AdvertiserId)
                .Index(t => t.CurrencyId);
            
            CreateTable(
                "dbo.Advertisers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AdvertiserNetworks",
                c => new
                    {
                        AdvertiserId = c.Guid(nullable: false),
                        NetworkId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.AdvertiserId, t.NetworkId })
                .ForeignKey("dbo.Advertisers", t => t.AdvertiserId, cascadeDelete: true)
                .ForeignKey("dbo.Networks", t => t.NetworkId, cascadeDelete: true)
                .Index(t => t.AdvertiserId)
                .Index(t => t.NetworkId);
            
            CreateTable(
                "dbo.Networks",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CityId = c.Guid(nullable: false),
                        Name = c.String(unicode: false),
                        LastViewedVideoIndex = c.Int(nullable: false),
                        LastViewedBannerIndex = c.Int(nullable: false),
                        ShowHeaderImage = c.Boolean(nullable: false),
                        ShowFooterImage = c.Boolean(nullable: false),
                        ShowAd = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cities", t => t.CityId, cascadeDelete: true)
                .Index(t => t.CityId);
            
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CountryId = c.Guid(nullable: false),
                        Name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Countries", t => t.CountryId, cascadeDelete: true)
                .Index(t => t.CountryId);
            
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        RatesId = c.Guid(nullable: false),
                        Name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Rates", t => t.RatesId, cascadeDelete: true)
                .Index(t => t.RatesId);
            
            CreateTable(
                "dbo.Rates",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CostPerClick = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerImpression = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CostPerInstall = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PortalImages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        NetworkId = c.Guid(nullable: false),
                        PortalImageType = c.Int(nullable: false),
                        FileName = c.String(unicode: false),
                        Url = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Networks", t => t.NetworkId, cascadeDelete: true)
                .Index(t => t.NetworkId);
            
            CreateTable(
                "dbo.Currencies",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Ads",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AdvertiserId = c.Guid(nullable: false),
                        Name = c.String(unicode: false),
                        Url = c.String(unicode: false),
                        Caption = c.String(unicode: false),
                        AdType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Advertisers", t => t.AdvertiserId, cascadeDelete: true)
                .Index(t => t.AdvertiserId);
            
            CreateTable(
                "dbo.AdNetworks",
                c => new
                    {
                        AdId = c.Guid(nullable: false),
                        NetworkId = c.Guid(nullable: false),
                        NumberOfClicks = c.Int(nullable: false),
                        NumberOfImpressions = c.Int(nullable: false),
                        Index = c.Int(nullable: false),
                        AdStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AdId, t.NetworkId })
                .ForeignKey("dbo.Ads", t => t.AdId, cascadeDelete: true)
                .ForeignKey("dbo.Networks", t => t.NetworkId, cascadeDelete: true)
                .Index(t => t.AdId)
                .Index(t => t.NetworkId);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AdId = c.Guid(nullable: false),
                        FileAdType = c.Int(nullable: false),
                        FileName = c.String(unicode: false),
                        UploadedFileName = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ads", t => t.AdId, cascadeDelete: true)
                .Index(t => t.AdId);
            
            CreateTable(
                "dbo.UserAds",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        AdId = c.Guid(nullable: false),
                        VerifyStatus = c.Int(nullable: false),
                        VerifyDate = c.DateTime(precision: 0),
                        NextVerifyDate = c.DateTime(precision: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Ads", t => t.AdId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.AdId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AdvertiserId = c.Guid(),
                        NetworkId = c.Guid(),
                        FacebookId = c.String(unicode: false),
                        InstagramLogin = c.String(unicode: false),
                        Username = c.String(unicode: false),
                        Name = c.String(unicode: false),
                        Phone = c.String(unicode: false),
                        PasswordHash = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Advertisers", t => t.AdvertiserId)
                .ForeignKey("dbo.Networks", t => t.NetworkId)
                .Index(t => t.AdvertiserId)
                .Index(t => t.NetworkId);
            
            CreateTable(
                "dbo.AdvertiserApplications",
                c => new
                    {
                        AdvertiserId = c.Guid(nullable: false),
                        ApplicationId = c.Guid(nullable: false),
                        NumberOfInstalls = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AdvertiserId, t.ApplicationId })
                .ForeignKey("dbo.Advertisers", t => t.AdvertiserId, cascadeDelete: true)
                .ForeignKey("dbo.Applications", t => t.ApplicationId, cascadeDelete: true)
                .Index(t => t.AdvertiserId)
                .Index(t => t.ApplicationId);
            
            CreateTable(
                "dbo.Applications",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserNetworks",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        NetworkId = c.Guid(nullable: false),
                        ShowTime = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => new { t.UserId, t.NetworkId })
                .ForeignKey("dbo.Networks", t => t.NetworkId, cascadeDelete: true)
                .Index(t => t.NetworkId);
            
            CreateTable(
                "dbo.Settings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(unicode: false),
                        Value = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WithdrawRequests",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        WithdrawType = c.Int(nullable: false),
                        Account = c.String(unicode: false),
                        Timestamp = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRoles",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        RoleId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserRoles", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.WithdrawRequests", "UserId", "dbo.Users");
            DropForeignKey("dbo.UserNetworks", "NetworkId", "dbo.Networks");
            DropForeignKey("dbo.AdvertiserApplications", "ApplicationId", "dbo.Applications");
            DropForeignKey("dbo.AdvertiserApplications", "AdvertiserId", "dbo.Advertisers");
            DropForeignKey("dbo.UserAds", "UserId", "dbo.Users");
            DropForeignKey("dbo.Users", "NetworkId", "dbo.Networks");
            DropForeignKey("dbo.Users", "AdvertiserId", "dbo.Advertisers");
            DropForeignKey("dbo.UserAds", "AdId", "dbo.Ads");
            DropForeignKey("dbo.Files", "AdId", "dbo.Ads");
            DropForeignKey("dbo.AdNetworks", "NetworkId", "dbo.Networks");
            DropForeignKey("dbo.AdNetworks", "AdId", "dbo.Ads");
            DropForeignKey("dbo.Ads", "AdvertiserId", "dbo.Advertisers");
            DropForeignKey("dbo.Accounts", "CurrencyId", "dbo.Currencies");
            DropForeignKey("dbo.Accounts", "AdvertiserId", "dbo.Advertisers");
            DropForeignKey("dbo.AdvertiserNetworks", "NetworkId", "dbo.Networks");
            DropForeignKey("dbo.PortalImages", "NetworkId", "dbo.Networks");
            DropForeignKey("dbo.Networks", "CityId", "dbo.Cities");
            DropForeignKey("dbo.Cities", "CountryId", "dbo.Countries");
            DropForeignKey("dbo.Countries", "RatesId", "dbo.Rates");
            DropForeignKey("dbo.AdvertiserNetworks", "AdvertiserId", "dbo.Advertisers");
            DropIndex("dbo.UserRoles", new[] { "RoleId" });
            DropIndex("dbo.UserRoles", new[] { "UserId" });
            DropIndex("dbo.WithdrawRequests", new[] { "UserId" });
            DropIndex("dbo.UserNetworks", new[] { "NetworkId" });
            DropIndex("dbo.AdvertiserApplications", new[] { "ApplicationId" });
            DropIndex("dbo.AdvertiserApplications", new[] { "AdvertiserId" });
            DropIndex("dbo.Users", new[] { "NetworkId" });
            DropIndex("dbo.Users", new[] { "AdvertiserId" });
            DropIndex("dbo.UserAds", new[] { "AdId" });
            DropIndex("dbo.UserAds", new[] { "UserId" });
            DropIndex("dbo.Files", new[] { "AdId" });
            DropIndex("dbo.AdNetworks", new[] { "NetworkId" });
            DropIndex("dbo.AdNetworks", new[] { "AdId" });
            DropIndex("dbo.Ads", new[] { "AdvertiserId" });
            DropIndex("dbo.PortalImages", new[] { "NetworkId" });
            DropIndex("dbo.Countries", new[] { "RatesId" });
            DropIndex("dbo.Cities", new[] { "CountryId" });
            DropIndex("dbo.Networks", new[] { "CityId" });
            DropIndex("dbo.AdvertiserNetworks", new[] { "NetworkId" });
            DropIndex("dbo.AdvertiserNetworks", new[] { "AdvertiserId" });
            DropIndex("dbo.Accounts", new[] { "CurrencyId" });
            DropIndex("dbo.Accounts", new[] { "AdvertiserId" });
            DropTable("dbo.Roles");
            DropTable("dbo.UserRoles");
            DropTable("dbo.WithdrawRequests");
            DropTable("dbo.Settings");
            DropTable("dbo.UserNetworks");
            DropTable("dbo.Applications");
            DropTable("dbo.AdvertiserApplications");
            DropTable("dbo.Users");
            DropTable("dbo.UserAds");
            DropTable("dbo.Files");
            DropTable("dbo.AdNetworks");
            DropTable("dbo.Ads");
            DropTable("dbo.Currencies");
            DropTable("dbo.PortalImages");
            DropTable("dbo.Rates");
            DropTable("dbo.Countries");
            DropTable("dbo.Cities");
            DropTable("dbo.Networks");
            DropTable("dbo.AdvertiserNetworks");
            DropTable("dbo.Advertisers");
            DropTable("dbo.Accounts");
        }
    }
}

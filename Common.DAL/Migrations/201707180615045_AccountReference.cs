namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountReference : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accounts", "Reference", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Accounts", "Reference");
        }
    }
}

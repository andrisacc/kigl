namespace Common.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFollowersCountToUser : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "FollowersCount", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "FollowesCount", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "FollowersCountLastUpdate", c => c.DateTime(nullable: false, precision: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "FollowersCountLastUpdate");
            DropColumn("dbo.Users", "FollowesCount");
            DropColumn("dbo.Users", "FollowersCount");
        }
    }
}

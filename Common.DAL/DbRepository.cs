﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Common.DAL.Contracts;

namespace Common.DAL
{
    internal class DbRepository<T> : IRepository<T> where T: class
    {
        private readonly IDbSet<T> _set;

        public DbRepository(IDbSet<T> set)
        {
            _set = set;
        }

        public IQueryable<T> Query()
        {
            return _set;
        }

        public T Attach(T entity)
        {
            return _set.Attach(entity);
        }

        public T Remove(T entity)
        {
            return _set.Remove(entity);
        }

        public IQueryable<T> Include<TProperty>(Expression<Func<T, TProperty>> path)
        {
            return _set.Include(path);
        }

        public T Add(T entity)
        {
            return _set.Add(entity);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return _set.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Expression Expression => _set.Expression;
        public Type ElementType => _set.ElementType;
        public IQueryProvider Provider => _set.Provider;
    }
}

﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class CityMap : EntityTypeConfiguration<City>
    {
        public CityMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.Country)
                .WithMany(item => item.Cities)
                .HasForeignKey(item => item.CountryId);
        }
    }
}

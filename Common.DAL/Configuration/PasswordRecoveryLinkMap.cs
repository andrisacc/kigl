﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class PasswordRecoveryLinkMap : EntityTypeConfiguration<PasswordRecoveryLink>
    {
        public PasswordRecoveryLinkMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.User)
                .WithMany()
                .HasForeignKey(item => item.UserId);
        }
    }
}

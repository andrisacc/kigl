﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class AdvertiserNetworkMap : EntityTypeConfiguration<AdvertiserNetwork>
    {
        public AdvertiserNetworkMap()
        {
            HasKey(item => new { item.AdvertiserId, item.NetworkId });

            HasRequired(item => item.Advertiser)
                .WithMany(item => item.EntityNetworks)
                .HasForeignKey(item => item.AdvertiserId);

            HasRequired(item => item.Network)
                .WithMany()
                .HasForeignKey(item => item.NetworkId);
        }
    }
}

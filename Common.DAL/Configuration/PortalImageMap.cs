﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class PortalImageMap : EntityTypeConfiguration<PortalImage>
    {
        public PortalImageMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.Network)
                .WithMany(item => item.PortalImages)
                .HasForeignKey(item => item.NetworkId);
        }
    }
}

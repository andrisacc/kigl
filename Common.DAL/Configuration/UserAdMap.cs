﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class UserAdMap : EntityTypeConfiguration<UserAd>
    {
        public UserAdMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.User)
                .WithMany(item => item.UserAds)
                .HasForeignKey(item => item.UserId);

            HasRequired(item => item.Ad)
                .WithMany(item => item.UserAds)
                .HasForeignKey(item => item.AdId);
        }
    }
}

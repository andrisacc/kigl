﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class FileMap : EntityTypeConfiguration<File>
    {
        public FileMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.Ad)
                .WithMany(item => item.Files)
                .HasForeignKey(item => item.AdId);
        }
    }
}

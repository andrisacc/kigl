﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class ReferralRewardMap : EntityTypeConfiguration<ReferralReward>
    {
        public ReferralRewardMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.UserAd)
                .WithMany()
                .HasForeignKey(item => item.UserAdId);

            HasRequired(item => item.Transaction)
                .WithMany()
                .HasForeignKey(item => item.TransactionId);

            HasRequired(item => item.InvitingUser)
                .WithMany()
                .HasForeignKey(item => item.InvitingUserId);
        }
    }
}

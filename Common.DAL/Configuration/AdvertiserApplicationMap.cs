﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class AdvertiserApplicationMap : EntityTypeConfiguration<AdvertiserApplication>
    {
        public AdvertiserApplicationMap()
        {
            HasKey(item => new {item.AdvertiserId, item.ApplicationId});

            HasRequired(item => item.Advertiser)
                .WithMany()
                .HasForeignKey(item => item.AdvertiserId);

            HasRequired(item => item.Application)
                .WithMany()
                .HasForeignKey(item => item.ApplicationId);
        }
    }
}

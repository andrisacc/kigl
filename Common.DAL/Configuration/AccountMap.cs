﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class AccountMap : EntityTypeConfiguration<Account>
    {
        public AccountMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.Advertiser)
                .WithMany(item => item.Accounts)
                .HasForeignKey(item => item.AdvertiserId);

            HasRequired(item => item.Currency)
                .WithMany()
                .HasForeignKey(item => item.CurrencyId);
        }
    }
}

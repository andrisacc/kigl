﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class PaymentMap : EntityTypeConfiguration<Payment>
    {
        public PaymentMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.Advertiser)
                .WithMany()
                .HasForeignKey(item => item.AdvertiserId);

            HasRequired(item => item.Currency)
                .WithMany()
                .HasForeignKey(item => item.CurrencyId);
        }
    }
}

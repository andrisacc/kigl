﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class AdMap : EntityTypeConfiguration<Ad>
    {
        public AdMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.Advertiser)
                .WithMany()
                .HasForeignKey(item => item.AdvertiserId);
        }
    }
}

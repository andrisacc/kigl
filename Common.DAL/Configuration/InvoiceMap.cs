﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class InvoiceMap : EntityTypeConfiguration<Invoice>
    {
        public InvoiceMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.Advertiser)
                .WithMany()
                .HasForeignKey(item => item.AdvertiserId);
        }
    }
}

﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class UserRoleMap : EntityTypeConfiguration<UserRole>
    {
        public UserRoleMap()
        {
            HasKey(item => new { item.UserId, item.RoleId });

            HasRequired(item => item.User)
                .WithMany()
                .HasForeignKey(item => item.UserId);

            HasRequired(item => item.Role)
                .WithMany()
                .HasForeignKey(item => item.RoleId);
        }
    }
}

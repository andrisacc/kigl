﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class CountryMap : EntityTypeConfiguration<Country>
    {
        public CountryMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.Rates)
                .WithMany()
                .HasForeignKey(item => item.RatesId);
        }
    }
}

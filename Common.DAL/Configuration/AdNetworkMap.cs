﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class AdNetworkMap : EntityTypeConfiguration<AdNetwork>
    {
        public AdNetworkMap()
        {
            HasKey(item => new { item.AdId, item.NetworkId });

            HasRequired(item => item.Ad)
                .WithMany(item => item.EntityNetworks)
                .HasForeignKey(item => item.AdId);

            HasRequired(item => item.Network)
                .WithMany()
                .HasForeignKey(item => item.NetworkId);

            Property(item => item.IsActive)
                .HasColumnType("bit");

            Property(item => item.IsStopped)
                .HasColumnType("bit");
        }
    }
}

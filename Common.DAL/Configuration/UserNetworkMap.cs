﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class UserNetworkMap : EntityTypeConfiguration<UserNetwork>
    {
        public UserNetworkMap()
        {
            HasKey(item => new {item.UserId, item.NetworkId});

            HasRequired(item => item.Network)
                .WithMany()
                .HasForeignKey(item => item.NetworkId);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class InstagramCategoryMap : EntityTypeConfiguration<InstagramCategory>
    {
        public InstagramCategoryMap()
        {
            HasKey(item => item.Id);
        }
    }
}

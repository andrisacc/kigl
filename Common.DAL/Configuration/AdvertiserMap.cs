﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class AdvertiserMap : EntityTypeConfiguration<Advertiser>
    {
        public AdvertiserMap()
        {
            HasKey(item => item.Id);
        }
    }
}

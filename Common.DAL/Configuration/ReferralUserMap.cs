﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class ReferralUserMap : EntityTypeConfiguration<ReferralUser>
    {
        public ReferralUserMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.InvitedUser)
                .WithMany()
                .HasForeignKey(item => item.InvitedUserId)
                .WillCascadeOnDelete(true);

            HasRequired(item => item.User)
                .WithMany(item => item.ReferralUsers)
                .HasForeignKey(item => item.UserId)
                .WillCascadeOnDelete(true);
        }
    }
}

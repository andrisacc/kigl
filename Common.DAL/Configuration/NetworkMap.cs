﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class NetworkMap : EntityTypeConfiguration<Network>
    {
        public NetworkMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.City)
                .WithMany(item => item.Networks)
                .HasForeignKey(item => item.CityId);
        }
    }
}

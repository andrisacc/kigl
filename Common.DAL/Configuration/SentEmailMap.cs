﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class SentEmailMap : EntityTypeConfiguration<SentEmail>
    {
        public SentEmailMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.User)
                .WithMany()
                .HasForeignKey(item => item.UserId)
                .WillCascadeOnDelete(true);

            HasRequired(item => item.Ad)
                .WithMany()
                .HasForeignKey(item => item.AdId)
                .WillCascadeOnDelete(true);
        }
    }
}

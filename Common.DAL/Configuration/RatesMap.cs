﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class RatesMap : EntityTypeConfiguration<Rates>
    {
        public RatesMap()
        {
            HasKey(item => item.Id);
        }
    }
}

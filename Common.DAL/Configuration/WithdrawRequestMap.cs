﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class WithdrawRequestMap : EntityTypeConfiguration<WithdrawRequest>
    {
        public WithdrawRequestMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.User)
                .WithMany()
                .HasForeignKey(item => item.UserId);
        }
    }
}

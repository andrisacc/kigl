﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class UserInstagramCategoryMap : EntityTypeConfiguration<UserInstagramCategory>
    {
        public UserInstagramCategoryMap()
        {
            HasKey(item => new {item.UserId, item.InstagramCategoryId});

            HasRequired(item => item.User)
                .WithMany(item => item.InstagramCategories)
                .HasForeignKey(item => item.UserId);

            HasRequired(item => item.InstagramCategory)
                .WithMany()
                .HasForeignKey(item => item.InstagramCategoryId);
        }
    }
}

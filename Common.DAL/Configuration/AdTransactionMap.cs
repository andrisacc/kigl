﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class AdTransactionMap : EntityTypeConfiguration<AdTransaction>
    {
        public AdTransactionMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.UserAd)
                .WithMany()
                .HasForeignKey(item => item.UserAdId);

            HasRequired(item => item.Currency)
                .WithMany()
                .HasForeignKey(item => item.CurrencyId);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DAL.Entities;

namespace Common.DAL.Configuration
{
    public class AdInstagramCategoryMap : EntityTypeConfiguration<AdInstagramCategory>
    {
        public AdInstagramCategoryMap()
        {
            HasKey(item => new {item.AdId, item.InstagramCategoryId});

            HasRequired(item => item.Ad)
                .WithMany(item => item.InstagramCategories)
                .HasForeignKey(item => item.AdId);

            HasRequired(item => item.InstagramCategory)
                .WithMany()
                .HasForeignKey(item => item.InstagramCategoryId);
        }
    }
}

﻿using System.Data.Entity.ModelConfiguration;
using Common.DAL.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;

namespace Common.DAL.Configuration
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            HasOptional(item => item.Advertiser)
                .WithMany()
                .HasForeignKey(item => item.AdvertiserId)
                .WillCascadeOnDelete(true);

            HasOptional(item => item.Network)
                .WithMany()
                .HasForeignKey(item => item.NetworkId);

            Property(item => item.Terms)
                .HasColumnType("bit");

            Property(item => item.ReferralCode)
                .IsRequired()
                .HasMaxLength(10);
        }
    }
}

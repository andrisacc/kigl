﻿using System.Collections.Generic;

namespace Common.DAL.Contracts
{
    public interface INetworksEntity<T> where T : IEntityNetwork
    {
        ICollection<T> EntityNetworks { get; set; }
    }
}

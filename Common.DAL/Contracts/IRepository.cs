﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace Common.DAL.Contracts
{
    public interface IRepository<T> : IQueryable<T>
    {
        IQueryable<T> Query();
        T Attach(T entity);
        IQueryable<T> Include<TProperty>(Expression<Func<T, TProperty>> path);
        T Add(T entity);
        T Remove(T entity);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DAL.Contracts
{
    public interface IEntityInstagramCategory
    {
        Guid InstagramCategoryId { get; set; }
    }
}

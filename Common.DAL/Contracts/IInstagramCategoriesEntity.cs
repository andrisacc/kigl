﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DAL.Contracts
{
    public interface IInstagramCategoriesEntity<T> where T: IEntityInstagramCategory
    {
        ICollection<T> InstagramCategories { get; set; }
    }
}

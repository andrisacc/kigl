﻿using System;

namespace Common.DAL.Contracts
{
    public interface IEntityNetwork
    {
        Guid NetworkId { get; set; }
    }
}

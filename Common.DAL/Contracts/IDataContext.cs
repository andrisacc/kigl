﻿using Common.DAL.Entities;

namespace Common.DAL.Contracts
{
    public interface IDataContext
    {
        IRepository<Country> Countries { get; }
        IRepository<City> Cities { get; }
        IRepository<Currency> Currencies { get; }
        IRepository<Network> Networks { get; }
        IRepository<Advertiser> Advertisers { get; }
        IRepository<AdvertiserNetwork> AdvertiserNetworks { get; }
        IRepository<Account> Accounts { get; }
        IRepository<User> Users { get; }
        IRepository<UserRole> UserRoles { get; }
        IRepository<Role> Roles { get; }
        IRepository<Ad> Ads { get; }
        IRepository<AdNetwork> AdNetworks { get; }
        IRepository<File> Files { get; }
        IRepository<Rates> Rates { get; }
        IRepository<UserNetwork> UserNetworks { get; }
        IRepository<PortalImage> PortalImages { get; }
        IRepository<UserAd> UserAds { get; }
        IRepository<Setting> Settings { get; }
        IRepository<WithdrawRequest> WithdrawRequests { get; }
        IRepository<InstagramCategory> InstagramCategories { get; }
        IRepository<UserInstagramCategory> UserInstagramCategories { get; }
        IRepository<Invoice> Invoices { get; }
        IRepository<Payment> Payments { get; }
        IRepository<AdTransaction> AdTransactions { get; }
        IRepository<ReferralUser> ReferralUsers { get; }
        IRepository<ReferralReward> ReferralRewards { get; }
        IRepository<PasswordRecoveryLink> PasswordRecoveryLinks { get; }
        IRepository<SentEmail> SentEmails { get; }

        IRepository<T> Repository<T>() where T : class;

        int SaveChanges();
    }
}

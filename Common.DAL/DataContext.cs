﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Common.DAL.Configuration;
using Common.DAL.Contracts;
using Common.DAL.Entities;

namespace Common.DAL
{
    public class DataContext : DbContext, IDataContext
    {
        public DataContext() : base("DataContext")
        {
        }

        public IRepository<Country> Countries => Repository<Country>();
        public IRepository<City> Cities => Repository<City>();
        public IRepository<Currency> Currencies => Repository<Currency>();
        public IRepository<Network> Networks => Repository<Network>();
        public IRepository<Advertiser> Advertisers => Repository<Advertiser>();
        public IRepository<AdvertiserNetwork> AdvertiserNetworks => Repository<AdvertiserNetwork>();
        public IRepository<Account> Accounts => Repository<Account>();
        public IRepository<User> Users => Repository<User>();
        public IRepository<UserRole> UserRoles => Repository<UserRole>();
        public IRepository<Role> Roles => Repository<Role>();
        public IRepository<Ad> Ads => Repository<Ad>();
        public IRepository<AdNetwork> AdNetworks => Repository<AdNetwork>();
        public IRepository<File> Files => Repository<File>();
        public IRepository<Rates> Rates => Repository<Rates>();
        public IRepository<UserNetwork> UserNetworks => Repository<UserNetwork>();
        public IRepository<PortalImage> PortalImages => Repository<PortalImage>();
        public IRepository<UserAd> UserAds => Repository<UserAd>();
        public IRepository<Setting> Settings => Repository<Setting>();
        public IRepository<WithdrawRequest> WithdrawRequests => Repository<WithdrawRequest>();
        public IRepository<InstagramCategory> InstagramCategories => Repository<InstagramCategory>();
        public IRepository<UserInstagramCategory> UserInstagramCategories => Repository<UserInstagramCategory>();
        public IRepository<Invoice> Invoices => Repository<Invoice>();
        public IRepository<Payment> Payments => Repository<Payment>();
        public IRepository<AdTransaction> AdTransactions => Repository<AdTransaction>();
        public IRepository<ReferralUser> ReferralUsers => Repository<ReferralUser>();
        public IRepository<ReferralReward> ReferralRewards => Repository<ReferralReward>();
        public IRepository<PasswordRecoveryLink> PasswordRecoveryLinks => Repository<PasswordRecoveryLink>();
        public IRepository<SentEmail> SentEmails => Repository<SentEmail>();

        private readonly Dictionary<Type, IQueryable> _repositories = new Dictionary<Type, IQueryable>();
        
        public IRepository<T> Repository<T>() where T : class
        {
            IQueryable repository;
            IRepository<T> result;
            if (_repositories.TryGetValue(typeof(T), out repository))
            {
                result = (IRepository<T>) repository;
            }
            else
            {
                _repositories.Add(typeof(T), result = new DbRepository<T>(Set<T>()));
            }
            return result;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AccountMap());
            modelBuilder.Configurations.Add(new AdMap());
            modelBuilder.Configurations.Add(new AdvertiserApplicationMap());
            modelBuilder.Configurations.Add(new AdvertiserMap());
            modelBuilder.Configurations.Add(new ApplicationMap());
            modelBuilder.Configurations.Add(new CountryMap());
            modelBuilder.Configurations.Add(new CityMap());
            modelBuilder.Configurations.Add(new CurrencyMap());
            modelBuilder.Configurations.Add(new RatesMap());
            modelBuilder.Configurations.Add(new FileMap());
            modelBuilder.Configurations.Add(new AdvertiserNetworkMap());
            modelBuilder.Configurations.Add(new AdNetworkMap());
            modelBuilder.Configurations.Add(new AdInstagramCategoryMap());
            modelBuilder.Configurations.Add(new InstagramCategoryMap());
            modelBuilder.Configurations.Add(new UserNetworkMap());
            modelBuilder.Configurations.Add(new PortalImageMap());
            modelBuilder.Configurations.Add(new SettingMap());
            modelBuilder.Configurations.Add(new WithdrawRequestMap());
            modelBuilder.Configurations.Add(new UserInstagramCategoryMap());

            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new UserRoleMap());
            modelBuilder.Configurations.Add(new UserAdMap());
            modelBuilder.Configurations.Add(new InvoiceMap());
            modelBuilder.Configurations.Add(new PaymentMap());
            modelBuilder.Configurations.Add(new AdTransactionMap());
            modelBuilder.Configurations.Add(new PasswordRecoveryLinkMap());
            modelBuilder.Configurations.Add(new ReferralUserMap());
            modelBuilder.Configurations.Add(new ReferralRewardMap());
            modelBuilder.Configurations.Add(new SentEmailMap());
        }

        public static DataContext Create()
        {
            return new DataContext();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kigl.Common.Enums;

namespace Common.DAL.Entities
{
    public class ReferralReward : BaseEntity
    {
        public Guid UserAdId { get; set; }
        public Guid TransactionId { get; set; }
        public Guid InvitingUserId { get; set; }

        public Decimal Reward { get; set; }

        public UserAd UserAd { get; set; }
        public AdTransaction Transaction { get; set; }
        public User InvitingUser { get; set; }
    }
}

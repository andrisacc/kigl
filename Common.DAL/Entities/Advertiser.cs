﻿using System.Collections.Generic;
using Common.DAL.Contracts;

namespace Common.DAL.Entities
{
    public class Advertiser : BaseEntity, INetworksEntity<AdvertiserNetwork>
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string BrandName { get; set; }

        public string ContactPerson { get; set; }

        public string AccountLink { get; set; }

        public bool IsApproved { get; set; }

        public bool IsRejected { get; set; }

        public ICollection<AdvertiserNetwork> EntityNetworks { get; set; }
        public ICollection<Account> Accounts { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DAL.Entities
{
    public class Invoice
    {
        public Guid Id { get; set; }
        public Guid AdvertiserId { get; set; }
        public DateTime Date { get; set; }
        public int Number { get; set; }
        public decimal Amount { get; set; }

        public Advertiser Advertiser { get; set; }
    }
}

﻿using System;
using ControlPanel.Common.Enums;

namespace Common.DAL.Entities
{
    public class File : BaseEntity
    {
        public Guid AdId { get; set; }

        public FileAdType FileAdType { get; set; }
        public string FileName { get; set; }
        public string UploadedFileName { get; set; }
        public string SharedLink { get; set; }

        public Ad Ad { get; set; }
    }
}

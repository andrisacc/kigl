﻿using System;
using System.Collections.Generic;

namespace Common.DAL.Entities
{
    public class City : BaseEntity
    {
        public Guid CountryId { get; set; }

        public string Name { get; set; }

        public Country Country { get; set; }

        public ICollection<Network> Networks { get; set; }
    }
}

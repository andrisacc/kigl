﻿using System;
using System.Collections.Generic;

namespace Common.DAL.Entities
{
    public class Network : BaseEntity
    {
        public Guid CityId { get; set; }
            
        public string Name { get; set; }

        public City City { get; set; }

        public int LastViewedVideoIndex { get; set; }

        public int LastViewedBannerIndex { get; set; }

        public bool ShowHeaderImage { get; set; }

        public bool ShowFooterImage { get; set; }

        public bool ShowAd { get; set; }

        public ICollection<PortalImage> PortalImages { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using Common.DAL.Contracts;

namespace Common.DAL.Entities
{
    public class User : BaseEntity, IInstagramCategoriesEntity<UserInstagramCategory>
    {
        public Guid? AdvertiserId { get; set; }
        public Guid? NetworkId { get; set; }

        public string FacebookId { get; set; }
        public string InstagramLogin { get; set; }
        public string Username { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Paypal { get; set; }
        public string Phone { get; set; }
        public string PhotoUrl { get; set; }
        public string PasswordHash { get; set; }
        public bool Terms { get; set; }
        public string RegistrationLang { get; set; }

        public DateTime Registred { get; set; }

        public string ReferralCode { get; set; }

        public Advertiser Advertiser { get; set; }
        public Network Network { get; set; }

        public ICollection<UserAd> UserAds { get; set; }
        public ICollection<UserInstagramCategory> InstagramCategories { get; set; }
        public ICollection<ReferralUser> ReferralUsers { get; set; }

        public Int32 FollowersCount { get; set; }
        public Int32 FollowesCount { get; set; }
        public DateTime FollowersCountLastUpdate { get; set; }
        public Boolean IsLastFollowersRequestSuccess { get; set; }
    }
}

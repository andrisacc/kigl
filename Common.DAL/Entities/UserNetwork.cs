﻿using System;

namespace Common.DAL.Entities
{
    public class UserNetwork
    {
        public string UserId { get; set; }
        public Guid NetworkId { get; set; }

        public DateTime ShowTime { get; set; }

        public Network Network { get; set; }
    }
}

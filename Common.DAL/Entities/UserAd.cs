﻿using System;
using Kigl.Common.Enums;

namespace Common.DAL.Entities
{
    public class UserAd : BaseEntity
    {        
        public Guid UserId { get; set; }
        public Guid AdId { get; set; }

        public VerifyStatus VerifyStatus { get; set; }
        public DateTime? VerifyDate { get; set; }
        public DateTime? NextVerifyDate { get; set; }
        public Decimal UserAmount { get; set; }
        public Decimal AdvertiserAmount { get; set; }
        public Decimal UserPercent { get; set; }
        public Decimal AdvertiserPercent { get; set; }
        public int FollowersCount { get; set; }

        public User User { get; set; }
        public Ad Ad { get; set; }
    }
}

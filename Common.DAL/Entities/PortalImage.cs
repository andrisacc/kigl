﻿using System;
using ControlPanel.Common.Enums;

namespace Common.DAL.Entities
{
    public class PortalImage : BaseEntity
    {
        public Guid NetworkId { get; set; }

        public PortalImageType PortalImageType { get; set; }
        public string FileName { get; set; }
        public string Url { get; set; }

        public Network Network { get; set; }
    }
}

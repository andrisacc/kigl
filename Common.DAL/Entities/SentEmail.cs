﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DAL.Entities
{
    public class SentEmail : BaseEntity
    {
        public Guid UserId { get; set; }
        public Guid AdId { get; set; }

        public Boolean IsSuccess { get; set; }
        public DateTime Timestamp { get; set; }

        public User User { get; set; }
        public Ad Ad { get; set; }
    }
}

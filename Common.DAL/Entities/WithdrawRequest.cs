﻿using System;
using Kigl.Common.Enums;

namespace Common.DAL.Entities
{
    public class WithdrawRequest : BaseEntity
    {        
        public Guid UserId { get; set; }

        public WithdrawType WithdrawType { get; set; }
        public string Account { get; set; }
        public DateTime Timestamp { get; set; }

        public User User { get; set; }
    }
}

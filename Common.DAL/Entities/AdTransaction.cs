﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kigl.Common.Enums;

namespace Common.DAL.Entities
{
    public class AdTransaction : BaseEntity
    {
        public Guid UserAdId { get; set; }
        public Guid CurrencyId { get; set; }

        public DateTime Timestamp { get; set; }
        public Decimal UserAmount { get; set; }
        public Decimal AdvertiserAmount { get; set; }
        public Decimal KiglAmount { get; set; }
        public Decimal UserPercent { get; set; }
        public Decimal AdvertiserPercent { get; set; }

        public UserAd UserAd { get; set; }
        public Currency Currency { get; set; }
    }
}

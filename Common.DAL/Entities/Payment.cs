﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kigl.Common.Enums;

namespace Common.DAL.Entities
{
    public class Payment : BaseEntity
    {
        public Guid AdvertiserId { get; set; }
        public Guid CurrencyId { get; set; }

        public String ProviderId { get; set; }
        public DateTime Timestamp { get; set; }
        public Decimal Amount { get; set; }
        public String SyncResult { get; set; }
        public String AsyncResult { get; set; }
        public PaymentState State { get; set; }

        public Advertiser Advertiser { get; set; }
        public Currency Currency { get; set; }
    }
}

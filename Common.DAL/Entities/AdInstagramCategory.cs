﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DAL.Contracts;

namespace Common.DAL.Entities
{
    public class AdInstagramCategory : IEntityInstagramCategory
    {
        public Guid AdId { get; set; }
        public Guid InstagramCategoryId { get; set; }

        public Ad Ad { get; set; }
        public InstagramCategory InstagramCategory { get; set; }
    }
}

﻿using System;

namespace Common.DAL.Entities
{
    public class PasswordRecoveryLink : BaseEntity
    {
        public Guid UserId { get; set; }

        public string Code { get; set; }
        public DateTime Timestamp { get; set; }

        public User User { get; set; }
    }
}

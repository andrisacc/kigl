﻿using System;
using System.Collections.Generic;

namespace Common.DAL.Entities
{
    public class Country : BaseEntity
    {
        public Guid RatesId { get; set; }

        public string Name { get; set; }

        public Rates Rates { get; set; }

        public ICollection<City> Cities { get; set; }
    }
}

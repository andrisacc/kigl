﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.DAL.Entities
{
    public class InstagramCategory : BaseEntity
    {
        public string Name { get; set; }
    }
}

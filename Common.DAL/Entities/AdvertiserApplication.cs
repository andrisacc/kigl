﻿using System;

namespace Common.DAL.Entities
{
    public class AdvertiserApplication
    {
        public Guid AdvertiserId { get; set; }
        public Guid ApplicationId { get; set; }

        public int NumberOfInstalls { get; set; }

        public Advertiser Advertiser { get; set; }
        public Application Application { get; set; }
    }
}

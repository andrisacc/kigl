﻿namespace Common.DAL.Entities
{
    public class Rates : BaseEntity
    {
        public decimal CostPerClick { get; set; }
        public decimal CostPerImpression { get; set; }
        public decimal CostPerInstall { get; set; }
    }
}

﻿using System;

namespace Common.DAL.Entities
{
    public class Account : BaseEntity
    {
        public Guid AdvertiserId { get; set; }
        public Guid CurrencyId { get; set; }

        public string Number { get; set; }
        public decimal Balance { get; set; }
        public string Reference { get; set; }

        public Advertiser Advertiser { get; set; }
        public Currency Currency { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DAL.Contracts;
using ControlPanel.Common.Enums;
using Kigl.Common.Enums;

namespace Common.DAL.Entities
{
    public class Ad : BaseEntity, INetworksEntity<AdNetwork>, IInstagramCategoriesEntity<AdInstagramCategory>
    {
        public Guid AdvertiserId { get; set; }

        public string Name { get; set; }
        public string Url { get; set; }
        public string Caption { get; set; }
        public bool IsActive { get; set; }
        public bool IsNew { get; set; }
        public decimal? Budget { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public string Status => IsActive ? "Active" : "Stopped";

        public AdState State { get; set; }

        public decimal Amount
        {
            get
            {
                var successUserAds = UserAds.Where(userAd => userAd.VerifyStatus == VerifyStatus.Success).ToArray();
                return successUserAds.Any() ? successUserAds.Sum(userAd => userAd.AdvertiserAmount) : 0.00m;
            }
        }

        public Advertiser Advertiser { get; set; }
        public AdType AdType { get; set; }

        public ICollection<File> Files { get; set; }
        public ICollection<AdNetwork> EntityNetworks { get; set; }
        public ICollection<UserAd> UserAds { get; set; }
        public ICollection<AdInstagramCategory> InstagramCategories { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DAL.Contracts;

namespace Common.DAL.Entities
{
    public class UserInstagramCategory : IEntityInstagramCategory
    {
        public Guid UserId { get; set; }
        public Guid InstagramCategoryId { get; set; }

        public User User { get; set; }
        public InstagramCategory InstagramCategory { get; set; }
    }
}

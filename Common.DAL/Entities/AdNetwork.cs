﻿using System;
using Common.DAL.Contracts;
using ControlPanel.Common.Enums;

namespace Common.DAL.Entities
{
    public class AdNetwork : IEntityNetwork
    {
        public Guid AdId { get; set; }
        public Guid NetworkId { get; set; }

        public int NumberOfClicks { get; set; }
        public int NumberOfImpressions { get; set; }
        public int Index { get; set; }
        public AdStatus AdStatus { get; set; }
        public decimal? Budget { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsActive { get; set; }
        public bool IsStopped { get; set; }

        public Ad Ad { get; set; }
        public Network Network { get; set; }
    }
}

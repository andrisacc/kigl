﻿using System;
using System.Collections.Generic;
using Common.DAL.Contracts;

namespace Common.DAL.Entities
{
    public class ReferralUser : BaseEntity
    {
        public Guid UserId { get; set; }
        public Guid InvitedUserId { get; set; }

        public User User { get; set; }
        public User InvitedUser { get; set; }
    }
}

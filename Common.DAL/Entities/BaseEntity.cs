﻿using System;

namespace Common.DAL.Entities
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}

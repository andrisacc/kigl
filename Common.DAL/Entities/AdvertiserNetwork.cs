﻿using System;
using Common.DAL.Contracts;

namespace Common.DAL.Entities
{
    public class AdvertiserNetwork : IEntityNetwork
    {
        public Guid AdvertiserId { get; set; }
        public Guid NetworkId { get; set; }

        public Advertiser Advertiser { get; set; }
        public Network Network { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dropbox.Api;
using Dropbox.Api.Files;
using Kigl.Domain.Contracts;

namespace Kigl.Dropbox
{
    public class DropboxService : IDropboxService
    {
        public async Task<string> UploadAndCreateSharedLink(string path)
        {
            using (var dbx = new DropboxClient(ConfigurationManager.AppSettings["DropboxAccessToken"]))
            {
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    var dropboxPath = "/" + Path.GetFileName(path);
                    await dbx.Files.UploadAsync(new CommitInfo(dropboxPath, WriteMode.Overwrite.Instance), stream);
                    var sharedLinkMetadata = await dbx.Sharing.CreateSharedLinkWithSettingsAsync(dropboxPath);
                    return
                    sharedLinkMetadata.Url;
                }
            }
        }
    }
}

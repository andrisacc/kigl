﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using Kigl.Common.Enums;
using Kigl.Common.Helpers;
using Common.DAL.Contracts;
using Common.DAL.Entities;
using ControlPanel.Common.Enums;
using ControlPanel.Common.Models;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Domain.Models;
using Kigl.Domain.Models.Instaram;
using Kigl.Common.Models;

namespace Kigl.Services
{
    public class ContentService : IContentService
    {
        private readonly IDataContext _dataContext;
        private readonly IInstagramParserService _instagramParserService;
        private readonly ISettingsService _settingsService;
        private readonly IUserService _userService;


        public ContentService(IDataContext dataContext, IInstagramParserService instagramParserService, ISettingsService settingsService, IUserService userService)
        {
            _dataContext = dataContext;
            _instagramParserService = instagramParserService;
            _settingsService = settingsService;
            _userService = userService;
        }

        public Ad GetAd(Guid id)
        {
            return _dataContext.Ads
                .Include(item => item.Files)
                .First(item => item.Id == id);
        }

        private IQueryable<Ad> GetAdForShowingQuery(User user)
        {
            var followersBalance = GetFollowersBalance(user.InstagramLogin);
            var query = _dataContext.Ads
                .Include(item => item.Files)
                .Include(item => item.UserAds)
                .Include(item => item.Advertiser.Accounts)
                .Where(item => item.IsActive && !item.IsNew && (item.AdType == AdType.InstagramImage || item.AdType == AdType.InstagramVideo)
                    && !item.UserAds.Select(ua => ua.UserId).Contains(user.Id)
                    && (!item.Budget.HasValue || item.Budget - followersBalance.AdvertiserBalance > 0));
            var categoryIds = user.InstagramCategories.Select(item => item.InstagramCategoryId).ToArray();
            if (categoryIds.Any())
                query = query.Where(item => item.InstagramCategories.Any(category => categoryIds.Contains(category.InstagramCategoryId)));
            return query;
        }

        public bool HasAdForShowing(User user)
        {
            return GetAdForShowingQuery(user).Any();
        }

        public bool HasWithdraws(User user)
        {
            return _dataContext.WithdrawRequests.Where(wr=>wr.UserId == user.Id).Any();
        }

        public Ad[] GetAdForShowing(User user)
        {
            return GetAdForShowingQuery(user)
                .Take(3)
                .ToArray();
        }

        public Ad[] GetUserAds(Guid userId)
        {
            return _dataContext.UserAds
               .Where(item =>  item.UserId == userId)
               .OrderBy(item => item.VerifyDate)
               .Select(item => item.Ad)
               .Include(item => item.Files)
               .Include(item => item.UserAds)
               .ToArray();
        }

        public void AddOrUpdateAd(Ad ad)
        {
            if (ad.Id == Guid.Empty)
            {                
                ad.Id = Guid.NewGuid();
                foreach (var file in ad.Files)
                {
                    if (!string.IsNullOrWhiteSpace(file.FileName))
                    {
                        file.Id = Guid.NewGuid();
                        file.AdId = ad.Id;
                        _dataContext.Files.Add(file);
                    }
                }
                _dataContext.Ads.Add(ad);
            }
            else
            {
                var dbAd = _dataContext.Ads
                    .Include(item => item.Files)
                    .FirstOrDefault(item => item.Id == ad.Id);
                if (dbAd != null)
                {
                    dbAd.Name = ad.Name;
                    dbAd.Caption = ad.Caption;
                    foreach (var file in ad.Files)
                    {
                        var dbFile = dbAd.Files.FirstOrDefault();
                        if (dbFile != null)
                        {
                            dbFile.FileName = file.FileName;
                            dbFile.UploadedFileName = file.UploadedFileName;
                        }
                    }
                }
            }
            _dataContext.SaveChanges();
        }

        public File GetFileByAdId(Guid id)
        {
            return _dataContext.Ads
                .Include(item => item.Files)
                .FirstOrDefault(item => item.Id == id)
                .Files.FirstOrDefault(item => item.FileAdType != FileAdType.Thumbnail);
        }

        public void AddUserAd(Guid userId, Guid adId, int followersCount)
        {
            if (!_dataContext.UserAds.Where(u => u.AdId == adId && u.UserId == userId).Any())
            {
                var userAd = new UserAd
                {
                    Id = Guid.NewGuid(),
                    UserId = userId,
                    AdId = adId,
                    VerifyStatus = VerifyStatus.Downloaded,
                    FollowersCount = followersCount
                };
                _dataContext.UserAds.Add(userAd);
                _dataContext.SaveChanges();
            }
        }

        public VerifyStatus UpdateVerifyStatus(bool verifyResult, User user, Guid adId)
        {
            var userId = user.Id;
            var userAd = _dataContext.UserAds
                .Include(item => item.Ad)
                .FirstOrDefault(item => item.UserId == userId && item.AdId == adId);
            if (verifyResult)
            {
                var followersBalance = GetFollowersBalance(user.InstagramLogin);
                var ad = userAd.Ad;
                if (ad.IsActive && (!ad.Budget.HasValue || ad.Budget - followersBalance.AdvertiserBalance > 0))
                {
                    userAd.VerifyDate = DateTime.UtcNow;
                    var checkInterval = Parser.ParseTimeSpan(_dataContext.Settings.FirstOrDefault(item => item.Name == Settings.CheckInterval).Value);
                    var instagramerPercent = Int32.Parse(_dataContext.Settings.FirstOrDefault(item => item.Name == Settings.InstagramerPercent).Value);
                    var timeToNextVerify = new Random().Next(checkInterval);
                    userAd.NextVerifyDate = userAd.VerifyDate.Value.AddMinutes(timeToNextVerify);
                    userAd.VerifyStatus = VerifyStatus.InProgress;
                    userAd.UserAmount = followersBalance.UserBalance;
                    userAd.UserPercent = ((decimal)instagramerPercent)/ 100;
                    userAd.AdvertiserAmount = followersBalance.AdvertiserBalance;
                    userAd.AdvertiserPercent = 1;
                    userAd.Ad.Budget -= userAd.AdvertiserAmount;
                }
                else
                {
                    userAd.VerifyStatus = VerifyStatus.CampaignStopped;
                }
            }
            else
            {
                userAd.VerifyStatus = VerifyStatus.Failed;
            }
            _dataContext.SaveChanges();
            return userAd.VerifyStatus;
        }

        public void ResetVerifyStatus(Guid userId, Guid adId)
        {
            var userAd = _dataContext.UserAds.FirstOrDefault(item => item.UserId == userId && item.AdId == adId);
            if (userAd != null)
            {
                userAd.VerifyStatus = VerifyStatus.Downloaded;
                userAd.VerifyDate = null;
                userAd.NextVerifyDate = null;
            }
            _dataContext.SaveChanges();
        }

        public void AddWithdrawRequest(string account, WithdrawType type, Guid userId)
        {
            _dataContext.WithdrawRequests.Add(new WithdrawRequest
            {
                Id = Guid.NewGuid(),
                Account = account,
                WithdrawType = type,
                UserId = userId,
                Timestamp = DateTime.UtcNow
            });

            var user = _dataContext.Users.Where(u => u.Id == userId).FirstOrDefault();

            if(user != null)
            {
                user.Paypal = account;
            }

            _dataContext.SaveChanges();
        }

        public void UpdatePayPal(string account, Guid userId)
        {
            var user = _dataContext.Users.Where(u => u.Id == userId).FirstOrDefault();

            if (user != null)
            {
                user.Paypal = account;
            }

            _dataContext.SaveChanges();
        }

        public void UpdatePhone(string phone, Guid userId)
        {
            var user = _dataContext.Users.Where(u => u.Id == userId).FirstOrDefault();

            if (user != null)
            {
                user.Phone = phone;
            }

            _dataContext.SaveChanges();
        }

        private Balance GetFollowersBalance(string instagramLogin)
        {
            FollowersInfo followersInfo = null;
            try
            {
                followersInfo = _instagramParserService.GetFollowers(instagramLogin);
            }
            catch (Exception exc)
            {
                followersInfo = _userService.GetFollowers(instagramLogin);
            }

            if (followersInfo == null)
                followersInfo = _userService.GetFollowers(instagramLogin);

            var result = new Balance()
            {
                UserBalance = _settingsService.GetUserFollowersBalance(followersInfo.FollowedBy),
                AdvertiserBalance = _settingsService.GetAdvertiserFollowersBalance(followersInfo.FollowedBy),

            };

            return result;
        }

        public TransactionsPage GetPayments(Guid userId, int payment_counts, DateTime? startDate, DateTime? endDate, int? paymentType, int page, String lang)
        {
            var transactions = _dataContext.AdTransactions.Include(t => t.UserAd).Where(ta => ta.UserAd.UserId == userId);
            var rewards = _dataContext.ReferralRewards.Include(t => t.UserAd).Where(rw => rw.InvitingUserId == userId);

            var outputTransactions = transactions
                .ToList()
                .Select(t => new Transaction()
                {
                    AdId = t.UserAdId.ToString().Replace("-", ""),
                    Amount = t.UserAmount,
                    Date = t.Timestamp,
                    DateStr = t.Timestamp.ToString("dd MMMM yyyy", lang == "Russian" ? CultureInfo.CreateSpecificCulture("ru-Ru") : CultureInfo.CreateSpecificCulture("en-US")),
                    PaymentType = "1"
                });

            var outputRewards = rewards
                .ToList()
                .Select(t => new Transaction()
                {
                    AdId = t.UserAd.AdId.ToString().Replace("-", ""),
                    Amount = t.Reward,
                    Date = t.UserAd.NextVerifyDate.Value,
                    DateStr = t.UserAd.NextVerifyDate.Value.ToString("dd MMMM yyyy", lang == "Russian" ? CultureInfo.CreateSpecificCulture("ru-Ru") : CultureInfo.CreateSpecificCulture("en-US")),
                    PaymentType = "2"
                });

            var output = outputTransactions.Union(outputRewards);

            if (paymentType.HasValue)
            {
                output = output.Where(o => o.PaymentType == paymentType.Value.ToString());
            }

            if (startDate.HasValue && endDate.HasValue)
            {
                output = output.Where(o => o.Date.Date >= startDate.Value.Date && o.Date.Date <= endDate.Value.Date);
            }

            var pagesCount = (int) (output.Count() / payment_counts) + 1;

            output = output.OrderBy(t => t.Date).Skip((page - 1)* payment_counts).Take(payment_counts);

            TransactionsPage transactionPage = new TransactionsPage();
            transactionPage.PageNum = page;
            transactionPage.PagesCount = pagesCount;
            transactionPage.Transactions = output.ToArray();

            return transactionPage;
        }
    }
}

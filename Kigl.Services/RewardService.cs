﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using Kigl.Common.Enums;
using Kigl.Common.Helpers;
using Common.DAL.Contracts;
using Common.DAL.Entities;
using ControlPanel.Common.Enums;
using ControlPanel.Common.Models;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Domain.Models;
using Kigl.Domain.Models.Instaram;

namespace Kigl.Services
{
    public class RewardService : IRewardService
    {
        private readonly IDataContext _dataContext;

        public RewardService(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void PayReward(Guid userAdId, Decimal instagramerRewardPercent, Decimal advertiserRewardPercent)
        {
            var userAd = _dataContext.UserAds
                .Include(item=>item.Ad)
                .Where(ad => ad.Id == userAdId)
                .FirstOrDefault();

            if (userAd != null)
            {
                var transaction = new AdTransaction()
                {
                    Id = Guid.NewGuid(),
                    UserAdId = userAd.Id,
                    AdvertiserAmount = userAd.AdvertiserAmount,
                    AdvertiserPercent = userAd.AdvertiserPercent,
                    KiglAmount = userAd.AdvertiserAmount - userAd.UserAmount,
                    Timestamp = DateTime.Now,
                    UserAmount = userAd.UserAmount,
                    UserPercent = userAd.UserPercent,
                    CurrencyId = new System.Guid("4a215876-053d-41f3-b282-42b325ee5740")
                };

                _dataContext.AdTransactions.Add(transaction);

                PayInstagramerForInstagramerReward(userAd, instagramerRewardPercent, transaction);

                PayInstagramerForAdvertiserReward(userAd, advertiserRewardPercent, transaction);

                PayAdvertiserForAdvertiserReward(userAd, advertiserRewardPercent, transaction);
            }
        }

        private void PayInstagramerForInstagramerReward(UserAd userAd, Decimal instagramerRewardPercent, AdTransaction transaction)
        {
            Decimal kiglIncome = userAd.AdvertiserAmount - userAd.UserAmount;
            ReferralReward invitingInstagramerReward = new ReferralReward() { Id = Guid.NewGuid() };

            var stopReferralDate = DateTime.Now.AddYears(-1);

            var invitingInstagramer = _dataContext.ReferralUsers
                .Include(item => item.User)
                .Where(item => item.InvitedUser.Id == userAd.UserId && item.InvitedUser.Registred > stopReferralDate)
                .Select(item => item.User)
                .FirstOrDefault();

            if (invitingInstagramer != null && !String.IsNullOrEmpty(invitingInstagramer.InstagramLogin))
            {
                invitingInstagramerReward.InvitingUser = invitingInstagramer;
                invitingInstagramerReward.UserAd = userAd;
                invitingInstagramerReward.TransactionId = transaction.Id;
                invitingInstagramerReward.Reward = userAd.UserAmount * instagramerRewardPercent;
                _dataContext.ReferralRewards.Add(invitingInstagramerReward);

                transaction.KiglAmount -= invitingInstagramerReward.Reward;
            }
        }

        private void PayInstagramerForAdvertiserReward(UserAd userAd, Decimal advertiserRewardPercent, AdTransaction transaction)
        {
            Decimal kiglIncome = userAd.AdvertiserAmount - userAd.UserAmount;
            ReferralReward invitingInstagramerReward = new ReferralReward() { Id = Guid.NewGuid() };

            var stopReferralDate = DateTime.Now.AddMonths(-3);

            var invitingInstagramer = _dataContext.ReferralUsers
                .Include(item => item.User)
                .Where(item => item.InvitedUser.AdvertiserId == userAd.Ad.AdvertiserId && item.InvitedUser.Registred > stopReferralDate)
                .Select(item => item.User)
                .FirstOrDefault();

            if (invitingInstagramer != null && invitingInstagramer.AdvertiserId == null)
            {
                invitingInstagramerReward.InvitingUser = invitingInstagramer;
                invitingInstagramerReward.UserAd = userAd;
                invitingInstagramerReward.TransactionId = transaction.Id;
                invitingInstagramerReward.Reward = userAd.UserAmount * advertiserRewardPercent;
                _dataContext.ReferralRewards.Add(invitingInstagramerReward);

                transaction.KiglAmount -= invitingInstagramerReward.Reward;
            }
        }

        private void PayAdvertiserForAdvertiserReward(UserAd userAd, Decimal advertiserRewardPercent, AdTransaction transaction)
        {
            Decimal kiglIncome = userAd.AdvertiserAmount - userAd.UserAmount;
            ReferralReward invitingAdvertiserReward = new ReferralReward() { Id = Guid.NewGuid() };

            var stopReferralDate = DateTime.Now.AddMonths(-3);

            var invitingAdvertiser = _dataContext.ReferralUsers
                .Include(item => item.User)
                .Where(item => item.InvitedUser.AdvertiserId == userAd.Ad.AdvertiserId && item.InvitedUser.Registred > stopReferralDate)
                .Select(item => item.User)
                .FirstOrDefault();

            if (invitingAdvertiser != null && invitingAdvertiser.AdvertiserId.HasValue)
            {
                invitingAdvertiserReward.InvitingUser = invitingAdvertiser;
                invitingAdvertiserReward.UserAd = userAd;
                invitingAdvertiserReward.TransactionId = transaction.Id;
                invitingAdvertiserReward.Reward = userAd.AdvertiserAmount * advertiserRewardPercent;
                _dataContext.ReferralRewards.Add(invitingAdvertiserReward);

                var invitingAdvertiserAccount = _dataContext.Advertisers
                    .Include(item => item.Accounts)
                    .Where(item => item.Id == invitingAdvertiser.AdvertiserId.Value)
                    .Select(item => item.Accounts)
                    .FirstOrDefault();

                if (invitingAdvertiserAccount.FirstOrDefault() != null)
                {
                    invitingAdvertiserAccount.FirstOrDefault().Balance += invitingAdvertiserReward.Reward;

                    transaction.KiglAmount -= invitingAdvertiserReward.Reward;
                }
            }
        }
    }
}

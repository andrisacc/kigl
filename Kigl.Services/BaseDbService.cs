﻿using Common.DAL.Contracts;

namespace Kigl.Services
{
    public class BaseDbService
    {
        protected readonly IDataContext DataContext;

        public BaseDbService(IDataContext dataContext)
        {
            DataContext = dataContext;
        }

        public void SaveChanges()
        {
            DataContext.SaveChanges();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Kigl.Domain.Contracts;
using Common.Logging;

namespace Kigl.Services
{
    public class EmailService : IEmailService
    {
        private static readonly ILog Log = LogManager.GetLogger<EmailService>();

        public void SendEmail(string to, string subject, string body, Stream attachment, string attachmentName)
        {
            Log.Info($"Sending email to:{to}; subject:{subject};");

            const String FROM = "no-reply@kigl.eu";
            // Supply your SMTP credentials below. Note that your SMTP credentials are different from your AWS credentials.
            const String SMTP_USERNAME = "AKIAJN4OJ6K3UMXRAKPQ";  // Replace with your SMTP username. 
            const String SMTP_PASSWORD = "AsDLcwC/iwhduhDzw7Azbo5+giO0jDhHzqQYHgZLKg2e";  // Replace with your SMTP password.

            // Amazon SES SMTP host name. This example uses the US West (Oregon) region.
            const String HOST = "email-smtp.us-west-2.amazonaws.com";

            // The port you will connect to on the Amazon SES SMTP endpoint. We are choosing port 587 because we will use
            // STARTTLS to encrypt the connection.
            const int PORT = 587;

            using (var message = new MailMessage())
            {
                message.From = new MailAddress(FROM);
                message.To.Add(new MailAddress(to));
                message.IsBodyHtml = true;
                message.Body = body;
                message.Subject = subject;
                var smtpClient = new SmtpClient(HOST, PORT)
                {
                    Credentials = new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD),
                    EnableSsl = true,
                };

                if (attachment != null)
                {
                    message.Attachments.Add(new Attachment(attachment, attachmentName));
                }

                try
                {
                    smtpClient.Send(message);
                    Log.Info($"Email sent successfully to:{to}; subject:{subject};");
                }
                catch (Exception ex)
                {
                    Log.Error(ex.Message, ex);
                }

            }
        }
    }
}

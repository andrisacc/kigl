﻿using System;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using Common.DAL.Contracts;
using Common.DAL.Entities;
using Kigl.Common.Enums;
using Kigl.Domain.Contracts;
using AutoMapper.QueryableExtensions;
using ControlPanel.Services;
using Kigl.Domain.Models.Instaram;

namespace Kigl.Services
{
    public class UserService : InstagramCategoryService, IUserService
    {
        protected readonly IDataContext DataContext;

        public UserService(IDataContext dataContext)
        {
            DataContext = dataContext;
        }

        public ControlPanel.Domain.Models.User[] GetUsers(Guid? advertiserId, bool isInstagramAdmin)
        {
            var query = DataContext.Users
                .Include(item => item.InstagramCategories.Select(category => category.InstagramCategory));
            if (advertiserId.HasValue)
            {
                query = query.Where(item => item.AdvertiserId == advertiserId.Value);
            }
            if (isInstagramAdmin)
            {
                query = query.Where(item => item.FacebookId != null);
            }
            return query.ProjectTo<ControlPanel.Domain.Models.User>().ToArray();
        }

        public ControlPanel.Domain.Models.User[] GetInstagrammers(Guid? advertiserId, bool isInstagramAdmin)
        {
            var query = DataContext.Users
                .Include(item => item.InstagramCategories.Select(category => category.InstagramCategory));
            if (advertiserId.HasValue)
            {
                query = query.Where(item => item.AdvertiserId == advertiserId.Value);
            }
            if (isInstagramAdmin)
            {
                query = query.Where(item => item.FacebookId != null);
            }

            var withdrawQuery = DataContext.WithdrawRequests.ToArray();
            var users = query.ProjectTo<ControlPanel.Domain.Models.User>().ToArray();

            ////users = users.
            ////    Join(withdrawQuery, u => u.Id, w => w.UserId, (u, w) => new { u, w }).
            ////    Select(r => { r.u.WithdrawAccount = r.w.Account; return r.u; }).
            ////    ToArray();

            //users = users.GroupJoin(
            //    withdrawQuery,
            //    foo => foo.Id,
            //    bar => bar.UserId,
            //    (x, y) => new { Foo = x, Bars = y }).
            //SelectMany(
            //    x => x.Bars.DefaultIfEmpty(),
            //    (x, y) => { x.Foo.WithdrawAccount = y==null ?"":y.Account ; return x.Foo; }).
            //OrderBy(x=>x.Registred).
            //ToArray();

            return users;
        }

        public User GetById(string id)
        {
            Guid userId;
            return Guid.TryParse(id, out userId) ? 
                DataContext.Users
                .Include(item => item.InstagramCategories)
                .FirstOrDefault(item => item.Id == userId) : null;
        }

        public User GetByFacebookId(string facebookId)
        {
            return DataContext.Users.FirstOrDefault(item => item.FacebookId == facebookId);
        }

        public User Create(string facebookId, string name)
        {
            var referralCode = GenerateReferralCode();

            if (referralCode != null)
            {
                var user = new User
                {
                    Id = Guid.NewGuid(),
                    FacebookId = facebookId,
                    Name = name,
                    Registred = DateTime.Now,
                    ReferralCode = referralCode
                };

                DataContext.Users.Add(user);
                return user;
            }

            return null;
        }

        public User Create(string username, string name, string password, Guid? advertiserId, Guid? networkId, string roleName)
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                Name = name,
                Username = username,
                AdvertiserId = advertiserId,
                NetworkId = networkId,
                PasswordHash = CreateHash(password),
                Registred = DateTime.Now
            };
            DataContext.Users.Add(user);
            var role = DataContext.Roles.FirstOrDefault(item => item.Name == roleName);
            if (role != null)
            {
                DataContext.UserRoles.Add(new UserRole
                {
                    UserId = user.Id,
                    RoleId = role.Id
                });
            }
            return user;
        }

        public string UpdatePassword(Guid userId, string password)
        {
            var user = DataContext.Users.FirstOrDefault(item => item.Id == userId);
            if(user != null)
            {
                user.PasswordHash = CreateHash(password);
                return user.Email;
            }
            return null;
        }

        public User Create(string username, string name, string email, string password, Guid? advertiserId, Guid? networkId, string roleName, string lang)
        {
            var user = new User
            {
                Id = Guid.NewGuid(),
                Name = name,
                Username = username,
                AdvertiserId = advertiserId,
                NetworkId = networkId,
                PasswordHash = CreateHash(password),
                Email = email,
                RegistrationLang = lang,
                Registred = DateTime.Now,
                ReferralCode = GenerateReferralCode()
        };
            DataContext.Users.Add(user);
            var role = DataContext.Roles.FirstOrDefault(item => item.Name == roleName);
            if (role != null)
            {
                DataContext.UserRoles.Add(new UserRole
                {
                    UserId = user.Id,
                    RoleId = role.Id
                });
            }
            return user;
        }

        public void Update(Guid id, string instagramLogin, string email, string phone, bool terms, String lang)
        {
            var user = DataContext.Users.First(item => item.Id == id);
            user.InstagramLogin = instagramLogin;
            user.Phone = phone;
            user.Email = email;
            user.Terms = terms;
            user.RegistrationLang = lang;
        }

        public void UpdatePic(Guid id, string picUrl)
        {
            var user = DataContext.Users.First(item => item.Id == id);
            user.PhotoUrl = picUrl;
        }

        public void Update(Guid id, string instagramLogin, string email, string phone, bool terms, String lang, String referralUserCode)
        {
            var user = DataContext.Users.First(item => item.Id == id);
            user.InstagramLogin = instagramLogin;
            user.Phone = phone;
            user.Email = email;
            user.Terms = terms;
            user.RegistrationLang = lang;

            var referralUser = DataContext.Users.Where(u => u.ReferralCode == referralUserCode).FirstOrDefault();
            if (referralUser != null)
            {
                DataContext.ReferralUsers.Add(new ReferralUser()
                {
                    Id = Guid.NewGuid(),
                    InvitedUser = user,
                    User = referralUser
                });
            }
        }

        public void Update(Guid id, Guid[] instagramCategories)
        {
            var user = DataContext.Users.Include(item => item.InstagramCategories).First(item => item.Id == id);
            ApplyCategories(user, instagramCategories);
        }

        public void Delete(Guid id)
        {
            var user = DataContext.Users.Where(item => item.Id == id).FirstOrDefault();
            if (user != null)
            {
                DataContext.Users.Remove(user);
            }
        }

        public User Check(string username, string password)
        {
            var user = DataContext.Users.FirstOrDefault(item => item.Username == username);
            if (user != null && !CheckPassword(password, user.PasswordHash))
            {
                user = null;
            }
            return user;
        }

        public User CheckRecoveryCode(string code)
        {
            var stoptime = DateTime.Now.AddMinutes(-30);

            var user = DataContext
                .PasswordRecoveryLinks
                .Include(item=>item.User)
                .FirstOrDefault(item => item.Code == code && stoptime < item.Timestamp)
                .User;
            
            return user;
        }

        public string GetPasswordRecoveryCode(string email)
        {
            var user = DataContext.Users
                .FirstOrDefault(item => item.Email == email && item.InstagramLogin == null);
            if (user != null)
            {
                var code = GenerateRecoveryPasswordCode();

                DataContext.PasswordRecoveryLinks.Add(new PasswordRecoveryLink()
                {
                    Code = code,
                    Id = Guid.NewGuid(),
                    Timestamp = DateTime.Now,
                    UserId = user.Id
                });

                return code;
            }
            return null;
        }

        public string GetPasswordRecoveryLang(string email)
        {
            var user = DataContext.Users
                .FirstOrDefault(item => item.Email == email && item.InstagramLogin == null);
            if (user != null)
            {
                return user.RegistrationLang;
            }
            return null;
        }

        public string[] GetRoles(Guid userId)
        {
            return DataContext.UserRoles.Include(item => item.Role)
                .Where(item => item.UserId == userId)
                .Select(item => item.Role.Name).ToArray();
        }

        public Guid[] GetUserInstagramCategories(Guid userId)
        {
            return DataContext.UserInstagramCategories.Where(item => item.UserId == userId)
                .Select(item => item.InstagramCategoryId).ToArray();
        }

        public String GetUserLastWithdrawAccount(Guid userId)
        {
            var lastWithdraw = DataContext.WithdrawRequests.Where(w => w.UserId == userId).OrderBy(u => u.Timestamp).FirstOrDefault();
            return lastWithdraw != null ? lastWithdraw.Account : "";
        }

        public bool ValidateUserName(string name)
        {
            return !DataContext.Users.Any(item => item.Name == name);
        }

        public bool ValidateAdvertiserEmail(string email)
        {
            return !(DataContext.Users.Any(item => item.Email == email || item.Username == email) || DataContext.Advertisers.Any(item=>item.Email == email));
        }

        public decimal GetBalance(Guid userId)
        {
            var campaignSum = DataContext.UserAds.Where(item => item.UserId == userId && item.VerifyStatus == VerifyStatus.Success)
                .Sum(item => (decimal?)item.UserAmount) ?? 0;

            var referralRewardSum = DataContext.ReferralRewards.Where(rr => rr.InvitingUserId == userId).Sum(item => (decimal?)item.Reward) ?? 0;

            return campaignSum + referralRewardSum;
        }

        private string CreateHash(string password)
        {
            byte[] salt;
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);
            var hash = GetHashBytes(password, salt);
            var hashBytes = new byte[36];
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);
            return Convert.ToBase64String(hashBytes);
        }

        private bool CheckPassword(string password, string hash)
        {
            var hashBytes = Convert.FromBase64String(hash);
            var salt = new byte[16];
            Array.Copy(hashBytes, 0, salt, 0, 16);
            var passwordHash = GetHashBytes(password,salt);
            for (var i = 0; i < 20; i++)
                if (hashBytes[i + 16] != passwordHash[i])
                    return false;
            return true;
        }

        private byte[] GetHashBytes(string password, byte[] salt)
        {
            var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 1000);
            return pbkdf2.GetBytes(20);
        }

        public void SaveChanges()
        {
            DataContext.SaveChanges();
        }

        public FollowersInfo GetFollowers(Guid userId)
        {
            var user = DataContext.Users.Where(u => u.Id == userId).FirstOrDefault();
            if (user == null)
            {
                return null;
            }

            var followersInfo = new FollowersInfo()
            {
                FollowedBy = user.FollowersCount,
                Follows = user.FollowesCount,
                IsLastFollowersRequestSuccess = user.IsLastFollowersRequestSuccess
            };

            return followersInfo;
        }

        public FollowersInfo GetFollowers(String instagramLogin)
        {
            var followersInfo = new FollowersInfo();

            var user = DataContext.Users.Where(u => u.InstagramLogin == instagramLogin).FirstOrDefault();
            if (user == null)
            {
                followersInfo.FollowedBy = 0;
                followersInfo.Follows = 0;
                followersInfo.IsLastFollowersRequestSuccess = false;
            }
            else
            {
                followersInfo.FollowedBy = user.FollowersCount;
                followersInfo.Follows = user.FollowesCount;
                followersInfo.IsLastFollowersRequestSuccess = user.IsLastFollowersRequestSuccess;
            };

            return followersInfo;
        }

        public string GenerateRecoveryPasswordCode()
        {
            var random = new Random();
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";

            for (int attempt = 1; attempt <= 1000; attempt++)
            {
                var code = new string(Enumerable.Repeat(chars, 10)
                  .Select(s => s[random.Next(s.Length)]).ToArray());

                if (!DataContext.PasswordRecoveryLinks.Any(u => u.Code == code))
                    return code;
            }
            return null;
        }

        public string GenerateReferralCode()
        {
            var random = new Random();
            const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";

            for (int attempt = 1; attempt <= 1000; attempt++)
            {
                var code = new string(Enumerable.Repeat(chars, 10)
                  .Select(s => s[random.Next(s.Length)]).ToArray());

                if (!DataContext.Users.Any(u => u.ReferralCode == code))
                    return code;
            }
            return null;
        }
    }
}

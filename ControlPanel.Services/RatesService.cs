﻿using System;
using System.Linq;
using AutoMapper;
using Common.DAL.Contracts;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;

namespace ControlPanel.Services
{
    public class RatesService : IRatesService
    {
        private readonly IDataContext _dataContext;

        public RatesService(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public Rates GetRates(Guid countryId)
        {
            var rates = _dataContext.Countries
                .Include(item => item.Rates)
                .First(item => item.Id == countryId).Rates;
            return Mapper.Map<Rates>(rates);
        }

        public void UpdateRates(Rates model)
        {
            var rates = _dataContext.Rates.FirstOrDefault(item => item.Id == model.Id);
            Mapper.Map(model, rates);
            _dataContext.SaveChanges();
        }
    }
}

﻿using System;
using Common.DAL;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Common.DAL.Contracts;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;
using Entities = Common.DAL.Entities;
using AdvertiserDo = Common.DAL.Entities.Advertiser;
using AccountDo = Common.DAL.Entities.Account;
using UserDo = Common.DAL.Entities.User;

namespace ControlPanel.Services
{
    public class AccountsService : IAccountsService
    {
        private readonly IDataContext _dataContext;

        public AccountsService(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public Account GetAccount(Guid id)
        {
            var account = _dataContext.Accounts.FirstOrDefault(item => item.Id == id);
            return Mapper.Map<Account>(account);
        }

        public AccountDo GetAdvertiserAccount(AdvertiserDo advertiser)
        {
            return advertiser.Accounts.FirstOrDefault();
        }

        public Account GetAccountByAdvertiserId(Guid advertiserId)
        {
            var account = _dataContext.Accounts.Where(acc => acc.AdvertiserId == advertiserId).FirstOrDefault();
            return Mapper.Map<Account>(account);
        }

        public Account[] GetAccounts()
        {
            return _dataContext.Accounts.ProjectTo<Account>().ToArray();
        }

        public void AddOrUpdateAccount(Account model)
        {
            AccountDo account;
            if (model.Id == Guid.Empty)
            {
                account = Mapper.Map<AccountDo>(model);
                account.Id = Guid.NewGuid();
                string reference;
                do
                {
                    reference = GenerateReference();
                } while (_dataContext.Accounts.Any(item => item.Reference == reference));
                account.Reference = reference;
                _dataContext.Accounts.Add(account);
            }
            else
            {
                account = _dataContext.Accounts.FirstOrDefault(item => item.Id == model.Id);
                Mapper.Map(model, account);
            }
            _dataContext.SaveChanges();
        }

        private string GenerateReference()
        {
            var random = new Random();
            return random.Next(10000000, 99999999).ToString();
        }

        class Ua
        {
            public Guid UserId { get; set; }
            public Guid AdId { get; set; }
        }

        public UserNewAdEmail[] GetEmailsForNewAdEmailSending()
        {
            var users = ((DataContext)_dataContext).Database.SqlQuery<UserNewAdEmail>(
                @"select
	                *
                from 
	                Users users
                    left join 
	                (select
		                n.UserId, n.AdId
	                from
		                (select
			                u.Id as UserId, 
			                a.Id as AdId
		                from
			                Users u
			                cross join Ads a
		                where
			                u.InstagramLogin is not null and a.IsActive = 1 and a.IsNew = 0
		                order by
			                u.Id, a.Id) n
		                left join SentEmails se on se.AdId = n.AdId and se.UserId = n.UserId
	                where
		                se.Id is null) t on users.Id = t.UserId
                where
	                t.UserId is not null"
            ).ToArray();

            return users;
        }
    }
}

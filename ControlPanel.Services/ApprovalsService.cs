﻿using System;
using System.Linq;
using ControlPanel.Common.Enums;
using Common.DAL.Contracts;
using ControlPanel.Common.Helpers;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;

namespace ControlPanel.Services
{
    public class ApprovalsService : IApprovalsService
    {
        private readonly IDataContext _dataContext;

        public ApprovalsService(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public Approval[] GetApprovals(Guid networkId)
        {
            var approvals = (from ad in _dataContext.Ads
                let network = ad.EntityNetworks.FirstOrDefault(network => network.NetworkId == networkId)
                where ad.AdType == AdType.Banner && network != null
                orderby network.AdStatus
                select new Approval
                {
                    Id = ad.Id,
                    Name = ad.Name,
                    AdType = ad.AdType,
                    Url = ad.Url,
                    AdStatus = network.AdStatus.ToString(),
                    FileId =  ad.Files.FirstOrDefault(file => ad.AdType != AdType.Banner || file.FileAdType == FileAdType.Large).Id,
                    IsActive = network.IsActive,
                    IsStopped = network.IsStopped,
                    StartDate = network.StartDate,
                    EndDate = network.EndDate
                }).ToArray();
            var now = DateTime.Now;
            foreach (var approval in approvals)
            {
                approval.ShowActive = (approval.StartDate.HasValue && approval.EndDate.HasValue && Utilites.IsCampaignActive(approval.StartDate, approval.EndDate, now) || !approval.StartDate.HasValue || !approval.EndDate.HasValue)
                    && !approval.IsStopped;
            }
            return approvals;
        }

        public void UpdateAdStatus(Guid adId, Guid networkId, AdStatus status)
        {
            var adNetwork = _dataContext.AdNetworks.FirstOrDefault(item => item.AdId == adId && item.NetworkId == networkId);
            if (adNetwork != null)
            {
                adNetwork.AdStatus = status;
                _dataContext.SaveChanges();
            }
        }

        public void ChangeAdActive(Guid adId, Guid networkId)
        {
            var adNetwork = _dataContext.AdNetworks.FirstOrDefault(item => item.AdId == adId && item.NetworkId == networkId);
            if (adNetwork != null)
            {
                adNetwork.IsActive = !adNetwork.IsActive;
                _dataContext.SaveChanges();
            }
        }
    }
}

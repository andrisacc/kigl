﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;
using ControlPanel.Domain.Models.Reports;
using Microsoft.Reporting.WinForms;

namespace ControlPanel.Services
{
    public class ReportService : IReportService
    {
        public ReportData GenerateReport(string reportName, string format, Dictionary<String, DataTable> dataSources)
        {
            var reportStream = GetType().Assembly.GetManifestResourceStream(GetType(), $"Reports.{reportName}.rdlc");
            Warning[] warnings;

            var reportViewer = new ReportViewer
            {
                ProcessingMode = ProcessingMode.Local
            };

            reportViewer.LocalReport.LoadReportDefinition(reportStream);

            foreach (var pair in dataSources)
            {
                reportViewer.LocalReport.DataSources.Add(new ReportDataSource(pair.Key, pair.Value));
            }

            string mimeType = null;
            string fileExtension = null;

            var stream = new MemoryStream();

            CreateStreamCallback callback = (name, extension, encoding, type, seek) =>
            {
                mimeType = type;
                fileExtension = extension;

                return stream;
            };

            reportViewer.LocalReport.Render(format, null, callback, out warnings);

            stream.Seek(0, SeekOrigin.Begin);

            if (!String.IsNullOrEmpty(fileExtension))
            {
                fileExtension = "." + fileExtension;
            }

            var result = new ReportData
            {
                Stream = stream,
                FileExtension = fileExtension,
                MimeType = mimeType,
                Warnings = warnings
                    .Select(item => item.Message)
                    .ToArray()
            };

            return result;

        }
    }
}

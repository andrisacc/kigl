﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using AutoMapper.QueryableExtensions;
using Common.DAL.Contracts;
using ControlPanel.Domain.Contracts;
using AutoMapper;
using ControlPanel.Domain.Models;
using ControlPanel.Domain.Models.Reports;
using Entities = Common.DAL.Entities;

namespace ControlPanel.Services
{
    public class AdvertisersService : NetworkService, IAdvertiserService
    {
        private readonly IDataContext _dataContext;
        private readonly IReportService _reportService;
        private readonly IAccountsService _accountsService;

        public AdvertisersService(IDataContext dataContext, IReportService reportService, IAccountsService accountsService)
        {
            _dataContext = dataContext;
            _reportService = reportService;
            _accountsService = accountsService;
        }

        public Advertiser GetAdvertiser(Guid id)
        {
            var advertiser = _dataContext.Advertisers
                .Include(item => item.EntityNetworks)
                .First(item => item.Id == id);
            return Mapper.Map<Advertiser>(advertiser);
        }

        public Advertiser GetAdvertiserByEmail(string email)
        {
            var advertiser = _dataContext.Advertisers
                .Include(item => item.EntityNetworks)
                .First(item => item.Email == email);
            return Mapper.Map<Advertiser>(advertiser);
        }

        public Advertiser[] GetAdvertisers(bool withoutAccounts)
        {
            var query = _dataContext.Advertisers.AsQueryable();
            if (withoutAccounts)
            {
                query = query.Where(item => !item.Accounts.Any());
            }
            return query.ProjectTo<Advertiser>().ToArray();
        }

        public Advertiser[] GetAdvertisersOrderedByTime(bool withoutAccounts)
        {
            var query = _dataContext
                .Users
                .Include(item => item.Advertiser)
                .OrderByDescending(item => item.Registred)
                .Where(item => item.Advertiser != null)
                .Select(item => item.Advertiser)
                .AsQueryable();

            if (withoutAccounts)
            {
                query = query.Where(item => !item.Accounts.Any());
            }

            return query.ProjectTo<Advertiser>().ToArray();
        }

        public bool ValidateAdvertiser(string name)
        {
            return !_dataContext.Advertisers.Any(item => item.Name == name);
        }

        public bool ValidateEditedAdvertiser(string name, string advertiserId)
        {
            Guid id;
            if (Guid.TryParse(advertiserId, out id))
            {
                return !_dataContext.Advertisers.Where(item=>item.Id != id).Any(item => item.Name == name);
            }
            else
            {
                return false;
            }
        }

        public void Remove(Guid advertiserId)
        {
            var adv = _dataContext.Advertisers.Where(item => item.Id == advertiserId).FirstOrDefault();
            if (adv != null)
            {
                _dataContext.Advertisers.Remove(adv);
                _dataContext.SaveChanges();
            }
        }

        public void UpdateApprovedStatus(Guid advertiserId, Boolean isApproved)
        {
            var adv = _dataContext.Advertisers.Where(item => item.Id == advertiserId).FirstOrDefault();
            if (adv != null)
            {
                adv.IsApproved = isApproved;
                _dataContext.SaveChanges();
            }
        }

        public void UpdateRejectedStatus(Guid advertiserId, Boolean isRejected)
        {
            var adv = _dataContext.Advertisers.Where(item => item.Id == advertiserId).FirstOrDefault();
            if (adv != null)
            {
                adv.IsRejected = isRejected;
                _dataContext.SaveChanges();
            }
        }

        public void AddOrUpdateAdvertiser(Advertiser model)
        {
            Entities.Advertiser advertiser;
            if (model.Id == Guid.Empty)
            {
                advertiser = Mapper.Map<Entities.Advertiser>(model);
                advertiser.Id = Guid.NewGuid();
                _dataContext.Advertisers.Add(advertiser);
            }
            else
            {
                advertiser = _dataContext.Advertisers.Include(item => item.EntityNetworks).FirstOrDefault(item => item.Id == model.Id);
                Mapper.Map(model, advertiser);
            }
            _dataContext.SaveChanges();
        }

        public decimal GetAdvertiserBalance(Guid id)
        {
            var advertiser = _dataContext.Advertisers
                .Include(item => item.Accounts)
                .FirstOrDefault(item => item.Id == id);
            var account = advertiser?.Accounts.FirstOrDefault();
            if (account != null)
                return account.Balance;
            return 0;
        }

        public ReportData GenerateInvoice(Guid advertiserId, string amount)
        {
            var advertiser = _dataContext.Advertisers.Include(item => item.Accounts).FirstOrDefault(item => item.Id == advertiserId);
            var from = "KIGL LIMITED";
            var companyNo = "Company number 10577040";
            var vat = "VAT 270618702";
            var ibanTitle = "IBAN:";
            var iban1 = "EUR GB02MIDL40051577788611";
            var iban2 = "USD GB49MIDL40051577788638";
            var iban3 = "GBP GB93HBUK40070782283654";

            var account = _accountsService.GetAdvertiserAccount(advertiser);
            var description = $"Payment for www.kigl.eu services.";
            var date = DateTime.Now.Date;
            var number = 1;
            var invoices = _dataContext.Invoices.Where(item => item.Date == date);
            if (invoices.Any())
            {
                number = invoices.Max(item => item.Number) + 1;
            }
            var invoiceNumber = $"{date:ddMMyyyy}/{number}";
            var invoice = new Entities.Invoice
            {
                Id = Guid.NewGuid(),
                AdvertiserId = advertiserId,
                Date = date,
                Number = number,
                Amount = Decimal.Parse(amount, CultureInfo.InvariantCulture)
            };
            _dataContext.Invoices.Add(invoice);
            _dataContext.SaveChanges();
            DataTable table = new DataTable();
            table.Columns.Add("From");

            table.Columns.Add("CompanyNo");
            table.Columns.Add("Vat");
            table.Columns.Add("IbanTitle");
            table.Columns.Add("Iban1");
            table.Columns.Add("Iban2");
            table.Columns.Add("Iban3");

            table.Columns.Add("Number");
            table.Columns.Add("Date");
            table.Columns.Add("Description");
            table.Columns.Add("Amount");
            table.Rows.Add(from, companyNo, vat, ibanTitle, iban1, iban2, iban3, invoiceNumber, date.ToString("dd/MM/yyyy"), description, amount);

            var dataSources = new Dictionary<string, DataTable> { { "Invoice", table } };
            return _reportService.GenerateReport("Invoice", "PDF", dataSources);
        }
    }
}

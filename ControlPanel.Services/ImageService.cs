﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using ControlPanel.Domain.Contracts;

namespace ControlPanel.Services
{
    public class ImageService : IImageService
    {
        public void ResizeImage(string sourcePath, string destPath, Size size)
        {
            var src = Image.FromFile(sourcePath);
            var ratio = Math.Min((double)size.Width / src.Width, (double)size.Height / src.Height);

            int destWidth = (int)(src.Width * ratio);
            int destHeight = (int)(src.Height * ratio);

            var dest = new Bitmap(destWidth, destHeight, PixelFormat.Format24bppRgb);

            dest.SetResolution(src.HorizontalResolution,
                                             src.VerticalResolution);

            var grPhoto = Graphics.FromImage(dest);

            grPhoto.DrawImage(src,
               new Rectangle(0, 0, destWidth, destHeight),
               new Rectangle(0, 0, src.Width, src.Height),
               GraphicsUnit.Pixel);

            grPhoto.Dispose();

            dest.Save(destPath, src.RawFormat);

            src.Dispose();
        }

        public void GenerateThumb(string sourcePath, string destPath)
        {
            using (var stream = new FileStream(destPath, FileMode.Create))
            {
                var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
                ffMpeg.GetVideoThumbnail(sourcePath, stream, 5);
            }
        }
    }
}

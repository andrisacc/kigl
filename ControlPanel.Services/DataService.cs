﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using AutoMapper;
using System.Linq;
using AutoMapper.QueryableExtensions;
using ControlPanel.Common.Enums;
using Common.DAL.Contracts;
using Common.DAL.Entities;
using ControlPanel.Domain.Contracts;
using Ad = ControlPanel.Domain.Models.Ad;
using PortalImage = ControlPanel.Domain.Models.PortalImage;

namespace ControlPanel.Services
{
    public class DataService : IDataService
    {
        private readonly IDataContext _dataContext;
        private const string AdShowIntervalKey = "AdShowInterval";

        public DataService(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public int GetTimeToShow(string userId, Guid networkId, out bool showTime)
        {
            var userNetwork =
                _dataContext.UserNetworks.FirstOrDefault(item => item.UserId == userId && item.NetworkId == networkId);
            var hasChanges = false;
            if (userNetwork == null)
            {
                userNetwork = new UserNetwork
                {
                    UserId = userId,
                    NetworkId = networkId,
                    ShowTime = DateTime.UtcNow
                };
                _dataContext.UserNetworks.Add(userNetwork);
                hasChanges = true;
            }
            var time = (int)(userNetwork.ShowTime - DateTime.UtcNow).TotalMilliseconds;
            showTime = time <= 0;
            if (showTime)
            {
                var interval = int.Parse(ConfigurationManager.AppSettings[AdShowIntervalKey]);
                time = (int)TimeSpan.FromMinutes(interval).TotalMilliseconds;
                userNetwork.ShowTime = DateTime.UtcNow.AddMilliseconds(time);
                hasChanges = true;
            }
            if (hasChanges)
                _dataContext.SaveChanges();

            return time;
        }

        public Ad GetVideoForNetwork(Guid networkId)
        {
            return GetAd(networkId, AdType.Video).FirstOrDefault();
        }

        public Ad[] GetBanners(Guid networkId, int count)
        {
            return GetAd(networkId, AdType.Banner, count);
        }

        private Ad[] GetAd(Guid networkId, AdType adType, int count = 1)
        {
            var network = _dataContext.Networks.First(item => item.Id == networkId);
            if (network.ShowAd)
            {
                var lastViewedIndex = adType == AdType.Banner ? network.LastViewedBannerIndex : network.LastViewedVideoIndex;
                var adNetworks = _dataContext.AdNetworks.Include(item => item.Ad.Files)
                    .Include(item => item.Network.City.Country.Rates)
                    .Where(item => item.NetworkId == networkId && item.Ad.AdType == adType && item.AdStatus == AdStatus.Approved && item.IsActive && 
                    (!item.Budget.HasValue || item.Budget - 
                            item.NumberOfClicks * item.Network.City.Country.Rates.CostPerClick - 
                            (item.NumberOfImpressions + 1 )* item.Network.City.Country.Rates.CostPerImpression > 0))
                    .OrderBy(item => item.Index).ToArray();
                if (adNetworks.Any())
                {
                    //var maxNetworkIndex = adNetworks.Max(item => item.Index);
                    //var lastIndex = lastViewedIndex + count;
                    List<AdNetwork> bannerNetworks;
                    //if (lastViewedIndex != maxNetworkIndex && lastIndex > maxNetworkIndex)
                    //{
                    //    bannerNetworks = adNetworks.Where(item => item.Index > lastViewedIndex).ToList();
                    //    bannerNetworks.AddRange(adNetworks.Take(count - bannerNetworks.Count));
                    //}
                    //else
                    //{
                    //    if (lastViewedIndex == maxNetworkIndex) lastViewedIndex = 0;
                    //    bannerNetworks = adNetworks.Skip(lastViewedIndex).Take(count).ToList();
                    //}
                    IEnumerable<AdNetwork> bannerNetworksQuery = adNetworks.AsQueryable();
                    if (adNetworks.Any(item => item.Index > lastViewedIndex))
                        bannerNetworksQuery = adNetworks.Where(item => item.Index > lastViewedIndex);
                    bannerNetworks = bannerNetworksQuery.Take(count).ToList();
                    foreach (var bannerNetwork in bannerNetworks)
                    {
                        if (bannerNetwork.Budget.HasValue)
                        {
                            var costPerImpression = bannerNetwork.Network.City.Country.Rates.CostPerImpression;
                            bannerNetwork.Budget = bannerNetwork.Budget.Value - costPerImpression;
                        }
                        bannerNetwork.NumberOfImpressions++;
                    }
                    var index = bannerNetworks.Last().Index;
                    if (adType == AdType.Banner)
                        network.LastViewedBannerIndex = index;
                    else
                        network.LastViewedVideoIndex = index;
                    _dataContext.SaveChanges();
                    return bannerNetworks.Select(item => Mapper.Map<Ad>(item.Ad)).ToArray();
                }
            }
            return new Ad[0];
        }

        public void IncrementNumberOfClicks(Guid networkId, Guid adId)
        {
            var adNetwork =
                _dataContext.AdNetworks
                .Include(item => item.Network.City.Country.Rates)
                .FirstOrDefault(item => item.NetworkId == networkId && item.AdId == adId);
            if (adNetwork != null)
            {
                if (adNetwork.Budget.HasValue)
                {
                    var costPerClick = adNetwork.Network.City.Country.Rates.CostPerClick;
                    adNetwork.Budget = adNetwork.Budget.Value - costPerClick;
                }
                adNetwork.NumberOfClicks++;
                _dataContext.SaveChanges();
            }
        }

        public PortalImage[] GetPortalImages(Guid networkId)
        {
            var network = _dataContext.Networks.Include(item => item.PortalImages)
                .First(item => item.Id == networkId);
            var images = network.PortalImages.AsQueryable();
            if (!network.ShowHeaderImage)
                images = images.Where(item => item.PortalImageType != PortalImageType.Header);
            if (!network.ShowFooterImage)
                images = images.Where(item => item.PortalImageType != PortalImageType.Footer);
            return images.ProjectTo<PortalImage>().ToArray();
        }
    }
}

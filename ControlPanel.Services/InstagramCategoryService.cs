﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.DAL.Contracts;

namespace ControlPanel.Services
{
    public class InstagramCategoryService
    {
        protected void ApplyCategories<T>(IInstagramCategoriesEntity<T> entity, Guid[] categoryIds) where T : IEntityInstagramCategory, new()
        {
            if (entity.InstagramCategories == null) entity.InstagramCategories = new List<T>();
            if (categoryIds == null) categoryIds = new Guid[0];

            var itemsToRemove = entity.InstagramCategories.Where(cateogry => !categoryIds.Contains(cateogry.InstagramCategoryId)).ToList();
            itemsToRemove.ForEach(item => entity.InstagramCategories.Remove(item));

            var existingCategoryIds = entity.InstagramCategories.Select(item => item.InstagramCategoryId);
            foreach (var categoryId in categoryIds.Where(item => !existingCategoryIds.Contains(item)))
            {
                entity.InstagramCategories.Add(new T
                {
                    InstagramCategoryId = categoryId
                });
            }
        }
    }
}

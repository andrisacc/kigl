﻿using System;
using System.Globalization;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Common.DAL.Contracts;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;
using Kigl.Common.Enums;
using Entities = Common.DAL.Entities;

namespace ControlPanel.Services
{
    public class SettingsService : ISettingsService
    {
        private readonly IDataContext _dataContext;

        public SettingsService(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public Country[] GetCountries()
        {
            return _dataContext.Countries.ProjectTo<Country>().ToArray();
        }

        public Country AddCountry(string countryName)
        {
            if (!string.IsNullOrWhiteSpace(countryName))
            {
                var country = new Entities.Country
                {
                    Id = Guid.NewGuid(),
                    Name = countryName,
                    Rates = new Entities.Rates
                    {
                        Id = Guid.NewGuid()
                    }
                };

                _dataContext.Countries.Add(country);
                _dataContext.SaveChanges();
                return Mapper.Map<Country>(country);
            }
            return null;
        }

        public City[] GetCities(Guid[] countryIds = null)
        {
            var query = _dataContext.Cities.Query();
            if (countryIds != null)
            {
                query = query.Where(item => countryIds.Contains(item.CountryId));
            }
            return query.ProjectTo<City>().ToArray();
        }

        public void AddCity(City model)
        {
            var city = Mapper.Map<Entities.City>(model);
            city.Id = Guid.NewGuid();
            _dataContext.Cities.Add(city);
            _dataContext.SaveChanges();
        }

        public Currency[] GetCurrencies()
        {
            return _dataContext.Currencies.ProjectTo<Currency>().ToArray();
        }

        public Currency AddCurrency(string currencyName)
        {
            if (!string.IsNullOrWhiteSpace(currencyName))
            {
                var currency = new Entities.Currency
                {
                    Id = Guid.NewGuid(),
                    Name = currencyName
                };

                _dataContext.Currencies.Add(currency);
                _dataContext.SaveChanges();

                return Mapper.Map<Currency>(currency);
            }
            return null;
        }

        public Network[] GetNetworks(Guid[] cityIds = null)
        {
            var query = _dataContext.Networks.Query();
            if (cityIds != null)
            {
                query = query.Where(item => cityIds.Contains(item.CityId));
            }
            return query.ProjectTo<Network>().ToArray();
        }

        public void AddNetwork(Network model)
        {
            var network = Mapper.Map<Entities.Network>(model);
            network.Id = Guid.NewGuid();
            _dataContext.Networks.Add(network);
            _dataContext.SaveChanges();
        }

        public InstagramCategory[] GetInstagramCategories()
        {
            return _dataContext.InstagramCategories.ProjectTo<InstagramCategory>().ToArray();
        }

        public decimal GetUserFollowersBalance(int followersCount)
        {
            var settings = _dataContext.Settings.Where(item => new[] {Settings.AdPrice, Settings.FollowersLimit, Settings.InstagramerPercent}.Contains(item.Name))
                .ToDictionary(item => item.Name, item => item.Value);
            var instagramerPercent = int.Parse(settings[Settings.InstagramerPercent]);
            var fullBalance = GetAdvertiserFollowersBalance(followersCount);
            return instagramerPercent * fullBalance / 100;
        }

        public decimal GetAdvertiserFollowersBalance(int followersCount)
        {
            var settings = _dataContext.Settings.Where(item => new[] { Settings.AdPrice, Settings.FollowersLimit, Settings.InstagramerPercent }.Contains(item.Name))
                .ToDictionary(item => item.Name, item => item.Value);
            var adPrice = decimal.Parse(settings[Settings.AdPrice], CultureInfo.InvariantCulture);
            var followersLimit = int.Parse(settings[Settings.FollowersLimit]);
            var instagramerPercent = int.Parse(settings[Settings.InstagramerPercent]);
            var fullBalance = followersCount / followersLimit * adPrice;
            return fullBalance;
        }
    }
}
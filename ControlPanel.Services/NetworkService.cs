﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.DAL.Contracts;

namespace ControlPanel.Services
{
    public class NetworkService : InstagramCategoryService
    {
        protected void ApplyNetworks<T>(INetworksEntity<T> entity, Guid[] networkIds) where T : IEntityNetwork, new()
        {
            if (entity.EntityNetworks == null) entity.EntityNetworks = new List<T>();
            if(networkIds == null) networkIds = new Guid[0];

            var itemsToRemove = entity.EntityNetworks.Where(advertiserNetwork => !networkIds.Contains(advertiserNetwork.NetworkId)).ToList();
            itemsToRemove.ForEach(item => entity.EntityNetworks.Remove(item));

            var existingNetworkIds = entity.EntityNetworks.Select(item => item.NetworkId);
            foreach (var networkId in networkIds.Where(item => !existingNetworkIds.Contains(item)))
            {
                entity.EntityNetworks.Add(new T
                {
                    NetworkId = networkId
                });
            }
        }
    }
}

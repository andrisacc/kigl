﻿using System;
using System.Linq;
using AutoMapper;
using Common.DAL.Contracts;
using Common.DAL.Entities;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;
using Kigl.Common.Enums;

namespace ControlPanel.Services
{
    public class PaymentService : IPaymentService
    {
        private readonly IDataContext _dataContext;

        public PaymentService(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public Payment GetById(Guid id)
        {
            return _dataContext.Payments.Where(p => p.Id == id).FirstOrDefault();
        }

        public Guid AddPayment(Guid paymentId, Guid advertiserId, Decimal amount, Guid currencyId, String tranId, String syncResult, Kigl.Common.Enums.PaymentState state)
        {
            var payment = new Payment()
            {
                AdvertiserId = advertiserId,
                Amount = amount,
                AsyncResult = null,
                CurrencyId = currencyId,
                Id = paymentId,
                ProviderId = tranId,
                State = Kigl.Common.Enums.PaymentState.InProgress,
                Timestamp = DateTime.Now,
                SyncResult = syncResult
            };

            _dataContext.Payments.Add(payment);
            _dataContext.SaveChanges();

            return paymentId;
        }

        public Boolean UpdatePayment(String providerId, PaymentState state, String asyncResult, Guid paymentId)
        {
            var payment = _dataContext.Payments.Where(p => p.Id == paymentId).FirstOrDefault();
            if(payment != null)
            {
                payment.State = state;
                payment.AsyncResult = asyncResult;
                _dataContext.SaveChanges();

                return true;
            }
            else
            {
                return false;
            }
        }

        public Int32 CheckPayment(String paymentId)
        {
            Guid pId;
            if(Guid.TryParse(paymentId, out pId))
            {
                var res = _dataContext.Payments.Where(p => p.Id == pId).FirstOrDefault();
                if (res != null)
                    return (Int32)res.State;
                else
                    return (Int32)PaymentState.Failed;
            }

            return (Int32)PaymentState.Failed;
        }
    }
}

﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Common.DAL.Contracts;
using ControlPanel.Common.Enums;
using ControlPanel.Common.Helpers;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;
using ControlPanel.Domain.Models.Statistic;
using Kigl.Common.Enums;
using Entities = Common.DAL.Entities;
using Ad = Common.DAL.Entities.Ad;

namespace ControlPanel.Services
{
    public class StatisticService : IStatisticService
    {
        private readonly IDataContext _dataContext;

        public StatisticService(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public AdvertiserStatistic GetStatistic(Guid advertiserId)
        {
            var ads = _dataContext.Ads.Include(item => item.EntityNetworks.Select(network => network.Network.City.Country.Rates))
                .Include(item => item.Files)
                .Include(item => item.UserAds)
                .Where(item => item.AdvertiserId == advertiserId).ToArray();
            return new AdvertiserStatistic
            {
                Statistics = GetHotspotStatistic(ads),
                InstagramStatistics = GetInstagramStatistic(ads)
            };
        }

        private CountryStatistic[] GetHotspotStatistic(Ad[] ads)
        {
            var countries = ads.SelectMany(item => item.EntityNetworks.Select(network => network.Network.City.Country)).Distinct().ToArray();
            return (from country in countries
                let countryStatisticItems = GetCountryStatisticItems(ads, country)
                select new CountryStatistic
                {
                    Country = Mapper.Map<Country>(country),
                    Statistics = countryStatisticItems,
                    Total = countryStatisticItems.Sum(item => item.Total)
                }).ToArray();
        }

        private bool IsFinished(Ad item)
        {
            bool isOutOfTime = item.EndDate.HasValue && item.EndDate.Value.AddDays(1) < DateTime.Now;
            bool isBudgetFinished = item.Budget.HasValue && item.Budget <= 0;
            return isOutOfTime || isBudgetFinished;
        }

        private InstagramStatistic[] GetInstagramStatistic(Ad[] ads)
        {
            return ads.Where(item => item.AdType == AdType.InstagramImage || item.AdType == AdType.InstagramVideo)
                .Select(item => new InstagramStatistic
                {
                    Id = item.Id,
                    Name = item.Name,
                    Status = item.Status,
                    ShowStatus = Utilites.IsCampaignActive(item.StartDate, item.EndDate, DateTime.Now),
                    IsActive = item.IsActive,
                    IsNew = item.IsNew,
                    Amount = item.Amount,
                    Budget = item.Budget,
                    StartDate = item.StartDate.ToFormattedDate(),
                    EndDate = item.EndDate.ToFormattedDate(),
                    AdType = item.AdType,
                    IsFinished = IsFinished(item)
                }).ToArray();
        }

        private NetworkStatistic[] GetCountryStatisticItems(Entities.Ad[] ads, Entities.Country country)
        {
            return (from network in country.Cities.SelectMany(item => item.Networks)
                let networkStatisticItems = GetNetworkStatisticItems(ads, country, network)
                select new NetworkStatistic
                {
                    Network = Mapper.Map<Network>(network),
                    Statistics = networkStatisticItems,
                    Total = networkStatisticItems.Sum(item => item.Total)
                }).ToArray();
        }

        private Statistic[] GetNetworkStatisticItems(Entities.Ad[] ads, Entities.Country country, Entities.Network network)
        {
            var networkAds =
                ads.Where(item => item.EntityNetworks.Select(entity => entity.NetworkId).Contains(network.Id));
            var statistics = (from networkAd in networkAds
                let adNetwork = networkAd.EntityNetworks.First(item => item.NetworkId == network.Id)
                select new Statistic
                {
                    Id = adNetwork.AdId,
                    NetworkId = adNetwork.NetworkId,
                    Name = networkAd.Name,
                    NumberOfClicks = adNetwork.NumberOfClicks,
                    NumberOfImpressions = adNetwork.NumberOfImpressions,
                    FileId = networkAd.Files.FirstOrDefault(item => item.FileAdType != FileAdType.Video).Id,
                    AdStatus = adNetwork.AdStatus,
                    IsStopped = adNetwork.IsStopped,
                    AdType = networkAd.AdType,
                    Budget = adNetwork.Budget,
                    StartDate = adNetwork.StartDate.ToFormattedDate(),
                    EndDate = adNetwork.EndDate.ToFormattedDate()
                }).ToList();
            foreach (var statistic in statistics)
            {
                statistic.ChargeOfClicks = statistic.NumberOfClicks * country.Rates.CostPerClick;
                statistic.ChargeOfImpressions = statistic.NumberOfImpressions * country.Rates.CostPerImpression;
                statistic.Total = statistic.ChargeOfClicks + statistic.ChargeOfImpressions;
            }
            return statistics.ToArray();
        }

        
    }
}

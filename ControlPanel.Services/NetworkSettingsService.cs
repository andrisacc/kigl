﻿using System;
using System.Linq;
using ControlPanel.Common.Enums;
using Common.DAL.Contracts;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;

namespace ControlPanel.Services
{
    public class NetworkSettingsService : INetworkSettingsService
    {
        private readonly IDataContext _dataContext;

        public NetworkSettingsService(IDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public NetworkSettings GetNetworkSettings(Guid networkId)
        {
            var network = _dataContext.Networks.FirstOrDefault(item => item.Id == networkId);
            return AutoMapper.Mapper.Map<NetworkSettings>(network);
        }

        public void UpdateNetworkSetting(Guid networkId, NetworkSetting setting, bool value)
        {
            var network = _dataContext.Networks.FirstOrDefault(item => item.Id == networkId);
            if (network != null)
            {
                switch (setting)
                {
                    case NetworkSetting.ShowHeaderImage:
                        network.ShowHeaderImage = value;
                        break;
                    case NetworkSetting.ShowFooterImage:
                        network.ShowFooterImage = value;
                        break;
                    case NetworkSetting.ShowAd:
                        network.ShowAd = value;
                        break;
                    default:
                        throw new ArgumentException("Unknown network setting", nameof(setting));
                }
                _dataContext.SaveChanges();
            }
        }
    }
}

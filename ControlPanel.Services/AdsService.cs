﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using ControlPanel.Common.Enums;
using Common.DAL.Contracts;
using ControlPanel.Common.Helpers;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;
using Kigl.Common.Enums;
using Entities = Common.DAL.Entities;

namespace ControlPanel.Services
{
    public class AdsService : NetworkService, IAdsService
    {
        private readonly IDataContext _dataContext;

        private readonly IAccountsService _accountsService;

        public AdsService(IDataContext dataContext, IAccountsService accountsService)
        {
            _dataContext = dataContext;
            _accountsService = accountsService;
        }

        public Ad GetAd(Guid id)
        {
            _dataContext.Networks.ToArray();
            var ad = _dataContext.Ads
                .Include(item => item.Files)
                .Include(item => item.InstagramCategories)
                .Include(item => item.InstagramCategories.Select(category => category.InstagramCategory))
                .Include(item => item.EntityNetworks.Select(entityNetwork => entityNetwork.Network))
                .First(item => item.Id == id);
            var model = Mapper.Map<Ad>(ad);
            model.NetworkIds = ad.EntityNetworks.Select(item => item.NetworkId).ToArray();
            model.CategoryIds = ad.InstagramCategories.Select(item => item.InstagramCategoryId).ToArray();
            model.CategoriesNames = ad.InstagramCategories.Select(item => item.InstagramCategory.Name).ToArray();
            model.NetworkStatuses = ad.EntityNetworks.Select(entityNetwork => new NetworkStatus
            {
                Id = entityNetwork.NetworkId,
                Name = entityNetwork.Network.Name,
                AdStatus = entityNetwork.AdStatus
            }).ToArray();
            return model;
        }

        public Ad[] GetAds(Guid advertiserId, params AdType[] types)
        {
            return _dataContext.Ads
                .Include(item => item.Files)
                .Where(item => item.AdvertiserId == advertiserId && types.Contains(item.AdType))
                .ProjectTo<Ad>().ToArray();
        }

        public void AddOrUpdateAd(Ad model, Guid? networkId)
        {
            Entities.Ad ad;
            if (model.Id == Guid.Empty)
            {
                ad = Mapper.Map<Entities.Ad>(model);
                ad.Id = Guid.NewGuid();
                ad.IsActive = false;
                
                foreach (var file in ad.Files)
                {
                    if (!string.IsNullOrWhiteSpace(file.FileName))
                    {
                        file.Id = Guid.NewGuid();
                        file.AdId = ad.Id;
                        _dataContext.Files.Add(file);
                    }
                }
                
                ad.IsNew = true;
                _dataContext.Ads.Add(ad);
            }
            else
            {
                ad = _dataContext.Ads
                    .Include(item => item.Files)
                    .Include(item => item.EntityNetworks)
                    .Include(item => item.InstagramCategories)
                    .Include(item => item.EntityNetworks)
                    .Include(item => item.Advertiser.Accounts)
                    .FirstOrDefault(item => item.Id == model.Id);
                if (ad != null)
                {
                    if ((ad.AdType == AdType.Banner || ad.AdType == AdType.Video) && networkId.HasValue)
                    {
                        var adNetwork = ad.EntityNetworks.FirstOrDefault(item => item.NetworkId == networkId.Value);
                        if (adNetwork != null)
                        {
                            adNetwork.Budget = model.Budget;
                            adNetwork.StartDate = model.StartDateValue;
                            adNetwork.EndDate = model.EndDateValue;
                            adNetwork.IsActive = Utilites.IsCampaignActive(model.StartDateValue, model.EndDateValue, DateTime.Now);
                            adNetwork.IsStopped = false;
                            if (adNetwork.Budget.HasValue)
                            {
                                var account = _accountsService.GetAdvertiserAccount(ad.Advertiser);
                                account.Balance -= adNetwork.Budget.Value;
                            }
                        }
                    }
                    else
                    {
                        ad.Name = model.Name;
                        ad.Caption = model.Caption;
                        ad.Url = model.Url;
                        ad.Budget = model.Budget;
                        ad.StartDate = model.StartDateValue;
                        ad.EndDate = model.EndDateValue;
                        ad.IsActive = Utilites.IsCampaignActive(model.StartDateValue, model.EndDateValue, DateTime.Now);
                        ad.State = AdState.Activated;

                        foreach (var file in ad.Files)
                        {
                            var fileModel = model.Files.FirstOrDefault(item => item.FileAdType == file.FileAdType);
                            if (fileModel != null)
                            {
                                file.FileName = fileModel.FileName;
                                file.UploadedFileName = fileModel.UploadedFileName;
                            }
                        }
                    }
                }
            }
            if (!networkId.HasValue)
            {
                if (model.AdType != AdType.InstagramImage
                    && model.AdType != AdType.InstagramVideo
                    && (model.NetworkIds == null || model.NetworkIds.Length == 0))
                {
                    model.NetworkIds = _dataContext.Networks.Select(item => item.Id).ToArray();
                }
                ApplyNetworks(ad, model.NetworkIds);
                foreach (var adNetwork in ad.EntityNetworks)
                {
                    adNetwork.Budget = model.Budget;
                    adNetwork.StartDate = model.StartDateValue;
                    adNetwork.EndDate = model.EndDateValue;
                }
                var budgetAdNetworks = ad.EntityNetworks.Where(item => item.Budget.HasValue);
                if (budgetAdNetworks.Any())
                {
                    var total = budgetAdNetworks.Sum(item => item.Budget.Value);
                    var advertiser = ad.Advertiser;
                    if(ad.Advertiser == null) advertiser = _dataContext.Advertisers.Include(item => item.Accounts).FirstOrDefault(item => item.Id == model.AdvertiserId);
                    var account = _accountsService.GetAdvertiserAccount(advertiser);
                    account.Balance -= total;
                }
                ApplyCategories(ad, model.CategoryIds);
                ApplyIndexes(ad);
            }
            _dataContext.SaveChanges();
        }

        public Boolean IsChangeAdStatusPossible(Guid adId)
        {
            var ad = _dataContext.Ads.FirstOrDefault(item => item.Id == adId);

            if (ad.IsNew)
            {
                var advertiser = _dataContext.Advertisers.Include(item => item.Accounts).FirstOrDefault(item => item.Id == ad.AdvertiserId);
                var account = _accountsService.GetAdvertiserAccount(advertiser);

                if (ad.Budget.HasValue && account != null && account.Balance >= ad.Budget)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }
        }

        public string ChangeAdStatus(Guid adId)
        {
            var ad = _dataContext.Ads.FirstOrDefault(item => item.Id == adId);

            if(ad.IsNew)
            {
                var advertiser = _dataContext.Advertisers.Include(item => item.Accounts).FirstOrDefault(item => item.Id == ad.AdvertiserId);
                var account = _accountsService.GetAdvertiserAccount(advertiser);

                if (ad.Budget.HasValue && account != null && account.Balance >= ad.Budget)
                {
                    account.Balance -= ad.Budget.Value;
                }
                else
                {
                    return ad.Status;
                }
            }

            if (ad.IsActive)
                ad.State = AdState.Stopped;
            else
                ad.State = AdState.Activated;

            if (ad.IsNew)
                ad.IsActive = Utilites.IsCampaignActive(ad.StartDate, ad.EndDate, DateTime.Now);
            else
                ad.IsActive = !ad.IsActive;

            ad.IsNew = false;

            _dataContext.SaveChanges();
            return ad.Status;
        }

        public string StopCampaign(Guid adId, Guid networkId)
        {
            var adNetwork = _dataContext.AdNetworks
                .Include(item => item.Ad.Advertiser.Accounts)
                .FirstOrDefault(item => item.AdId == adId && item.NetworkId == networkId);
            adNetwork.IsActive = false;
            adNetwork.IsStopped = true;
            var account = _accountsService.GetAdvertiserAccount(adNetwork.Ad.Advertiser);
            if (adNetwork.Budget.HasValue)
            {
                account.Balance += adNetwork.Budget.Value;
                adNetwork.Budget = null;
            }
            _dataContext.SaveChanges();
            return account.Balance.ToString(CultureInfo.InvariantCulture);
        }

        public Domain.Models.Statistic.UserAd[] GetUserAds(Guid adId)
        {
            return _dataContext.UserAds
                .Include(item => item.Ad)
                .Include(item => item.User)
                .Where(item => item.VerifyStatus == VerifyStatus.Success && item.AdId == adId)
                .ProjectTo<Domain.Models.Statistic.UserAd>()
                .ToArray();
        }

        private void ApplyIndexes(Entities.Ad ad)
        {
            var networks = ad.EntityNetworks.Where(item => item.Index == 0).ToArray();
            var networkIds = networks.Select(item => item.NetworkId).ToArray();
            var indexes = _dataContext.AdNetworks.Where(item => networkIds.Contains(item.NetworkId) && item.Ad.AdType == ad.AdType)
                .GroupBy(item => item.NetworkId)
                .ToDictionary(group => group.Key, group => group.Max(network => network.Index));
            foreach (var entityNetwork in networks)
            {
                int index;
                if (!indexes.TryGetValue(entityNetwork.NetworkId, out index)) index = 0;
                entityNetwork.Index = index + 1;
            }
        }

        public File GetFile(Guid id)
        {
            var file = _dataContext.Files
                .Include(item => item.Ad)
                .First(item => item.Id == id);
            return Mapper.Map<File>(file);
        }

        public PortalImage[] GetPortalImages(Guid networkId)
        {
            return _dataContext.PortalImages
                .Where(item => item.NetworkId == networkId)
                .ProjectTo<PortalImage>().ToArray();
        }

        public PortalImage GetPortalImage(Guid id)
        {
            var portalImage = _dataContext.PortalImages
               .First(item => item.Id == id);
            var model = Mapper.Map<PortalImage>(portalImage);
            return model;
        }

        public void AddOrUpdatePortalImage(PortalImage portalImage)
        {
            var dbImage =
                _dataContext.PortalImages.FirstOrDefault(
                    item =>
                        item.NetworkId == portalImage.NetworkId && item.PortalImageType == portalImage.PortalImageType);
            if (dbImage == null)
            {
                dbImage = Mapper.Map<Entities.PortalImage>(portalImage);
                dbImage.Id = Guid.NewGuid();
                _dataContext.PortalImages.Add(dbImage);
            }
            else
            {
                portalImage.Id = dbImage.Id;
                Mapper.Map(portalImage, dbImage);
            }
            _dataContext.SaveChanges();
        }

        public Boolean RemoveAd(Guid adId)
        {
            var ad = _dataContext.Ads.Include(item=>item.Advertiser.Accounts).Where(a => a.Id == adId).FirstOrDefault();

            if(ad == null)
            {
                return false;
            }

            if((ad.StartDate.HasValue && ad.StartDate > DateTime.Now) || ad.IsNew)
            {
                var account = ad.Advertiser.Accounts.FirstOrDefault();
                if (ad.Budget.HasValue && !ad.IsNew)
                {
                    account.Balance += ad.Budget.Value;
                }

                _dataContext.Ads.Remove(ad);

                _dataContext.SaveChanges();
                return true;
            }
            
            return false;
        }

        public Boolean AddBudgetForAd(Guid adId, decimal amount)
        {
            var ad = _dataContext.Ads.Where(a => a.Id == adId).FirstOrDefault();
            var account = _dataContext.Accounts.Where(acc => acc.AdvertiserId == ad.AdvertiserId).FirstOrDefault();

            if (ad == null || account == null || (account.Balance - amount) < 0)
            {
                return false;
            }

            ad.Budget += amount;
            _dataContext.SaveChanges();

            if (ad.IsNew)
                return true;
            
            account.Balance -= amount;

            _dataContext.SaveChanges();

            return true;
        }
    }
}

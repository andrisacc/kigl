﻿namespace Kigl.Domain.Models.Instaram
{
    public class GetFollowersRequest
    {
        public string InstagramLogin { get; set; }
    }
}

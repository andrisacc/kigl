﻿namespace Kigl.Domain.Models.Instaram
{
    public class FollowersInfo
    {
        public int Follows { get; set; }
        public int FollowedBy { get; set; }
        public decimal Balance { get; set; }
        public bool IsLastFollowersRequestSuccess { get; set; }
    }
}

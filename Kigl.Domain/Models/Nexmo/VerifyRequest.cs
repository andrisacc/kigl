﻿namespace Kigl.Domain.Models.Nexmo
{
    public class VerifyRequest
    {
        public string RequestId { get; set; }
        public string Code { get; set; }
    }
}

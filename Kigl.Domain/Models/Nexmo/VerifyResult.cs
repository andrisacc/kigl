﻿namespace Kigl.Domain.Models.Nexmo
{
    public class VerifyResult
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}

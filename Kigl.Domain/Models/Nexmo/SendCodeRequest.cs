﻿namespace Kigl.Domain.Models.Nexmo
{
    public class SendCodeRequest
    {
        public string Number { get; set; }
    }
}

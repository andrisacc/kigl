﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kigl.Domain.Models.Stripe
{
    public class ChargeResult
    {
        public String ProviderId { get; set; }

        public String JsonResult { get; set; }

        public Boolean IsPaid { get; set; }

        public String Status { get; set; }

        public Int32 Amount { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kigl.Domain.Models.Stripe
{
    public class WebHookRequest
    {
        public Guid AdvertiserId { get; set; }

        public Guid PaymentId { get; set; }

        public String ProviderPaymentId { get; set; }

        public Boolean IsPaymentSuccesfull { get; set; }

        public Decimal Amount { get; set; }

        public String Status { get; set; }

        public String Request { get; set; }
    }
}

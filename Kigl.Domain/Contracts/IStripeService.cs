﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kigl.Domain.Models.Stripe;

namespace Kigl.Domain.Contracts
{
    public interface IStripeService
    {
        ChargeResult Charge(String token, Decimal amount, String email, Guid advertiserId, Guid paymentId);

        WebHookRequest Validate(String json, String signature);
    }
}

﻿using Kigl.Domain.Models.Nexmo;

namespace Kigl.Domain.Contracts
{
    public interface INexmoService
    {
        string SendCode(string number);
        VerifyResult VerifyCode(string requestId, string code);
    }
}

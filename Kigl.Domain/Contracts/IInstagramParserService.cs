﻿using Kigl.Domain.Models.Instaram;

namespace Kigl.Domain.Contracts
{
    public interface IInstagramParserService
    {
        FollowersInfo GetFollowers(string userId);
        bool CheckText(string userId, string text, params string[] tags);
    }
}

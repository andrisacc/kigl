﻿namespace Kigl.Domain.Contracts
{
    public interface IDbService
    {
        void SaveChanges();
    }
}

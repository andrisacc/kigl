﻿using System;
using Common.DAL.Entities;
using Kigl.Domain.Models;
using Kigl.Domain.Models.Instaram;

namespace Kigl.Domain.Contracts
{
    public interface IUserService : IDbService
    {
        ControlPanel.Domain.Models.User[] GetUsers(Guid? advertiserId, bool isInstagramAdmin);
        ControlPanel.Domain.Models.User[] GetInstagrammers(Guid? advertiserId, bool isInstagramAdmin);
        User GetById(string id);
        User GetByFacebookId(string facebookId);
        User Create(string facebookId, string name);
        User Create(string username, string name, string password, Guid? advertiserId, Guid? networkId, string rolename);
        User Create(string username, string name, string email, string password, Guid? advertiserId, Guid? networkId, string roleName, string lang);
        void Update(Guid id, string instagramLogin, string email, string phone, bool terms, String lang);
        void Update(Guid id, string instagramLogin, string email, string phone, bool terms, String lang, String referralUserCode);
        void Update(Guid id, Guid[] instagramCategories);
        void Delete(Guid id);
        Decimal GetBalance(Guid userId);
        User Check(string username, string password);
        string[] GetRoles(Guid userId);
        Guid[] GetUserInstagramCategories(Guid userId);
        bool ValidateUserName(string name);
        String GetUserLastWithdrawAccount(Guid userId);
        FollowersInfo GetFollowers(Guid userId);
        FollowersInfo GetFollowers(String instagramLogin);
        bool ValidateAdvertiserEmail(string email);
        string GetPasswordRecoveryCode(string email);
        string GetPasswordRecoveryLang(string email);
        User CheckRecoveryCode(string code);
        string UpdatePassword(Guid userId, string password);
        void UpdatePic(Guid id, string picUrl);
    }
}

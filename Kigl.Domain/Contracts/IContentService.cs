﻿using System;
using Common.DAL.Entities;
using ControlPanel.Common.Enums;
using Kigl.Common.Enums;
using Kigl.Common.Models;
using Kigl.Domain.Models;

namespace Kigl.Domain.Contracts
{
    public interface IContentService
    {
        Ad GetAd(Guid id);
        bool HasAdForShowing(User user);
        bool HasWithdraws(User user);
        Ad[] GetAdForShowing(User user);
        Ad[] GetUserAds(Guid userId);
        File GetFileByAdId(Guid id);
        void AddUserAd(Guid userId, Guid adId, int followersCount);
        VerifyStatus UpdateVerifyStatus(bool verifyResult, User user, Guid adId);
        void ResetVerifyStatus(Guid userId, Guid adId);
        void AddWithdrawRequest(string account, WithdrawType type, Guid userId);
        void UpdatePayPal(string account, Guid userId);
        void UpdatePhone(string phone, Guid userId);
        TransactionsPage GetPayments(Guid userId, int payment_counts, DateTime? startDate, DateTime? endDate, int? paymentType, int page, String lang);
    }
}

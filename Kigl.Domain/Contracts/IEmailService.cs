﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kigl.Domain.Contracts
{
    public interface IEmailService
    {
        void SendEmail(string to, string subject, string body, Stream attachment = null, string attachmentName = null);
    }
}

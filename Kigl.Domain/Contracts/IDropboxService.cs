﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kigl.Domain.Contracts
{
    public interface IDropboxService
    {
        Task<string> UploadAndCreateSharedLink(string path);
    }
}

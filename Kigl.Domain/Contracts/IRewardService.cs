﻿using System;
using Common.DAL.Entities;
using ControlPanel.Common.Enums;
using Kigl.Common.Enums;
using Kigl.Domain.Models;

namespace Kigl.Domain.Contracts
{
    public interface IRewardService
    {
        void PayReward(Guid userAdId, Decimal instagramerRewardPercent, Decimal advertiserRewardPercent);
    }
}

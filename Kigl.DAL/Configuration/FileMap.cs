﻿using System.Data.Entity.ModelConfiguration;
using Kigl.Domain.Models;

namespace Kigl.DAL.Configuration
{
    public class FileMap : EntityTypeConfiguration<File>
    {
        public FileMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.Ad)
                .WithMany(item => item.Files)
                .HasForeignKey(item => item.AdId);
        }
    }
}

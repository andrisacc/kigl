﻿using System.Data.Entity.ModelConfiguration;
using Kigl.Domain.Models;

namespace Kigl.DAL.Configuration
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            HasKey(item => item.Id);
        }
    }
}

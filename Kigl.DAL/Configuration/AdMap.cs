﻿using System.Data.Entity.ModelConfiguration;
using Kigl.Domain.Models;

namespace Kigl.DAL.Configuration
{
    public class AdMap : EntityTypeConfiguration<Ad>
    {
        public AdMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.User)
                .WithMany()
                .HasForeignKey(item => item.UserId);
        }
    }
}

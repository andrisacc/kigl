﻿using System.Data.Entity.ModelConfiguration;
using Kigl.Domain.Models;

namespace Kigl.DAL.Configuration
{
    public class SettingMap : EntityTypeConfiguration<Setting>
    {
        public SettingMap()
        {
            HasKey(item => item.Id);
        }
    }
}

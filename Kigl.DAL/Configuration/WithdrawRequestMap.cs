﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kigl.Domain.Models;

namespace Kigl.DAL.Configuration
{
    public class WithdrawRequestMap : EntityTypeConfiguration<WithdrawRequest>
    {
        public WithdrawRequestMap()
        {
            HasKey(item => item.Id);

            HasRequired(item => item.User)
                .WithMany()
                .HasForeignKey(item => item.UserId);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Kigl.DAL.Configuration;
using Kigl.DAL.Contracts;
using Kigl.Domain.Models;

namespace Kigl.DAL
{
    public class DataContext : DbContext, IDataContext
    {
        public DataContext() : base("DataContext")
        {            
        }

        public IRepository<User> Users => Repository<User>();
        public IRepository<Ad> Ads => Repository<Ad>();
        public IRepository<File> Files => Repository<File>();
        public IRepository<UserAd> UserAds => Repository<UserAd>();
        public IRepository<Setting> Settings => Repository<Setting>();
        public IRepository<WithdrawRequest> WithdrawRequests => Repository<WithdrawRequest>();

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new AdMap());
            modelBuilder.Configurations.Add(new FileMap());
            modelBuilder.Configurations.Add(new UserAdMap());
            modelBuilder.Configurations.Add(new SettingMap());
            modelBuilder.Configurations.Add(new WithdrawRequestMap());
        }

        private readonly Dictionary<Type, IQueryable> _repositories = new Dictionary<Type, IQueryable>();

        public IRepository<T> Repository<T>() where T : class
        {
            IQueryable repository;
            IRepository<T> result;
            if (_repositories.TryGetValue(typeof(T), out repository))
            {
                result = (IRepository<T>)repository;
            }
            else
            {
                _repositories.Add(typeof(T), result = new DbRepository<T>(Set<T>()));
            }
            return result;
        }        
    }
}

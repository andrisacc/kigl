﻿using Kigl.Domain.Models;

namespace Kigl.DAL.Contracts
{
    public interface IDataContext
    {
        IRepository<T> Repository<T>() where T : class;

        IRepository<User> Users { get; }
        IRepository<Ad> Ads { get; }
        IRepository<File> Files { get; }
        IRepository<UserAd> UserAds { get; }
        IRepository<Setting> Settings { get; }
        IRepository<WithdrawRequest> WithdrawRequests { get; }

        int SaveChanges();
    }
}

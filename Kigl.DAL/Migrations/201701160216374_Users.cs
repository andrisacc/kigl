using System.Data.Entity.Migrations;

namespace Kigl.DAL.Migrations
{
    public partial class Users : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        FacebookId = c.String(unicode: false),
                        InstagramLogin = c.String(unicode: false),
                        Name = c.String(unicode: false),
                        Phone = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Users");
        }
    }
}

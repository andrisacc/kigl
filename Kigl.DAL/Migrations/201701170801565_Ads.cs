using System.Data.Entity.Migrations;

namespace Kigl.DAL.Migrations
{
    public partial class Ads : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Ads",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        Name = c.String(unicode: false),
                        Caption = c.String(unicode: false),
                        AdType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "Files",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        AdId = c.Guid(nullable: false),
                        FileName = c.String(unicode: false),
                        UploadedFileName = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Ads", t => t.AdId, cascadeDelete: true)
                .Index(t => t.AdId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Files", "AdId", "Ads");
            DropForeignKey("Ads", "UserId", "Users");
            DropIndex("Files", new[] { "AdId" });
            DropIndex("Ads", new[] { "UserId" });
            DropTable("Files");
            DropTable("Ads");
        }
    }
}

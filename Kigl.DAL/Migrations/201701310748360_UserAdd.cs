namespace Kigl.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UserAdd : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "UserAds",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        AdId = c.Guid(nullable: false),
                        VerifyStatus = c.Int(nullable: false),
                        VerifyDate = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Ads", t => t.AdId, cascadeDelete: true)
                .ForeignKey("Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.AdId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("UserAds", "UserId", "Users");
            DropForeignKey("UserAds", "AdId", "Ads");
            DropIndex("UserAds", new[] { "UserId" });
            DropIndex("UserAds", new[] { "AdId" });
            DropTable("UserAds");
        }
    }
}

namespace Kigl.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Settings : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Settings",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(unicode: false),
                        Value = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            Sql("insert into Settings (Id, Name, Value) values (UUID(), 'CheckInterval', '24:00')");
            Sql("insert into Settings (Id, Name, Value) values (UUID(), 'AdPrice', '3')");
        }
        
        public override void Down()
        {
            DropTable("Settings");
        }
    }
}

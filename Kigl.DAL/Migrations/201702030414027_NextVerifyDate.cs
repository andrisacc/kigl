namespace Kigl.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NextVerifyDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.UserAds", "NextVerifyDate", c => c.DateTime(precision: 0));
            AlterColumn("dbo.UserAds", "VerifyDate", c => c.DateTime(precision: 0));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.UserAds", "VerifyDate", c => c.DateTime(nullable: false, precision: 0));
            DropColumn("dbo.UserAds", "NextVerifyDate");
        }
    }
}

using System.Data.Entity.Migrations;

namespace Kigl.DAL.Migrations
{
    public partial class UserInfo : DbMigration
    {
        public override void Up()
        {
            AddColumn("Users", "Email", c => c.String(unicode: false));
            AddColumn("Users", "PasswordHash", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("Users", "PasswordHash");
            DropColumn("Users", "Email");
        }
    }
}

namespace Kigl.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WithdrawRequests : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "WithdrawRequests",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        UserId = c.Guid(nullable: false),
                        WithdrawType = c.Int(nullable: false),
                        Account = c.String(unicode: false),
                        Timestamp = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("WithdrawRequests", "UserId", "Users");
            DropIndex("WithdrawRequests", new[] { "UserId" });
            DropTable("WithdrawRequests");
        }
    }
}

using System.Data.Entity.Migrations;

namespace Kigl.DAL.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }      
    }
}

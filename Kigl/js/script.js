$(document).ready(function(){
	$(document).ready(function() {
    $('select').material_select();
	});

	$('.head__bottom__content').slick({
		slide: '.head__bottom-slide',
		slidesToShow: 1,
		slidesToSlides: 1,
		dots: true,
		arrows: false,
		autoplay: true,
		speed: 500,
		autoplaySpeed: 5000,
		responsive: [
			{
				breakpoint: 767,
				settings: {
					adaptiveHeight: true,
				}
			}
		]
	});
});
﻿using System;
using System.Globalization;
using System.Threading;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Common.Logging;
using Kigl.Controllers;

namespace Kigl
{
    public class Global : HttpApplication
    {
        private static readonly ILog Log = LogManager.GetLogger<Global>();

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            MapConfig.Configure();
            UnityMvcConfig.RegisterComponents();
            UnityWebApiConfig.RegisterComponents();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        public void Application_OnBeginRequest(object sender, EventArgs e)
        {
            var culture = CultureInfo.InvariantCulture;
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;
        }

        public void Application_Error()
        {
            var exception = Server.GetLastError();
            if(exception != null) Log.Error(exception.Message, exception);
        }
    }
}
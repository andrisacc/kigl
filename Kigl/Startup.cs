﻿using Kigl;
using Microsoft.Owin;
using Owin;
using System.Configuration;
using Stripe;

[assembly: OwinStartup(typeof(Startup))]
namespace Kigl
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            StripeConfiguration.SetApiKey(ConfigurationManager.AppSettings["StripeSecretKey"]);

            Microsoft.ApplicationInsights.Extensibility.TelemetryConfiguration.Active.InstrumentationKey = System.Web.Configuration.WebConfigurationManager.AppSettings["iKey"];

            ConfigureAuth(app);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Filters;
using Kigl.Models;

namespace Kigl.Controllers
{
    [KiglAuthorize(RoleName.Advertiser)]
    public class AdvertiserController : BaseController
    {
        private readonly IAdvertiserService _advertiserService;
        private readonly IStripeService _stripeService;
        private readonly IAccountsService _accountService;
        private readonly IPaymentService _paymentService;


        [HttpGet]
        public ActionResult Topup()
        {
            return View();
        }

        public ActionResult Invoice(InvoiceModel model)
        {
            var advertiserId = CurrentUser.AdvertiserId.Value;
            var reportData = _advertiserService.GenerateInvoice(advertiserId, model.Amount);
            Response.AddHeader("Content-Disposition", "attachment; filename=Invoice" + reportData.FileExtension);
            return File(reportData.Stream, reportData.MimeType);
        }

        public ActionResult PaymentMethod(InvoiceModel model, string submit)
        {
            if (submit == Resources.Global.Admin_PayByCard)
            {
                
                Session["InvoiceModelSubmit"] = new InvoiceModelSubmit() { Amount = model.Amount };
                return RedirectToAction("CardTopup");
            }
            else
            {
                return RedirectToAction("Invoice", model);
            }
        }

        public AdvertiserController(IUserService userService, IAdvertiserService advertiserService, IStripeService stripeService, IAccountsService accountService, IPaymentService paymentService) : base(userService)
        {
            _advertiserService = advertiserService;
            _stripeService = stripeService;
            _accountService = accountService;
            _paymentService = paymentService;
        }

        [HttpGet]
        public ActionResult CardTopup()
        {
            var model = (InvoiceModelSubmit)Session["InvoiceModelSubmit"];
            return View(model);
        }

        [HttpPost]
        public ActionResult CardTopupSubmit(InvoiceModelSubmit model)
        {
            if(!String.IsNullOrEmpty(model.CardToken) && !String.IsNullOrEmpty(model.Amount))
            {
                var advertiser = _advertiserService.GetAdvertiser(CurrentUser.AdvertiserId.Value);

                Guid paymentId = Guid.NewGuid();

                var result = _stripeService.Charge(model.CardToken, Decimal.Parse(model.Amount), advertiser.Email, CurrentUser.AdvertiserId.Value, paymentId);

                _paymentService.AddPayment(paymentId, CurrentUser.AdvertiserId.Value, ((Decimal)result.Amount)/100, new Guid("4a215876-053d-41f3-b282-42b325ee5740"), result.ProviderId, result.JsonResult, Common.Enums.PaymentState.InProgress);

                Session["PaymentId"] = paymentId;
            }
            return RedirectToAction("Processing");
        }

        public ActionResult Processing()
        {
            ViewBag.PaymentId = Session["PaymentId"];
            return View();
        }
    }
}
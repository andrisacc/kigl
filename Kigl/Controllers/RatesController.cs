﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Filters;
using Kigl.Models;

namespace Kigl.Controllers
{
    [KiglAuthorize(RoleName.Admin)]
    public class RatesController : BaseController
    {
        private readonly IRatesService _ratesService;
        private readonly ISettingsService _settingsService;

        public RatesController(IUserService userService, IRatesService ratesService, ISettingsService settingsService) : base(userService)
        {
            _ratesService = ratesService;
            _settingsService = settingsService;
        }

        public ActionResult Index()
        {
            var model = new EditRatesModel
            {
                Countries = _settingsService.GetCountries()
            };
            return View(model);
        }

        public ActionResult GetRates(Guid countryId)
        {
            var rates = _ratesService.GetRates(countryId);
            var model = AutoMapper.Mapper.Map<EditRatesModel>(rates);
            return SuccessResult(model);
        }

        public ActionResult UpdateRates(EditRatesModel model)
        {
            _ratesService.UpdateRates(model);
            return SuccessResult();
        }
    }
}
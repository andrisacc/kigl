﻿using System;
using System.Linq;
using System.Net;
using System.IO;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;
using Kigl.Domain.Contracts;
using Kigl.Filters;
using Kigl.Models;
using Kigl.Stripe;
using System.Web;

namespace Kigl.Controllers
{
    public class RefAdvController : BaseController
    {
        public RefAdvController(IUserService userService) : base(userService)
        {
        }

        public ActionResult Index(String id)
        {
#if DEBUG
            Uri baseUri = new Uri("http://kigl.eu:8000");

#else
            Uri baseUri = new Uri("http://kigl.eu");
#endif
            var tlang = CurrentLanguage == Language.Russian ? "ru" : "en";

            if (!String.IsNullOrEmpty(id))
            {
                var referralCode = id;

                var c = new HttpCookie(Constants.Referral)
                {
                    Expires = DateTime.Now.AddDays(1),
                    Value = referralCode
                };
                HttpContext.Response.Cookies.Add(c);

                Uri ruri = new Uri(baseUri, tlang + "/Account/Advertisers?type=signup");

                return Redirect(ruri.ToString());
            }

            Uri uri = new Uri(baseUri, tlang + "/Home/Index");

            return Redirect(uri.ToString()); ;
        }
    }
}
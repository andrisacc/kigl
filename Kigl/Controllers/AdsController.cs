﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Common.Configs;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;
using Kigl.Domain.Contracts;
using Kigl.Helpers;
using Kigl.Models;
using File = ControlPanel.Domain.Models.File;
using AutoMapper;
using Kigl.Common.Helpers;
using Kigl.Filters;
using System.Text.RegularExpressions;


namespace Kigl.Controllers
{
    [KiglAuthorize(RoleName.Advertiser, RoleName.NetworkAdmin)]
    public class AdsController : BaseController
    {        
        private readonly IAdsService _adsService;
        private readonly IImageService _imageService;
        private readonly ISettingsService _settingsService;
        private readonly IDropboxService _dropboxService;

        public AdsController(IUserService userService, IAdsService adsService, IImageService imageService, ISettingsService settingsService, IDropboxService dropboxService) : base(userService)
        {
            _adsService = adsService;
            _imageService = imageService;
            _settingsService = settingsService;
            _dropboxService = dropboxService;
        }

        [HttpGet]
        public ActionResult Content()
        {
            var model = new ListAdModel
            {
                Banners = _adsService.GetAds(CurrentUser.AdvertiserId.Value, AdType.Banner, AdType.InstagramImage),
                Videos = _adsService.GetAds(CurrentUser.AdvertiserId.Value, AdType.Video, AdType.InstagramVideo)
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult StartCampaign()
        {
            return View();
        }

        [HttpGet]
        public ActionResult EditBanner(Guid? id, Guid? networkId = null)
        {
            var model = PrepareModel(id, networkId);
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteCampaign(Guid adId, Guid? networkId)
        {
            var res = _adsService.RemoveAd(adId);

            Log.Info($"Campaign {adId} has been deleted.");

            return SuccessCamelCaseResult(new
            {
                success = res
            });
        }

        [HttpPost]
        public ActionResult AddBudget(Guid adId, Guid? networkId, String Amount)
        {
            Boolean res = false;
            Decimal amount = 0;

            if (!Decimal.TryParse(Amount, out amount))
            {
                return SuccessCamelCaseResult(new
                {
                    success = res
                });
            }

            res = _adsService.AddBudgetForAd(adId, amount);

            Log.Info($"Add funds for campaign {adId} for {amount}, result {res}");

            return SuccessCamelCaseResult(new
            {
                success = res
            });
        }

        [HttpPost]
        public async Task<ActionResult> EditBanner(EditAdModel model)
        {
            await AddOrUpdateAd(model);
            
            Log.Info($"Campaign {model.Id} has been updated.");

            return RedirectToAction("AdvertiserIndex", "Home");
        }

        [HttpGet]
        public ActionResult EditVideo(Guid? id, Guid? networkId = null)
        {
            var model = PrepareModel(id, networkId, AdType.Video);
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EditVideo(EditAdModel model)
        {
            await AddOrUpdateAd(model);

            Log.Info($"Campaign {model.Id} has been updated.");

            return RedirectToAction("AdvertiserIndex", "Home");
        }

        [HttpPost]
        public ActionResult IsUpdateAdStatusPossible(Guid adId)
        {
            var status = _adsService.IsChangeAdStatusPossible(adId);

            return Json(status);
        }

        [HttpPost]
        public ActionResult UpdateAdStatus(Guid adId)
        {
            var status = _adsService.ChangeAdStatus(adId);

            Log.Info($"State of the campaign {adId} has been updated: {status}");

            return Json(status);
        }

        [HttpPost]
        public ActionResult StopCampaign(Guid adId, Guid networkId)
        {
            var balance = _adsService.StopCampaign(adId, networkId);

            Log.Info($"Campaign {adId} has been stoped");

            return SuccessCamelCaseResult(new 
            {
                success = true,
                balance
            });
        }

        [HttpPost]
        public ActionResult GetAdUsers(Guid adId)
        {
            var adUsers = _adsService.GetUserAds(adId);
            return SuccessCamelCaseResult(adUsers);
        }

        private AdForShowingModel PrepareAdForShowingModel(Ad ad)
        {
            var model = MapModel<AdForShowingModel>(ad);
            var adFile = ad.Files.First(item => item.FileAdType != FileAdType.Thumbnail);
            var thumbnailFile = ad.Files.FirstOrDefault(item => item.FileAdType == FileAdType.Thumbnail);
            model.DownloadUrl = Url.Action("DownloadFile", "Files", new { id = ad.Id });
            if (model.AdType == AdType.InstagramImage)
            {
                model.FileUrl = Url.Action("GetFileByName", "Files", new { fileName = adFile.FileName, adType = AdType.Banner });
            }
            if (model.AdType == AdType.InstagramVideo)
            {
                model.FileUrl = Url.Action("GetVideoFileByName", "Files", new { fileName = adFile.FileName });
            }
            if (thumbnailFile != null)
            {
                model.ThumbnailUrl = Url.Action("GetFileByName", "Files", new { fileName = thumbnailFile.FileName, adType = AdType.Banner });
            }

            model.CategoryNames = ad.CategoriesNames;
            model.FileName = adFile.FileName;

            return model;
        }

        private T MapModel<T>(Ad ad) where T : ControlPanel.Domain.Models.Ad, new()
        {
            return new T
            {
                Id = ad.Id,
                Name = ad.Name,
                Caption = ad.Caption,
                AdType = ad.AdType,
                Files = ad.Files.Select(item => new ControlPanel.Domain.Models.File
                {
                    Id = item.Id,
                    FileName = item.FileName,
                    UploadedFileName = item.UploadedFileName,
                    FileAdType = item.FileAdType
                }).ToArray()
            };
        }

        [HttpPost]
        public ActionResult GetAd(Guid adId)
        {
            var ad = _adsService.GetAd(adId);
            var model = PrepareAdForShowingModel(ad);
            return SuccessCamelCaseResult(model);
        }

        [HttpPost]
        public ActionResult GetCities(Guid[] countryIds)
        {
            var cities = _settingsService.GetCities(countryIds);
            return Json(cities);
        }

        [HttpPost]
        public ActionResult GetNetworks(Guid[] cityIds)
        {
            var networks = _settingsService.GetNetworks(cityIds);
            return Json(networks);
        }

        [HttpGet]
        public ActionResult PortalImages()
        {
            var portalImages = _adsService.GetPortalImages(CurrentUser.NetworkId.Value);
            return View(portalImages);
        }

        [HttpPost]
        public ActionResult EditPortalImage(string fileName, PortalImageType portalImageType, string url)
        {
            var portalImage = new PortalImage
            {
                FileName = fileName,
                PortalImageType = portalImageType,
                NetworkId = CurrentUser.NetworkId.Value,
                Url = url
            };
            _adsService.AddOrUpdatePortalImage(portalImage);

            return SuccessResult();
        }

        private EditAdModel PrepareModel(Guid? id, Guid? networkId = null, AdType defaultType = AdType.Banner)
        {
            EditAdModel model;
            if (id.HasValue)
            {
                var ad = _adsService.GetAd(id.Value);
                model = Mapper.Map<EditAdModel>(ad);
            }
            else
            {
                model = new EditAdModel
                {
                    Files = new File[0],
                    NetworkIds = new Guid[0],
                    NetworkStatuses = new NetworkStatus[0],
                    AdType = defaultType
                };
            }
            model.Countries = _settingsService.GetCountries();
            model.Categories = _settingsService.GetInstagramCategories();
            model.NetworkId = networkId;
            return model;
        }

        private async Task AddOrUpdateAd(EditAdModel model, AdType? adType = null)
        {
            model.AdvertiserId = CurrentUser.AdvertiserId.Value;
            model.AdType = adType ?? model.AdType;
            model.Caption = Regex.Replace(model.Caption, @"\s+", " "); //remove extra white spaces
            if (model.AdType == AdType.Banner)
            {
                GenerateFiles(model);
            }
            if (model.AdType == AdType.Video || model.AdType == AdType.InstagramVideo)
            {
                var limit = int.Parse(ConfigurationManager.AppSettings["FileLimit"]) * 1000000;
                foreach (var file in model.Files)
                {
                    file.FileAdType = FileAdType.Video;
                    if (model.AdType == AdType.InstagramVideo)
                    {
                        var path = Path.Combine(DirectoryHelper.VideosDirectory, file.FileName);
                        var destPath = path + Path.GetExtension(file.UploadedFileName);
                        System.IO.File.Copy(path, destPath);
                        if (new FileInfo(path).Length > limit)
                        {
                            file.SharedLink = await _dropboxService.UploadAndCreateSharedLink(destPath);
                        }
                        System.IO.File.Delete(destPath);
                    }
                }
                GenerateThumb(model);
            }
            _adsService.AddOrUpdateAd(model, model.NetworkId);
        }

        private void GenerateFiles(EditAdModel model)
        {            
            var fileArray = model.Files;
            var sourceFileName = model.Files.Last(item => !string.IsNullOrWhiteSpace(item.FileName)).FileName;
            if (model.Files.Length < 3)
            {
                Array.Resize(ref fileArray, 3);
                for (var fileAdType = FileAdType.Medium; fileAdType >= 0; fileAdType--)
                {
                    fileArray[2 - (int) fileAdType] = new File
                    {
                        FileAdType = fileAdType
                    };
                }
            }
            var sizes = ImageSizesSection.GetSection().ImageSizes;
            foreach (var file in fileArray.Where(item => string.IsNullOrWhiteSpace(item.FileName)))
            {
                file.FileName = Guid.NewGuid().ToString();
                _imageService.ResizeImage(Path.Combine(DirectoryHelper.BannersDirectory, sourceFileName),
                    Path.Combine(DirectoryHelper.BannersDirectory, file.FileName),
                    sizes[file.FileAdType]);
            }
            model.Files = fileArray;
        }

        private void GenerateThumb(EditAdModel model)
        {
            var videoFile = model.Files.FirstOrDefault(item => item.FileAdType == FileAdType.Video);
            if (videoFile != null)
            {
                var fileArray = model.Files;                
                Array.Resize(ref fileArray, 2);
                var thumbFileName = Guid.NewGuid().ToString();
                fileArray[1] = new File
                {
                    FileName = thumbFileName,
                    FileAdType = FileAdType.Thumbnail,
                };                
                _imageService.GenerateThumb(Path.Combine(DirectoryHelper.VideosDirectory, videoFile.FileName), Path.Combine(DirectoryHelper.BannersDirectory, thumbFileName));
                model.Files = fileArray;
            }
        }
    }
}
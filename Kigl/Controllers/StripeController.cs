﻿using System;
using System.Linq;
using System.Net;
using System.IO;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;
using Kigl.Domain.Contracts;
using Kigl.Filters;
using Kigl.Models;
using Kigl.Stripe;

namespace Kigl.Controllers
{
    public class StripeController : BaseController
    {
        private readonly IAdvertiserService _advertiserService;
        private readonly IStripeService _stripeService;
        private readonly IAccountsService _accountService;
        private readonly IPaymentService _paymentService;

        public StripeController(IUserService userService, IAccountsService accountService, IAdvertiserService advertiserService, IStripeService stripeService, IPaymentService paymentService) : base(userService)
        {
            _accountService = accountService;
            _advertiserService = advertiserService;
            _stripeService = stripeService;
            _paymentService = paymentService;
        }

        [HttpPost]
        public ActionResult WebHook()
        {
            var json = new StreamReader(Request.InputStream).ReadToEnd();
            var signature = Request.Headers["Stripe-Signature"];

            var res = _stripeService.Validate(json, signature);

            if (res != null)
            {
                var payment = _paymentService.GetById(res.PaymentId);

                if (payment.State == Common.Enums.PaymentState.InProgress)
                {
                    if (res.IsPaymentSuccesfull)
                    {
                        if (_paymentService.UpdatePayment(res.ProviderPaymentId, Common.Enums.PaymentState.Success, res.Request, res.PaymentId))
                        {
                            try
                            {

                                var acc = _accountService.GetAccountByAdvertiserId(res.AdvertiserId);

                                if (acc == null)
                                {
                                    Log.Info($"account for advertiser {res.AdvertiserId} is null");

                                    acc = new Account()
                                    {
                                        AdvertiserId = res.AdvertiserId,
                                        Balance = 0,
                                        CurrencyId = new System.Guid("4a215876-053d-41f3-b282-42b325ee5740"),
                                        Number = ""
                                    };
                                }

                                acc.Balance += res.Amount;
                                _accountService.AddOrUpdateAccount(acc);

                                Log.Info($"account for advertiser {res.AdvertiserId} has been incresed for {res.Amount}");
                            }
                            catch (Exception exc)
                            {
                                Log.Error($"Exception for advertiser {res.AdvertiserId}: {exc.ToString()}");
                            }
                        }
                        Log.Error($"Double stripe notification. Payment is already updated.");
                    }
                    else
                    {
                        _paymentService.UpdatePayment(res.ProviderPaymentId, Common.Enums.PaymentState.Failed, res.Request, res.PaymentId);
                    }
                }
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);   
        }

        [HttpPost]
        public ActionResult Check(string paymentId)
        {
            return SuccessResult(_paymentService.CheckPayment(paymentId)); 
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Filters;
using Kigl.Models;

namespace Kigl.Controllers
{
    [KiglAuthorize(RoleName.NetworkAdmin)]
    public class ApprovalsController : BaseController
    {
        private readonly IApprovalsService _approvalsService;

        public ApprovalsController(IUserService userService, IApprovalsService approvalsService) : base(userService)
        {
            _approvalsService = approvalsService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetApprovals()
        {
            var approvals = _approvalsService.GetApprovals(CurrentUser.NetworkId.Value);
            return SuccessCamelCaseResult(approvals);
        }

        public ActionResult UpdateAdStatus(Guid adId, AdStatus status)
        {
            _approvalsService.UpdateAdStatus(adId, CurrentUser.NetworkId.Value, status);
            return SuccessResult();
        }

        public ActionResult ChangeAdActive(Guid adId)
        {
            _approvalsService.ChangeAdActive(adId, CurrentUser.NetworkId.Value);
            return SuccessResult();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Filters;
using Kigl.Models;

namespace Kigl.Controllers
{
    [KiglAuthorize(RoleName.Admin)]
    public class SettingsController : BaseController
    {
        private readonly ISettingsService _settingsService;

        public SettingsController(IUserService userService, ISettingsService settingsService) : base(userService)
        {            
            _settingsService = settingsService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new SettingsModel
            {
                Countries = _settingsService.GetCountries(),
                Cities = _settingsService.GetCities(),
                Currencies = _settingsService.GetCurrencies(),
                Networks = _settingsService.GetNetworks()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult AddCountry(string countryName)
        {
            var country = _settingsService.AddCountry(countryName);
            return SuccessResult(country);
        }

        [HttpPost]
        public ActionResult AddCurrency(string currencyName)
        {
            var currency = _settingsService.AddCurrency(currencyName);
            return SuccessResult(currency);
        }

        [HttpGet]
        public ActionResult EditCity()
        {
            var model = new EditCityModel
            {
                Countries = _settingsService.GetCountries()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult EditCity(EditCityModel model)
        {
            _settingsService.AddCity(model);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditNetwork()
        {
            var model = new EditNetworkModel
            {
                Cities = _settingsService.GetCities()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult EditNetwork(EditNetworkModel model)
        {
            _settingsService.AddNetwork(model);
            return RedirectToAction("Index");
        }
    }
}
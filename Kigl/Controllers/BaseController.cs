﻿using System;
using System.Configuration;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Common.Logging;
using Common.DAL.Entities;
using ControlPanel.Common.Enums;
using Kigl.Common.Helpers;
using Kigl.Domain.Contracts;
using Kigl.Domain.Models;
using Kigl.Filters;
using Kigl.Helpers;
using Microsoft.Owin.Security;


namespace Kigl.Controllers
{
    [Localization]
    public class BaseController : Controller
    {
        protected static readonly ILog Log = LogManager.GetLogger<ContentController>();

        protected readonly IUserService _userService;

        protected IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        public BaseController(IUserService userService)
        {
            _userService = userService;
            ViewBag.FacebookAppId = ConfigurationManager.AppSettings["FacebookAppId"];
            
        }

        public decimal Balance => CurrentUser != null ? _userService.GetBalance(CurrentUser.Id) : 0;

        private User _currentUser;

        public User CurrentUser => _currentUser ?? (_currentUser = _userService.GetById(User.Identity.Name));

        private string[] _currentUserRoles;

        public string[] CurrentUserRoles
        {
            get
            {
                if (_currentUserRoles == null || _currentUserRoles.Length == 0)
                {
                    var currentUser = CurrentUser;
                    _currentUserRoles = currentUser != null ? _userService.GetRoles(currentUser.Id) : new string[0];
                }
                return _currentUserRoles;
            }
        }

        private Language? _currentLanguage;

        public Language CurrentLanguage
        {
            get
            {
                if (!_currentLanguage.HasValue)
                {
                    var culture = GetLanguageHelper.GetLanguage(RouteData, Request).ToLower();

                    if (culture.StartsWith("en"))
                        _currentLanguage = Language.English;
                    else
                        _currentLanguage = Language.Russian;
                }
                return _currentLanguage.Value;
            }
        }

        public Uri AdvertiserReferralUri
        {
            get
            {
#if DEBUG
                Uri baseUri = new Uri("http://kigl.eu:8000");

#else
            Uri baseUri = new Uri("http://kigl.eu");
#endif
                var lang = CurrentLanguage == Language.Russian ? "ru" : "en";
                Uri advertiserReferralUri = new Uri(baseUri, lang + "/RefAdv/" + CurrentUser.ReferralCode);

                return advertiserReferralUri;
            }
        }

        public Uri InstagramerReferralUri
        {
            get
            {
#if DEBUG
                Uri baseUri = new Uri("http://kigl.eu:8000");

#else
            Uri baseUri = new Uri("http://kigl.eu");
#endif
                var lang = CurrentLanguage == Language.Russian ? "ru" : "en";
                Uri instagramerReferralUri = new Uri(baseUri, lang + "/RefInst/" + CurrentUser.ReferralCode);

                return instagramerReferralUri;
            }
        }

        protected void SignIn(Guid userId)
        {
            var claim = new ClaimsIdentity("ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            claim.AddClaim(new Claim(ClaimsIdentity.DefaultNameClaimType, userId.ToString(), ClaimValueTypes.String));
            claim.AddClaim(new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider",
                "OWIN Provider", ClaimValueTypes.String));

            AuthenticationManager.SignOut();
            AuthenticationManager.SignIn(new AuthenticationProperties
            {
                IsPersistent = false
            }, claim);
        }

        protected ActionResult SuccessResult()
        {
            return Json(new { success = true }, JsonRequestBehavior.AllowGet);
        }

        protected ActionResult SuccessCamelCaseResult(object data)
        {
            return new JsonCamelCaseResult(data, JsonRequestBehavior.AllowGet);
        }

        protected ActionResult SuccessResult(object data)
        {
            return Json(new { success = true, data }, JsonRequestBehavior.AllowGet);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using Kigl.Domain.Contracts;

namespace Kigl.Controllers
{
    public class InfoController : BaseController
    {
        public InfoController(IUserService userService) : base(userService)
        {
        }

        public ActionResult FAQ()
        {
            if (CurrentLanguage == Language.Russian)
                return View("FAQRus");
            return View();
        }

        public ActionResult CampaignInitiatorsTermsOfUse()
        {
            if (CurrentLanguage == Language.Russian)
                return View("CampaignInitiatorsTermsOfUseRus");
            return View();
        }

        public ActionResult PublisherTermsOfUse()
        {
            if (CurrentLanguage == Language.Russian)
                return View("PublisherTermsOfUseRus");
            return View();
        }

        public ActionResult PrivacyPolicy()
        {
            if (CurrentLanguage == Language.Russian)
                return View("PrivacyPolicyRus");
            return View();
        }
    }
}
﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Common.DAL.Entities;
using Common.Logging;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using Kigl.Common.Enums;
using Kigl.Common.Models;
using Kigl.Domain.Contracts;
using Kigl.Domain.Models;
using Kigl.Filters;
using Kigl.Helpers;
using Kigl.Models;


namespace Kigl.Controllers
{
    public class ContentController : BaseController
    {
        private readonly IContentService _contentService;
        private readonly IInstagramParserService _instagramParserService;
        private readonly IEmailService _emailService;

        public ContentController(IUserService userService, IContentService contentService, IInstagramParserService instagramParserService, IEmailService emailService) : base(userService)
        {
            _contentService = contentService;
            _instagramParserService = instagramParserService;
            _emailService = emailService;
        }

        private UserAdModel PrepareUserAdModel(Ad ad)
        {
            var model = MapModel<UserAdModel>(ad);
            var userAd = ad.UserAds.First(item => item.UserId == CurrentUser.Id);

            if (!ad.IsActive && (userAd.VerifyStatus == VerifyStatus.Downloaded || userAd.VerifyStatus == VerifyStatus.Failed))
                model.VerifyStatus = VerifyStatus.CampaignStopped;
            else
                model.VerifyStatus = userAd.VerifyStatus;

            model.UserAmount = userAd.UserAmount;
            return model;
        }

        private AdForShowingModel PrepareAdForShowingModel(Ad ad)
        {
            var model = MapModel<AdForShowingModel>(ad);
            var adFile = ad.Files.First(item => item.FileAdType != FileAdType.Thumbnail);
            var thumbnailFile = ad.Files.FirstOrDefault(item => item.FileAdType == FileAdType.Thumbnail);
            model.DownloadUrl = Url.Action("DownloadFile", "Files", new { id = ad.Id });
            if (model.AdType == AdType.InstagramImage)
            {
                model.FileUrl = Url.Action("GetFileByName", "Files", new {fileName = adFile.FileName, adType = AdType.Banner});
            }
            if (model.AdType == AdType.InstagramVideo)
            {
                model.FileUrl = Url.Action("GetVideoFileByName", "Files", new { fileName = adFile.FileName });
            }
            if (thumbnailFile != null)
            {
                model.ThumbnailUrl = Url.Action("GetFileByName", "Files", new { fileName = thumbnailFile.FileName, adType = AdType.Banner });
            }
            return model;
        }

        private T MapModel<T>(Ad ad) where T : ControlPanel.Domain.Models.Ad, new()
        {
            return new T
            {
                Id = ad.Id,
                Name = ad.Name,
                Caption = ad.Caption,
                AdType = ad.AdType,
                Files = ad.Files.Select(item => new ControlPanel.Domain.Models.File
                {
                    Id = item.Id,
                    FileName = item.FileName,
                    UploadedFileName = item.UploadedFileName,
                    FileAdType = item.FileAdType
                }).ToArray()
            };
        }

        [HttpGet]
        [UserInfoRequired]
        public ActionResult Choose(bool removeCookie = false, Guid? downloadAdId = null)
        {
            Log.Info($"User.Identity.Name {User.Identity.Name}");

            if (removeCookie)
            {
                RemoveContentCookie("chooseModule");
                RemoveContentCookie("choosedItem");
                TempData["downloadAdId"] = downloadAdId;
                return RedirectToAction("Choose");
            }
            downloadAdId = (Guid?)TempData["downloadAdId"];
            if (CurrentUser != null)
            {
                if (String.IsNullOrEmpty((String)Session["InstagramPicUrl"]))
                {
                    Session["InstagramPicUrl"] = CurrentUser.PhotoUrl;
                }



                var model = new ChooseAdModel
                {
                    UserHasCategories = CurrentUser.InstagramCategories.Any(),
                    HasAdForShowing = _contentService.HasAdForShowing(CurrentUser),
                    HasWithdraws = _contentService.HasWithdraws(CurrentUser),
                    DownloadUrl = downloadAdId.HasValue ? Url.Action("DownloadFile", "Files", new { id = downloadAdId }) : null,
                    InstagramerName = CurrentUser.Name,
                    InstagramerUserName = CurrentUser.InstagramLogin,
                    InstagramerEmail = CurrentUser.Email,
                    InstagramerPayPal = CurrentUser.Paypal,
                    InstagramerPhoneNumber = CurrentUser.Phone,
                    AdvertiserReferralLink = AdvertiserReferralUri.ToString(),
                    InstagramerReferralLink = InstagramerReferralUri.ToString()
                };
                var ads = _contentService.GetUserAds(CurrentUser.Id);
                model.UserAds = ads.Select(PrepareUserAdModel).ToArray();
                Log.Info($"user {CurrentUser.Name} ({CurrentUser.Id}) successfully opend choose page.");
                return View(model);
            }
            return RedirectToAction("Index", "Home");
        }

        private void RemoveContentCookie(string cookieName)
        {
            if (Request.Cookies[cookieName] != null)
            {
                var c = new HttpCookie(cookieName)
                {
                    Expires = DateTime.Now.AddDays(-1),
                    Path = "/Content"
                };
                HttpContext.Response.Cookies.Add(c);
            }
        }

        public ActionResult GetContent()
        {
            try
            {
                var ads = _contentService.GetAdForShowing(CurrentUser);
                var model = ads.Select(PrepareAdForShowingModel).ToArray();
                return SuccessCamelCaseResult(model);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return null;
            }
        }

        public ActionResult AdvertiserFacebookShare()
        {
            return FacebookShare(AdvertiserReferralUri.ToString());
        }

        public ActionResult InstagramerFacebookShare()
        {
            return FacebookShare(InstagramerReferralUri.ToString());
        }

        public ActionResult FacebookShare(String targetUrl)
        {
            try
            {

                var lang = CurrentLanguage == Language.Russian ? "ru" : "en";
#if DEBUG
                Uri baseUri = new Uri("http://kigl.eu:8000");

#else
                Uri baseUri = new Uri("http://kigl.eu");
#endif
                Uri futureRedirectUrl = new Uri(baseUri, lang + "/Content/Choose");

                Facebook.FacebookService fb = new Facebook.FacebookService();
                var redirectUrl = fb.Share(targetUrl, "testCaption", futureRedirectUrl.ToString());
                return Redirect(redirectUrl);
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return null;
            }
        }

        public ActionResult AddUserAd(Guid adId, int followersCount, DeliveryMethod deliveryMethod)
        {
            try
            {
                Log.Info($"Adding user ad. Name: {CurrentUser.Name} ({CurrentUser.Id}), adId: {adId}, followersCount: {followersCount}, deliveryMethod: {deliveryMethod}");
                _contentService.AddUserAd(CurrentUser.Id, adId, followersCount);
                var ad = _contentService.GetAd(adId);

                switch (deliveryMethod)
                {
                    case DeliveryMethod.Email:
                        var file = ad.Files.FirstOrDefault(item => item.FileAdType != FileAdType.Thumbnail);

                        if (file != null)
                        {
                            Task.Factory.StartNew(() =>
                            {
                                var subject = "Instagram content";
                                if (!string.IsNullOrWhiteSpace(file.SharedLink))
                                {

                                    var body = GetBody("mail_v3.html", subject, ad.Caption, ad.AdType, file.SharedLink);
                                    _emailService.SendEmail(CurrentUser.Email, subject, body);
                                }
                                else
                                {
                                    var filePath = Path.Combine(DirectoryHelper.GetAdDirectory(ad.AdType), file.FileName);
                                    using (var attachment = new FileStream(filePath, FileMode.Open))
                                    {
                                        var body = GetBody("mail_v2.html", subject, ad.Caption, ad.AdType);
                                        _emailService.SendEmail(CurrentUser.Email, subject, body, attachment, file.UploadedFileName);
                                    }
                                }
                            });
                        }

                        break;
                }
                var downloadAdId = deliveryMethod == DeliveryMethod.DirectDownload ? adId : (Guid?) null;
                return RedirectToAction("Choose", new {removeCookie = true, downloadAdId});
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return null;
            }
        }

        private string GetBody(string templateName, string subject, string caption, AdType type, string sharedLink = null)
        {
            var lang = CurrentLanguage == Language.Russian ? "ru" : "en";

             return System.IO.File.ReadAllText(Server.MapPath($"~/EmailTemplates/{lang}/{templateName}"))
                .Replace("*|MC:SUBJECT|*", subject)
                .Replace("{Caption}", caption.Substring(0, Math.Min(caption.Length, 2000)))
                .Replace("{SharedLink}", sharedLink)
                .Replace("{Ad}", type == AdType.InstagramImage ? "картинку" : "видео")
                .Replace("{This}", type == AdType.InstagramImage ? "ее" : "его")
                .Replace("{ThisAd}", type == AdType.InstagramImage ? "данной картинкой" : "данным видео");
        }

        public ActionResult Verify(Guid adId)
        {
            try
            {
                Log.Info($"Verifying ad: {adId}");
                var ad = _contentService.GetAd(adId);
                var verifyResult = _instagramParserService.CheckText(CurrentUser.InstagramLogin, ad.Caption);
                Log.Info($"Verifying result ad {adId}: {verifyResult}");
                var verifyStatus = _contentService.UpdateVerifyStatus(verifyResult, CurrentUser, adId);
                Log.Info($"Verify status ad {adId}: {verifyStatus}");
                return SuccessResult(verifyStatus.ToString());
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return null;
            }
        }

        public ActionResult ResetVerifyInfo(Guid adId)
        {
            try
            {
                Log.Info($"Resetting verify status, ad: {adId}");
                _contentService.ResetVerifyStatus(CurrentUser.Id, adId);
                return SuccessResult();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message, ex);
                return null;
            }
        }

        public ActionResult CreateWithdrawRequest(string account, WithdrawType type)
        {
            _contentService.AddWithdrawRequest(account, type, CurrentUser.Id);

            var lang = CurrentLanguage == Language.Russian ? "ru" : "en";
            var bodyToUser = System.IO.File.ReadAllText(Server.MapPath($"~/EmailTemplates/{lang}/05_PayPal-change.html"));
            _emailService.SendEmail(CurrentUser.Email, Resources.Global.Email_05_PayPal, bodyToUser);

            return SuccessResult();
        }

        [HttpPost]
        public ActionResult UpdatePayPal(string account)
        {
            _contentService.UpdatePayPal(account, CurrentUser.Id);

            var lang = CurrentLanguage == Language.Russian ? "ru" : "en";
            var bodyToUser = System.IO.File.ReadAllText(Server.MapPath($"~/EmailTemplates/{lang}/05_PayPal-change.html"));
            _emailService.SendEmail(CurrentUser.Email, Resources.Global.Email_05_PayPal, bodyToUser);

            return SuccessResult();
        }

        [HttpPost]
        public ActionResult UpdatePhone(string phone)
        {
            _contentService.UpdatePhone(phone, CurrentUser.Id);
            return SuccessResult();
        }

        public ActionResult GetPayments(int paymentsCount, String startDate, String endDate, int? paymentType, int page)
        {
            DateTime? dateFrom = null;
            if (!String.IsNullOrEmpty(startDate))
            {
                dateFrom = DateTime.ParseExact(startDate, "dd.MM.yyyy", null);
            }

            DateTime? dateTo = null;
            if (!String.IsNullOrEmpty(endDate))
            {
                dateTo = DateTime.ParseExact(endDate, "dd.MM.yyyy", null);
            }

            var result = _contentService.GetPayments(CurrentUser.Id, paymentsCount, dateFrom, dateTo, paymentType, page, CurrentLanguage.ToString());
            return SuccessResult(result);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Filters;
using Kigl.Models;

namespace Kigl.Controllers
{
    [KiglAuthorize(RoleName.Admin)]
    public class BankAccountsController : BaseController
    {
        private readonly IAccountsService _accountService;
        private readonly IAdvertiserService _advertiserService;
        private readonly ISettingsService _settingsService;

        public BankAccountsController(IUserService userService, IAccountsService accountService, IAdvertiserService advertiserService, ISettingsService settingsService) : base(userService)
        {
            _accountService = accountService;
            _advertiserService = advertiserService;
            _settingsService = settingsService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new ListAccountModel
            {
                Accounts = _accountService.GetAccounts(),
                Advertisers = _advertiserService.GetAdvertisers(true),
                Currencies = _settingsService.GetCurrencies()
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(EditAccountModel model)
        {
            _accountService.AddOrUpdateAccount(model);
            return RedirectToAction("Index");
        }
    }
}
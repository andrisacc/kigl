﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Filters;
using Kigl.Models;

namespace Kigl.Controllers
{
    [KiglAuthorize(RoleName.Admin)]
    public class CompanyController : BaseController
    {
        private readonly IAdvertiserService _advertiserService;

        public CompanyController(IUserService userService, IAdvertiserService advertiserService) : base(userService)
        {
            _advertiserService = advertiserService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var model = new ListAdvertiserModel
            {
                Advertisers = _advertiserService.GetAdvertisersOrderedByTime()
            };
            return View(model);
        }

        [HttpGet]
        public ActionResult ValidateCompanyName(string name)
        {
            var result = _advertiserService.ValidateAdvertiser(name);
            return SuccessCamelCaseResult(result);
        }

        [HttpGet]
        public ActionResult ValidateEditedCompanyName(string name, string advertiserId)
        {
            var result = _advertiserService.ValidateEditedAdvertiser(name, advertiserId);
            return SuccessCamelCaseResult(result);
        }

        [HttpPost]
        public ActionResult Edit(EditAdvertiserModel model, string submit)
        {
            if(String.IsNullOrEmpty(submit) || submit == Resources.Global.Admin_Edit)
                _advertiserService.AddOrUpdateAdvertiser(model);

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Remove(String advertiserId)
        {
            Guid id;
            if(Guid.TryParse(advertiserId, out id))
            {
                _advertiserService.Remove(id);
            }

            return null;
        }

        [HttpPost]
        public ActionResult UpdateApprovedStatus(String advertiserId, Boolean isApproved)
        {
            Guid id;
            if (Guid.TryParse(advertiserId, out id))
            {
                _advertiserService.UpdateApprovedStatus(id, isApproved);

                if (isApproved)
                {
                    _advertiserService.UpdateRejectedStatus(id, false);
                }
            }

            return null;
        }

        [HttpPost]
        public ActionResult UpdateRejectedStatus(String advertiserId, Boolean isRejected)
        {
            Guid id;
            if (Guid.TryParse(advertiserId, out id))
            {
                _advertiserService.UpdateRejectedStatus(id, isRejected);

                if (isRejected)
                {
                    _advertiserService.UpdateApprovedStatus(id, false);
                }
            }

            return null;
        }
    }
}
﻿using System.Linq;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Filters;
using Kigl.Models;
using System.Threading;

namespace Kigl.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IStatisticService _statisticService;
        private readonly IAdvertiserService _advertiserService;

        public HomeController(IUserService userService, IStatisticService statisticService, IAdvertiserService advertiserService) : base(userService)
        {
            _statisticService = statisticService;
            _advertiserService = advertiserService;
        }

        public ActionResult Index()
        {
            if (CurrentUser != null)
            {
                if (!string.IsNullOrWhiteSpace(CurrentUser.FacebookId))
                    return RedirectToAction("Choose", "Content", new { removeCookie = true });
                
                AuthenticationManager.SignOut();
                return RedirectToAction("Index");
            }
            ViewBag.FbLang = Thread.CurrentThread.CurrentUICulture.Name;
            return View();
        }

        [KiglAuthorize]
        public ActionResult AdvertiserIndex()
        {
            ListStatisticModel model;
            if (CurrentUser.AdvertiserId.HasValue)
            {
                var statistic = _statisticService.GetStatistic(CurrentUser.AdvertiserId.Value);
                var advretiser = _advertiserService.GetAdvertiser(CurrentUser.AdvertiserId.Value);
                model = new ListStatisticModel
                {
                    AdvertiserBrandName = advretiser.BrandName,
                    IsApproved = advretiser.IsApproved,
                    AccountBalance = _advertiserService.GetAdvertiserBalance(CurrentUser.AdvertiserId.Value),
                    Statistics = statistic.Statistics,
                    InstagramStatistics = statistic.InstagramStatistics
                };
                model.Total = model.Statistics.Sum(item => item.Total);
            }
            else
            {
                model = null;
            }
            return View(model);
        }
    }
}
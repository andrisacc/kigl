﻿using System;
using System.Resources;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Kigl.Domain.Contracts;
using Kigl.Models;
using ControlPanel.Common.Enums;
using System.Configuration;
using ControlPanel.Domain.Contracts;
using ControlPanel.Domain.Models;

namespace Kigl.Controllers
{
    public class AccountController : BaseController
    {
        private readonly IEmailService _emailService;
        private readonly IAdvertiserService _advertisersService;

        public AccountController(IUserService userService, IEmailService emailService, IAdvertiserService advertisersService) : base(userService)
        {
            _emailService = emailService;
            _advertisersService = advertisersService;
        }

        [HttpGet]
        public ActionResult SignOut()
        {
            Log.Info($"admin {CurrentUser.Username} ({CurrentUser.Id})successfully signed out");
            AuthenticationManager.SignOut();
            return RedirectToAction("AdvertiserIndex", "Home");
        }

        [HttpGet]
        public ActionResult InstagramLogout()
        {
            Log.Info($"user {CurrentUser.Name} ({CurrentUser.Id}) successfully signed out");
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public ActionResult Advertisers(String type)
        {
            var model = new LoginRegistrationUserModel();

            model.IsSigningUp = type == "signup";

            return View(model);
        }

        [HttpPost]
        public ActionResult Advertisers(LoginRegistrationUserModel model)
        {
            model.LoginErrorMessage = null;
            model.SignUpErrorMessage = null;

            var user = _userService.Check(model.Username, model.Password);
            if (user != null)
            {
                if(user.AdvertiserId.HasValue)
                {
                    var advertiser = _advertisersService.GetAdvertiser(user.AdvertiserId.Value);

                    if(advertiser.IsRejected)
                    {
                        return View(model);
                    }
                }

                SignIn(user.Id);
                Log.Info($"admin {model.Username} ({user.Id}) successfully signed in");
                return RedirectToAction("AdvertiserIndex", "Home");
            }
            else
            {
                model.LoginErrorMessage = Resources.Global.Account_Login_IncorrectLoginPasswordError;
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult SignUp(LoginRegistrationUserModel model)
        {
            model.LoginErrorMessage = null;
            model.SignUpErrorMessage = null;

            if (_userService.ValidateAdvertiserEmail(model.Email))
            {
                Advertiser advertiser = new Advertiser()
                {
                    AccountLink = model.AccountLink,
                    BrandName = model.BrandName,
                    ContactPerson = model.ContactPerson,
                    Email = model.Email,
                    IsApproved = false,
                    Name = model.BrandName,
                };
                _advertisersService.AddOrUpdateAdvertiser(advertiser);
                advertiser = _advertisersService.GetAdvertiserByEmail(model.Email);

                if (advertiser != null)
                {
                    var lang = CurrentLanguage == Language.Russian ? "ru" : "en";

                    var user = _userService.Create(model.Email, model.Email, model.Email, model.Password, advertiser.Id, null, RoleName.Advertiser, lang);
                    _userService.SaveChanges();
                    
                    var body = System.IO.File.ReadAllText(Server.MapPath($"~/EmailTemplates/{lang}/newAdvertiser.html"))
                        .Replace("{{BrandName}}", advertiser.BrandName)
                        .Replace("{{ContactPerson}}", advertiser.ContactPerson)
                        .Replace("{{Email}}", advertiser.Email)
                        .Replace("{{AccountLink}}", advertiser.AccountLink);
                    _emailService.SendEmail("new_insta@kigl.eu", "New advertiser", body);

                    if (user != null)
                    {
                        SignIn(user.Id);

                        var referralUserCode = "";
                        var referralCookie = Request.Cookies[Constants.Referral];
                        if (referralCookie != null)
                        {
                            referralUserCode = referralCookie.Value;
                            _userService.Update(user.Id, null, model.Email, null, false, lang, referralUserCode);
                            _userService.SaveChanges();
                        }

                        Log.Info($"new advertiser model.AccountLink: {model.AccountLink}; model.BrandName: {model.BrandName}; model.ContactPerson: {model.ContactPerson}; model.Email: {model.Email}; model.BrandName: {model.BrandName}; successfully signed up and signed in");
                        return RedirectToAction("AdvertiserIndex", "Home");
                    }
                }
            }
            else
            {
                model.SignUpErrorMessage = Resources.Global.Account_SignUp_EmailAlreadyExistsError;
                return View(model);
            }
            //var user = _userService.Check(model.Username, model.Password);
            //if (user != null)
            //{
            //    SignIn(user.Id);
            //    Log.Info($"admin {model.Username} ({user.Id})successfully signed in");
            //    return RedirectToAction("AdvertiserIndex", "Home");
            //}
            //return View(model);
            return RedirectToAction("Advertisers");
        }

        [HttpPost]
        public ActionResult ValidateSignUpEmail(string email)
        {
            var result = _userService.ValidateAdvertiserEmail(email);
            return SuccessCamelCaseResult(result);
        }

        public ActionResult FacebookLogin()
        {
            Facebook.FacebookService fbService = new Facebook.FacebookService();

            #if DEBUG
            var currentHost = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
            if(currentHost == "http://localhost:60576")
            {
                var facebookId = "133926907351211";
                var user = _userService.GetByFacebookId(facebookId);

                SignIn(user.Id);
                return RedirectToAction("Choose", "Content");
            }
            #endif

            return Redirect(fbService.GetRedirectUrl());
        }

        public ActionResult FacebookLoginResponse(string code, string state)
        {
            var values = string.Join(", ",
                    Request.QueryString
                   .AllKeys
                   .Select(key => key + ": " + Request.QueryString[key])
                   .ToArray());

            Log.Info($"facebook code: {code}, facebookfullresponse: {values}");

            Facebook.FacebookService fbService = new Facebook.FacebookService();
            var facebookId = fbService.GetUserId(code);

            var name = fbService.Name;

            var user = _userService.GetByFacebookId(facebookId);

            if (user == null && !string.IsNullOrWhiteSpace(facebookId))
            {
                user = _userService.Create(facebookId, name);
                _userService.SaveChanges();
            }

            if(user != null)
            {
                _userService.UpdatePic(user.Id, fbService.PicUrl);
                _userService.SaveChanges();
            }

            if (user == null)
            {
                return RedirectToAction("Index", "Home");
            }

            SignIn(user.Id);
            Log.Info($"user {name} (fb account id: {facebookId}) ({user.Id}) successfully signed in");
            Session["InstagramPicUrl"] = fbService.PicUrl;
            return RedirectToAction("Choose", "Content");
        }

        [HttpGet]
        public ActionResult Info()
        {
            var user = _userService.GetById(CurrentUser.Id.ToString());
            UpdateUserModel model = new UpdateUserModel();
            if (user != null)
            {
                model = new UpdateUserModel() { Email = user.Email, InstagramLogin = user.InstagramLogin, Phone = user.Phone, Terms = user.Terms? user.Terms.ToString(): null };
            }
            
            return View(model);
        }

        [HttpPost]
        public ActionResult Info(UpdateUserModel info)
        {
            info.Phone = info.Phone.TrimStart('0');
            info.InstagramLogin = info.InstagramLogin.Trim();
            var lang = CurrentLanguage == Language.Russian ? "ru" : "en";

            var referralUserCode = "";
            var referralCookie = Request.Cookies[Constants.Referral];
            if (referralCookie != null)
                referralUserCode = referralCookie.Value;

            _userService.Update(CurrentUser.Id, info.InstagramLogin, info.Email, info.Phone, !string.IsNullOrEmpty(info.Terms), lang, referralUserCode);
            var body = System.IO.File.ReadAllText(Server.MapPath($"~/EmailTemplates/{lang}/newInstagrammer.html"))
                .Replace("{{Name}}", CurrentUser.Name)
                .Replace("{{Instagram}}", info.InstagramLogin)
                .Replace("{{Email}}", info.Email)
                .Replace("{{Phone}}", info.Phone);
            _emailService.SendEmail("new_insta@kigl.eu", "New instagrammer", body);
            Log.Info($"New instagrammer sent email successfully about {info.InstagramLogin} to new_insta@kigl.eu");
            _userService.SaveChanges();

            Log.Info($"Register new user. Name: {CurrentUser.Name}; Instagram: {CurrentUser.InstagramLogin}; Email: {CurrentUser.Email}; Phone {CurrentUser.Phone}; Id: {CurrentUser.Id}; facebookId: {CurrentUser.FacebookId}");

            if (!String.IsNullOrEmpty(info.Phone) && !String.IsNullOrEmpty(info.InstagramLogin) && !String.IsNullOrEmpty(info.Email) && !string.IsNullOrEmpty(info.Terms))
            {
                var bodyToUser = System.IO.File.ReadAllText(Server.MapPath($"~/EmailTemplates/{lang}/01_Welcom-email.html"));
                _emailService.SendEmail(info.Email, Resources.Global.Email_01_Welcom, bodyToUser);
            }

            return RedirectToAction("Choose", "Content", new { removeCookie = true });
        }

        [HttpPost]
        public ActionResult UpdateUser(string phone, string instagram, string email, string terms)
        {
            var lang = CurrentLanguage == Language.Russian ? "ru" : "en";
            _userService.Update(CurrentUser.Id, instagram, email, phone, !string.IsNullOrEmpty(terms), lang, "");
            _userService.SaveChanges();

            return null;
        }

        [HttpPost]
        public ActionResult LogClientInfo(string info)
        {
            Log.Info($"client info for user {CurrentUser.Name} (fb account id: {CurrentUser.FacebookId}) ({CurrentUser.Id}): {info}");

            return null;
        }

        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        [HttpGet]
        public ActionResult PasswordRecovery(string code)
        {
            var user = _userService.CheckRecoveryCode(code);

            if(user != null)
            {
                RecoveryEnterPasswordModel model = new RecoveryEnterPasswordModel() { };
                Session["UserId"] = user.Id.ToString();
                return View(model);
            }
            else
            {
                return RedirectToAction("Advertisers");
            }
        }

        [HttpGet]
        public ActionResult PasswordRecoverySuccess()
        {
            return View();
        }

        [HttpPost]
        public ActionResult PasswordRecovery(RecoveryEnterPasswordModel model)
        {
            if(model.NewForgotPass != model.RepeatNewForgotPass)
            {
                return View(model);
            }

            var userIdStr = (String)Session["UserId"];
            Guid userId = Guid.Empty;
            if (String.IsNullOrEmpty(userIdStr) || !Guid.TryParse(userIdStr, out userId))
            {
                RedirectToAction("Advertisers");
            }

            var email = _userService.UpdatePassword(userId, model.NewForgotPass);
            _userService.SaveChanges();

            if (!String.IsNullOrEmpty(email))
            {
                var lang = CurrentLanguage == Language.Russian ? "ru" : "en";
                var body = System.IO.File.ReadAllText(Server.MapPath($"~/EmailTemplates/{lang}/08_Password_access_changed.html"));

                var emailCulture = lang == "ru" ? new System.Globalization.CultureInfo("ru-Ru") : new System.Globalization.CultureInfo("en-Us");
                ResourceManager manager = new ResourceManager("Resources.Global", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                var subject = manager.GetString("Email_08_Password_access_changed", emailCulture);

                 _emailService.SendEmail(email, subject, body);

                return RedirectToAction("PasswordRecoverySuccess");
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult ForgotPassword(RecoveryPasswordModel model)
        {
            var code = _userService.GetPasswordRecoveryCode(model.ForgotEmail);
            _userService.SaveChanges();

            if(!String.IsNullOrEmpty(code))
            {
                AuthenticationManager.SignOut();
                var lang = CurrentLanguage == Language.Russian ? "ru" : "en";

#if DEBUG
                Uri baseUri = new Uri("http://kigl.eu:8000");

#else
                Uri baseUri = new Uri("http://kigl.eu");
#endif

                Uri myUri = new Uri(baseUri, lang + "/Account/PasswordRecovery?code=" + HttpUtility.UrlEncode(code));

                var body = System.IO.File.ReadAllText(Server.MapPath($"~/EmailTemplates/{lang}/07_Forgot_password.html"))
                    .Replace("{{Link}}", myUri.ToString());
                
                var emailCulture = lang == "ru" ? new System.Globalization.CultureInfo("ru-Ru") : new System.Globalization.CultureInfo("en-Us");
                ResourceManager manager = new ResourceManager("Resources.Global", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                var subject = manager.GetString("Email_07_Forgot_password", emailCulture);

                _emailService.SendEmail(model.ForgotEmail, subject, body);

                return RedirectToAction("ForgotPasswordSuccess");
            }
            else
            {
                return RedirectToAction("ForgotPasswordFail");
            }
        }

        [HttpGet]
        public ActionResult ForgotPasswordFail()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ForgotPasswordSuccess()
        {
            return View();
        }

        [HttpPost]
        public ActionResult BackToLogin()
        {
            return RedirectToAction("Advertisers");
        }
    }
}
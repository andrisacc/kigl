﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.DAL.Entities;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Filters;
using Kigl.Models;
using System.Resources;


namespace Kigl.Controllers
{
    [KiglAuthorize]
    public class UsersController : BaseController
    {        
        private readonly IAdvertiserService _advertiserService;
        private readonly ISettingsService _settingsService;
        private readonly IInstagramParserService _instagramParserService;
        private readonly IEmailService _emailService;

        public UsersController(IUserService userService, IAdvertiserService advertiserService, ISettingsService settingsService, IInstagramParserService instagramParserService, IEmailService emailService) : base(userService)
        {            
            _advertiserService = advertiserService;
            _settingsService = settingsService;
            _instagramParserService = instagramParserService;
            _emailService = emailService;
        }

        private ControlPanel.Domain.Models.User UpdateFollowersCountForUser(ControlPanel.Domain.Models.User user)
        {
            var info = _userService.GetFollowers(user.Id);
            user.FollowersCnt = info!=null?info.FollowedBy:0;
            user.IsLastFollowersRequestSuccess = info != null ? info.IsLastFollowersRequestSuccess:false;
            return user;
        }

        private ControlPanel.Domain.Models.User UpdateWithdrawAccount(ControlPanel.Domain.Models.User user)
        {
            user.WithdrawAccount = _userService.GetUserLastWithdrawAccount(user.Id);
            return user;
        }

        [HttpGet]
        public ActionResult Instagramers()
        {
            var isInstagramAdmin = CurrentUserRoles.Contains(RoleName.InstagramAdmin);
            var model = new ListUserModel
            {
                Users = _userService.GetInstagrammers(CurrentUser.AdvertiserId, isInstagramAdmin)/*.Select(UpdateFollowersCountForUser).Select(UpdateWithdrawAccount)*/.ToArray(),
                IsAdmin = CurrentUserRoles.Contains(RoleName.Admin),
                IsAdvertiser = CurrentUserRoles.Contains(RoleName.Advertiser),
                IsInstagramAdmin = isInstagramAdmin,
                Advertisers = _advertiserService.GetAdvertisers(),
                Networks = _settingsService.GetNetworks()
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult AdvertiserUsers()
        {
            var isInstagramAdmin = CurrentUserRoles.Contains(RoleName.InstagramAdmin);
            var model = new ListUserModel
            {
                Users = _userService.GetUsers(CurrentUser.AdvertiserId, isInstagramAdmin).Select(UpdateFollowersCountForUser).Select(UpdateWithdrawAccount).ToArray(),
                IsAdmin = CurrentUserRoles.Contains(RoleName.Admin),
                IsAdvertiser = CurrentUserRoles.Contains(RoleName.Advertiser),
                IsInstagramAdmin = isInstagramAdmin,
                Advertisers = _advertiserService.GetAdvertisers(),
                Networks = _settingsService.GetNetworks()
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult AdminUsers()
        {
            var isInstagramAdmin = CurrentUserRoles.Contains(RoleName.InstagramAdmin);
            var model = new ListUserModel
            {
                Users = _userService.GetUsers(CurrentUser.AdvertiserId, isInstagramAdmin)
                    .Where(u => u.FacebookId == null)
                    .Select(UpdateFollowersCountForUser)
                    .Select(UpdateWithdrawAccount).ToArray(),
                IsAdmin = CurrentUserRoles.Contains(RoleName.Admin),
                IsAdvertiser = CurrentUserRoles.Contains(RoleName.Advertiser),
                IsInstagramAdmin = isInstagramAdmin,
                Advertisers = _advertiserService.GetAdvertisers(),
                Networks = _settingsService.GetNetworks()
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult ValidateUserName(string userName)
        {
            var result = _userService.ValidateUserName(userName);
            return SuccessCamelCaseResult(result ? "true" : "User with such name already exists");
        }

        [HttpPost]
        public ActionResult Edit(EditUserModel model, string submit)
        {
            if (submit == Resources.Global.Admin_Edit)
            {
                if (model.Id == Guid.Empty)
                {
                    var isAdmin = CurrentUserRoles.Contains(RoleName.Admin);
                    Guid? advertiserId = null, networkId = null;
                    if (model.RoleName == RoleName.Advertiser)
                        advertiserId = isAdmin ? model.AdvertiserId : CurrentUser.AdvertiserId;
                    if (model.RoleName == RoleName.NetworkAdmin)
                        networkId = model.NetworkId;
                    _userService.Create(model.UserName, model.UserName, model.Password, advertiserId, networkId, model.RoleName);
                }
                else
                {
                    var user = _userService.GetById(model.Id.ToString());

                    if(user.InstagramCategories.Count() == 0 && model.InstagramCategories.Count() > 0)
                    {
                        var body = System.IO.File.ReadAllText(Server.MapPath($"~/EmailTemplates/{user.RegistrationLang}/02_Moderation(success).html"));

                        var emailCulture = user.RegistrationLang == "ru" ? new System.Globalization.CultureInfo("ru-Ru") : new System.Globalization.CultureInfo("en-Us");

                        ResourceManager manager = new ResourceManager("Resources.Global", global::System.Reflection.Assembly.Load("App_GlobalResources"));

                        var subject = manager.GetString("Email_02_Moderation_success", emailCulture);

                        _emailService.SendEmail(user.Email, subject, body);
                    }

                    _userService.Update(model.Id, model.InstagramCategories);
                }
            }
            else if (submit == Resources.Global.Admin_DeleteUser)
            {
                var user = _userService.GetById(model.Id.ToString());

                if(user != null && !String.IsNullOrEmpty(user.Phone) && !String.IsNullOrEmpty(user.InstagramLogin) && !String.IsNullOrEmpty(user.Email) && user.Terms)
                {
                    var body = System.IO.File.ReadAllText(Server.MapPath($"~/EmailTemplates/{user.RegistrationLang}/03_Moderation(fail).html"));
                    var emailCulture = user.RegistrationLang == "ru" ? new System.Globalization.CultureInfo("ru-Ru") : new System.Globalization.CultureInfo("en-Us");

                    ResourceManager manager = new ResourceManager("Resources.Global", global::System.Reflection.Assembly.Load("App_GlobalResources"));

                    var subject = manager.GetString("Email_03_Moderation_fail", emailCulture);

                    _emailService.SendEmail(user.Email, subject, body);
                }

                _userService.Delete(model.Id);
            }
            _userService.SaveChanges();
            return RedirectToAction("Instagramers");
        }

        [HttpPost]
        public ActionResult EditAdmins(EditUserModel model, string submit)
        {
            if (submit == Resources.Global.Admin_Edit || submit == Resources.Global.Admin_Create)
            {
                if (model.Id == Guid.Empty)
                {
                    var isAdmin = CurrentUserRoles.Contains(RoleName.Admin);
                    Guid? advertiserId = null, networkId = null;
                    if (model.RoleName == RoleName.Advertiser)
                        advertiserId = isAdmin ? model.AdvertiserId : CurrentUser.AdvertiserId;
                    if (model.RoleName == RoleName.NetworkAdmin)
                        networkId = model.NetworkId;
                    _userService.Create(model.UserName, model.UserName, model.Password, advertiserId, networkId, model.RoleName);
                }
                else
                {
                    _userService.Update(model.Id, model.InstagramCategories);
                }
            }
            else if (submit == Resources.Global.Admin_DeleteUser)
            {
                _userService.Delete(model.Id);
            }
            _userService.SaveChanges();
            return RedirectToAction("AdminUsers");
        }

        [HttpPost]
        public ActionResult GetUserCategories(Guid userId)
        {
            var categories = _settingsService.GetInstagramCategories();
            var userCategories = _userService.GetUserInstagramCategories(userId);

            var model = categories.Select(item => new SelectInstagramCategoryModel
            {
                Id = item.Id,
                Name = item.Name,
                Selected = userCategories.Contains(item.Id)
            }).ToArray();

            return SuccessCamelCaseResult(model);
        }
    }
}
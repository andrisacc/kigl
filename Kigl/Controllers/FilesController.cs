﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Helpers;

namespace Kigl.Controllers
{
    public class FilesController : BaseController
    {
        private readonly IAdsService _adsService;
        private readonly IContentService _contentService;

        public FilesController(IUserService userService, IAdsService adsService, IContentService contentService) : base(userService)
        {
            _adsService = adsService;
            _contentService = contentService;
        }

        [AllowAnonymous]
        public ActionResult GetFile(Guid id)
        {
            var file = _adsService.GetFile(id);
            return GetFileInner(file.FileName, file.Ad.AdType);
        }

        [AllowAnonymous]
        public ActionResult GetVideoFile(Guid id)
        {
            var file = _adsService.GetFile(id);
            return GetRangeFile(file.FileName, AdType.Video);
        }

        public ActionResult GetFileByName(string fileName, AdType adType)
        {
            return GetFileInner(fileName, adType);
        }

        public ActionResult GetVideoFileByName(string fileName)
        {
            return GetRangeFile(fileName, AdType.Video);
        }

        public ActionResult DownloadFile(Guid id)
        {
            var file = _contentService.GetFileByAdId(id);
            var cd = new System.Net.Mime.ContentDisposition
            {
                FileName = file.UploadedFileName,
                Inline = false
            };
            Response.AppendHeader("Content-Disposition", cd.ToString());
            if (file.Ad.AdType == AdType.Video || file.Ad.AdType == AdType.InstagramVideo)
                return GetRangeFile(file.FileName, file.Ad.AdType);
            return GetFileInner(file.FileName, file.Ad.AdType);
        }

        private RangeFilePathResult GetRangeFile(string fileName, AdType adType)
        {
            var fullpath = Path.Combine(DirectoryHelper.GetAdDirectory(adType), fileName);
            var fileInfo = new FileInfo(fullpath);
            return new RangeFilePathResult(GetMimeType(adType), fullpath, fileInfo.LastWriteTime, fileInfo.Length);
        }

        private ActionResult GetFileInner(string fileName, AdType adType)
        {
            var path = Path.Combine(DirectoryHelper.GetAdDirectory(adType), fileName);
            return File(path, GetMimeType(adType));
        }

        public ActionResult Upload(HttpPostedFileBase file, AdType adType)
        {
            var fileName = Guid.NewGuid().ToString();
            file.SaveAs(Path.Combine(DirectoryHelper.GetAdDirectory(adType), fileName));
            return Json(new ControlPanel.Domain.Models.File
            {
                FileName = fileName,
                UploadedFileName = file.FileName
            });
        }

        public ActionResult RemoveFile(string fileName, AdType adType)
        {
            System.IO.File.Delete(Path.Combine(DirectoryHelper.GetAdDirectory(adType), fileName));
            return SuccessResult();
        }

        private string GetMimeType(AdType adType)
        {
            switch (adType)
            {
                case AdType.Banner:
                case AdType.HeaderImage:
                case AdType.FooterImage:
                case AdType.InstagramImage:
                    return "image/png";
                case AdType.Video:
                case AdType.InstagramVideo:
                    return "video/mp4";
                default:
                    throw new ArgumentException("Unknown ad type", "adType");
            }            
        }

        public ActionResult UploadPortalImage(HttpPostedFileBase file, PortalImageType portalImageType)
        {
            var fileName = Guid.NewGuid().ToString();
            file.SaveAs(Path.Combine(DirectoryHelper.PortalImagesDirectory, fileName));
            return Json(fileName);
        }

        [AllowAnonymous]
        public ActionResult GetPortalImageFile(Guid id)
        {
            var file = _adsService.GetPortalImage(id);
            var path = Path.Combine(DirectoryHelper.PortalImagesDirectory, file.FileName);
            return File(path, "image/png");
        }

        public ActionResult GetPortalImageFileByName(string fileName)
        {
            var path = Path.Combine(DirectoryHelper.PortalImagesDirectory, fileName);
            return File(path, "image/png");
        }

        public ActionResult RemovePortalImageFile(string fileName)
        {
            System.IO.File.Delete(Path.Combine(DirectoryHelper.PortalImagesDirectory, fileName));
            return SuccessResult();
        }
    }
}
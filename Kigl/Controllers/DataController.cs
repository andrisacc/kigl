﻿using System;
using System.Linq;
using System.Web.Http;
using Common.Logging;
using ControlPanel.ActionFilters;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.ApiModels;
using ControlPanel.Domain.ApiModels.Requests;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Domain.Models.Instaram;
using Kigl.Domain.Models.Nexmo;
using Kigl.Models;

namespace Kigl.Controllers
{
    [AllowCrossSiteJson]
    public class DataController : ApiController
    {
        private readonly IInstagramParserService _instagramParserService;
        private readonly INexmoService _nexmoService;
        private readonly IDataService _dataService;
        private readonly ISettingsService _settingsService;
        private readonly IUserService _userService;

        private static readonly ILog Log = LogManager.GetLogger<DataController>();

        public DataController(IInstagramParserService instagramParserService, INexmoService nexmoService, IDataService dataService, ISettingsService settingsService, IUserService userService)
        {
            _instagramParserService = instagramParserService;
            _nexmoService = nexmoService;
            _dataService = dataService;
            _settingsService = settingsService;
            _userService = userService;
        }

        [HttpPost]
        public string ValidateInstagramLogin(ValidateLoginRequest request)
        {
            Log.Info($"Validating instagram login {request.Login}");
            var followersInfo = _instagramParserService.GetFollowers(request.Login.Trim());
            Log.Info($"Result validation {request.Login}: {followersInfo != null}");
            return followersInfo == null ? "Invalid login" : "true";
        }

        [HttpPost]
        public FollowersInfo GetFollowersInfo(GetFollowersRequest request)
        {
            FollowersInfo followersInfo = null;
            try
            {
                followersInfo = _instagramParserService.GetFollowers(request.InstagramLogin);

                //if (request.InstagramLogin == "ivanovovi")
                //    throw new Exception();
            }
            catch(Exception exc)
            {
                followersInfo = _userService.GetFollowers(request.InstagramLogin);
                Log.Error($"Exception during get followers count for {request.InstagramLogin}: {exc.ToString()}");
            }

            if(followersInfo == null)
                followersInfo = _userService.GetFollowers(request.InstagramLogin);

            followersInfo.Balance = _settingsService.GetUserFollowersBalance(followersInfo.FollowedBy);
            return followersInfo;
        }

        [HttpPost]
        public string SendCode(SendCodeRequest request)
        {
            return _nexmoService.SendCode(request.Number.TrimStart('0'));
        }

        [HttpPost]
        public VerifyResult VerifyCode(VerifyRequest request)
        {
            return _nexmoService.VerifyCode(request.RequestId, request.Code);
        }

        public Ad GetVideo(string userId, Guid networkId)
        {
            bool showTime;
            var timeToShow = _dataService.GetTimeToShow(userId, networkId, out showTime);
            var model = new VideoAd
            {
                TimeToShow = timeToShow
            };
            if (showTime)
            {
                var ad = _dataService.GetVideoForNetwork(networkId);
                if (ad != null)
                {
                    model.Id = ad.Id;
                    model.FileId = ad.Files[0].Id;
                    model.Url = ad.Url;
                }
            }
            return model;
        }

        public Ad[] GetBanner(Guid networkId, int count = 1)
        {
            var ads = _dataService.GetBanners(networkId, count);
            return ads?.Select(item => new Ad
            {
                Id = item.Id,
                FileId = item.Files.First(file => file.FileAdType == FileAdType.Large).Id,
                Url = item.Url
            }).ToArray();
        }

        public PortalImagesModel GetPortalImages(Guid networkId)
        {
            var images = _dataService.GetPortalImages(networkId);
            return new PortalImagesModel
            {
                Header = images.FirstOrDefault(item => item.PortalImageType == PortalImageType.Header),
                Footer = images.FirstOrDefault(item => item.PortalImageType == PortalImageType.Footer)
            };
        }

        [HttpPost]
        public void IncrementNumberOfClicks(IncrementNumberOfClicksRequest request)
        {
            _dataService.IncrementNumberOfClicks(request.NetworkId, request.AdId);
        }
    }
}

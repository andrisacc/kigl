﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Contracts;
using Kigl.Domain.Contracts;
using Kigl.Filters;

namespace Kigl.Controllers
{
    [KiglAuthorize(RoleName.NetworkAdmin)]
    public class NetworkSettingsController : BaseController
    {
        private readonly INetworkSettingsService _networkSettingsService;

        public NetworkSettingsController(IUserService userService, INetworkSettingsService networkSettingsService) : base(userService)
        {
            _networkSettingsService = networkSettingsService;
        }

        public ActionResult Index()
        {
            var model = _networkSettingsService.GetNetworkSettings(CurrentUser.NetworkId.Value);
            return View(model);
        }

        [HttpPost]
        public ActionResult UpdateNetworkSetting(NetworkSetting setting, bool value)
        {
            _networkSettingsService.UpdateNetworkSetting(CurrentUser.NetworkId.Value, setting, value);
            return SuccessResult();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ControlPanel.Common.Enums;
using Kigl.Domain.Contracts;

namespace Kigl.Filters
{
    public class KiglAuthorizeAttribute : ActionFilterAttribute
    {
        private string[] _roleNames;

        public KiglAuthorizeAttribute()
        {
            _roleNames = new string[0];
        }

        public KiglAuthorizeAttribute(params string[] roleNames)
        {
            _roleNames = roleNames;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var userService = (IUserService)DependencyResolver.Current.GetService(typeof(IUserService));
                var userIdentity = HttpContext.Current.User.Identity.Name;
                var user = userService.GetById(userIdentity);
                if(user == null || !string.IsNullOrWhiteSpace(user.FacebookId))
                    RedirectToLogin(filterContext);
                if (_roleNames.Any())
                {
                    
                    Guid userId;
                    if (Guid.TryParse(userIdentity, out userId))
                    {
                        var userRoles = userService.GetRoles(userId);
                        if(!_roleNames.Any(item => userRoles.Contains(item)))
                            RedirectToLogin(filterContext);
                    }
                    else
                        RedirectToLogin(filterContext);
                }
            }
            else
                RedirectToLogin(filterContext);
        }

        private void RedirectToLogin(ActionExecutingContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                {
                    {"controller", Constants.AccountControllerName},
                    {"action", Constants.LoginActionName}
                });
        }
    }
}
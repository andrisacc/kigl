﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using ControlPanel.Common.Enums;
using Kigl.Helpers;

namespace Kigl.Filters
{
    public class LocalizationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var language = GetLanguageHelper.GetLanguage(filterContext.RouteData, filterContext.HttpContext.Request);

            if (string.IsNullOrWhiteSpace(language))
            {
                language = "ru-RU";
            }

            if (!string.IsNullOrWhiteSpace(filterContext.RouteData.Values[Constants.Lang]?.ToString()))
            {
                filterContext.RouteData.Values[Constants.Lang] = language;
            }

            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(language);

            var langCookie = new HttpCookie(Constants.CurrentCulture, Thread.CurrentThread.CurrentUICulture.Name)
            {
                Expires = DateTime.Now.AddYears(1)
            };
            filterContext.HttpContext.Response.SetCookie(langCookie);

            base.OnActionExecuting(filterContext);
        }
    }
}
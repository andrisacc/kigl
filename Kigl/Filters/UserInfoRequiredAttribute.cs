﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using ControlPanel.Common.Enums;
using Kigl.Domain.Contracts;

namespace Kigl.Filters
{
    public class UserInfoRequiredAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var userService = (IUserService) DependencyResolver.Current.GetService(typeof(IUserService));
                var userId = HttpContext.Current.User.Identity.Name;
                var currentUser = userService.GetById(userId);
                if (currentUser != null)
                {
                    if (!string.IsNullOrWhiteSpace(currentUser.FacebookId))
                    {
                        if (string.IsNullOrWhiteSpace(currentUser.InstagramLogin)
                            || string.IsNullOrWhiteSpace(currentUser.Email)
                            || string.IsNullOrWhiteSpace(currentUser.Phone)
                            || !currentUser.Terms)
                        {
                            filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                            {
                                {"controller", Constants.AccountControllerName},
                                {"action", Constants.InfoActionName}
                            });
                        }
                    }
                    else
                    {
                        filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary
                        {
                            {"controller", Constants.AccountControllerName},
                            {"action", Constants.InstagramLogoutActionName}
                        });
                    }
                }
            }
        }
    }
}
(function() {

	'use strict';

	$(function() {




		$('.campaign-list__select').fancySelect(
			{
				showImages: true,
				autoHide  : true
			}
		);

		$(document).on('click', '.campaign-list__item-status .button', function() {

			$('.campaign-list__item-status .droplist').slideUp();
			if($(this).closest('.btns').find('.droplist').css('display') !== 'none') {
				$(this).closest('.btns').find('.droplist').slideUp();
			} else {
				$(this).closest('.btns').find('.droplist').slideDown();
			}
		});

		$(document).on('click', '.campaign-list__item-status .drop-button.visible', function() {
			if ($(this).attr('data-btn') === 'pause') {
				$(this).removeClass('visible');
				$(this).siblings().addClass('visible');
				$(this).closest('.droplist').slideUp();
				$(this).closest('.btns').find('.active').removeClass('active');
				$(this).closest('.btns').find('.pause-btn[data-btn=pause]').addClass('active');
			} else if ($(this).attr('data-btn') === 'active') {
				$(this).removeClass('visible');
				$(this).siblings().addClass('visible');
				$(this).closest('.droplist').slideUp();
				$(this).closest('.btns').find('.active').removeClass('active');
				$(this).closest('.btns').find('.active-btn[data-btn=active]').addClass('active');
			} else if ($(this).attr('data-btn') === 'delete') {
				$(this).removeClass('visible');
				$(this).siblings().addClass('visible');
				$(this).closest('.droplist').slideUp();
				$(this).closest('.btns').find('.active').removeClass('active');
				$(this).closest('.btns').find('.delete-btn[data-btn=delete]').addClass('active');
			}
		});

		$(document).on('click', '.campaign-list__item-details a', function(e) {

			e.preventDefault();

			$('body').css('overflow', 'hidden');
			$('.overlay, .modal').fadeIn();
		});

		$(document).on('click', '.overlay, .modal__close', function(e) {

			e.preventDefault();

			$('body').attr('style', '');
			$('.overlay, .modal').fadeOut();
		});

	});

	$(window).on('load resize', function() {

		if($(window).width() >= 992) {
			/* Данная комбинация выравнивает высоты у блоков( сайдбар по отношению к контенту и формы между левой и правой). */
			var footerRightHeight = $('.footer__right.footer__box').outerHeight(true),
				rightHeightTop = $('.mainSec__right-content').outerHeight(true);

			$('.footer__left.footer__box').css('height', footerRightHeight);

			$('.mainSec__left-content').css('height', rightHeightTop);
		} else {
			$('.footer__left.footer__box').attr('style', '');

			$('.mainSec__left-content').attr('style', '');
		}

	});

	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$('.trigger').css('marginTop', '1px');
	}

})();
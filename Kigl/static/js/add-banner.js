(function() {
	'use strict';

	$(function() {
		$('#datetimepicker1').datetimepicker({
			widgetPositioning: {
				horizontal: 'right',
				vertical  : 'bottom'
			}
		});

		$(".chosen-select").chosen({
			width: '100%'
		});
	});

	$(window).on('load resize', function() {
		if ($(window).width() >= 992) {
			/* Данная комбинация выравнивает высоты у блоков( сайдбар по отношению к контенту и формы между левой и правой). */
			var footerRightHeight = $('.footer__right.footer__box').outerHeight(true),
				rightHeightTop = $('.mainSec__right-content').outerHeight(true),
				formLeftHeight = $('.form__left-part').outerHeight(true);

			$('.footer__left.footer__box').css('height', footerRightHeight);

			$('.mainSec__left-content').css('height', footerRightHeight + rightHeightTop);

			$('.form__right-part').css('height', formLeftHeight - 39);
			/* 39 это значение нижнего маргина textarea левой формы */
		} else {
			$('.footer__left.footer__box').attr('style', '');

			$('.mainSec__left-content').attr('style', '');
		}
	});


})();



﻿'use strict';

var stripe;

function kiglStripeInit(_stripe){
    stripe = _stripe;
}

function registerElements(elements) {
    var formClass = '.payment';
    var example = document.querySelector(formClass);

    var form = example.querySelector('form');
    //var error = form.querySelector('.error');
    //var errorMessage = error.querySelector('.message');

    function enableInputs() {
        Array.prototype.forEach.call(
          form.querySelectorAll(
            "input[type='text'], input[type='email'], input[type='tel']"
          ),
          function (input) {
              input.removeAttribute('disabled');
          }
        );
    }

    function disableInputs() {
        Array.prototype.forEach.call(
          form.querySelectorAll(
            "input[type='text'], input[type='email'], input[type='tel']"
          ),
          function (input) {
              input.setAttribute('disabled', 'true');
          }
        );
    }

    function updateCardErrors() {
        var res = false;

        if ($('#card-number').hasClass('invalid')) {
            $('#card-number').parent().addClass('stripe_elemenet-invalid');
            res = true;
        } else {
            $('#card-number').parent().removeClass('stripe_elemenet-invalid');
        }

        if ($('#card-expiry').hasClass('invalid')) {
            $('#card-expiry').parent().addClass('stripe_elemenet-invalid');
            res = true;
        } else {
            $('#card-expiry').parent().removeClass('stripe_elemenet-invalid');
        }

        if ($('#card-cvc').hasClass('invalid')) {
            $('#card-cvc').parent().addClass('stripe_elemenet-invalid');
            res = true;
        } else {
            $('#card-cvc').parent().removeClass('stripe_elemenet-invalid');
        }

        return res;
    }

    function updateCardEmpty() {
        var res = false;

        if ($('#card-number').hasClass('empty') || $('#card-number').hasClass('invalid')) {
            $('#card-number').parent().addClass('stripe_elemenet-invalid');
            res = true;
        } else {
            $('#card-number').parent().removeClass('stripe_elemenet-invalid');
        }

        if ($('#card-expiry').hasClass('empty') || $('#card-expiry').hasClass('invalid')) {
            $('#card-expiry').parent().addClass('stripe_elemenet-invalid');
            res = true;
        } else {
            $('#card-expiry').parent().removeClass('stripe_elemenet-invalid');
        }

        if ($('#card-cvc').hasClass('empty') || $('#card-cvc').hasClass('invalid')) {
            $('#card-cvc').parent().addClass('stripe_elemenet-invalid');
            res = true;
        } else {
            $('#card-cvc').parent().removeClass('stripe_elemenet-invalid');
        }

        return res;
    }

    function updateCardHolderNameError() {
        var res = false;
        if ($('#card_holder_name').hasClass('empty')) {
            $('#card_holder_name').parent().addClass('stripe_elemenet-invalid');
            res = true;
        } else {
            $('#card_holder_name').parent().removeClass('stripe_elemenet-invalid');
        }

        return res;
    }

    function updateTermsError() {
        var res = false;
        if (!$('#terms').is(':checked')) {
            $('#terms').parent().removeClass('stripe_elemenet-valid');
            $('#terms').parent().addClass('stripe_elemenet-invalid');
            res = true;
        } else {
            $('#terms').parent().removeClass('stripe_elemenet-invalid');
            $('#terms').parent().addClass('stripe_elemenet-valid');
        }

        return res;
    }

    // Listen for errors from each Element, and show error messages in the UI.
    elements.forEach(function (element) {
        element.on('change', function (event) {
            if (event.error) {
                //error.classList.add('visible');
                //errorMessage.innerText = event.error.message;
            } else {
                //error.classList.remove('visible');
            }

            setTimeout(function () {
                updateCardErrors();
                if (postHeppened) {
                    updateCardEmpty();
                }
            }, 100);
        });
    });

    $('#card_holder_name').on('change keyup paste', function() {
        updateCardHolderNameError();
    });

    var postHeppened = false;

    // Listen on the form's 'submit' handler...
    form.addEventListener('submit', function (e) {
        e.preventDefault();

        // Show a loading screen...
        example.classList.add('submitting');

        // Disable all inputs.
        disableInputs();

        // Gather additional customer data we may have collected in our form.
        var name = form.querySelector('#card_holder_name');
        var additionalData = {
            name: name ? name.value : undefined,
        };

        //$('#terms').change(function () {
        //    if ($('#terms').hasClass('empty')) {
        //        $('#terms').addClass('stripe_elemenet-invalid');
        //    } else {
        //        $('#terms').removeClass('stripe_elemenet-invalid');
        //    }
        //});

        var isCardsErrors = updateCardErrors();

        var isCardHolderNameErrors = updateCardHolderNameError();

        var isCardEmpty = updateCardEmpty();

        var isCheckedTerms = updateTermsError();

        if (isCardsErrors || isCardHolderNameErrors || isCardEmpty || isCheckedTerms) {
            enableInputs();
            postHeppened = true;
            return;
        }
            
        
        // Use Stripe.js to create a token. We only need to pass in one Element
        // from the Element group in order to create a token. We can also pass
        // in the additional customer data we collected in our form.
        stripe.createToken(elements[0], additionalData).then(function (result) {
            // Stop loading!
            example.classList.remove('submitting');

            if (result.token) {
                // If we received a token, show the token ID.
                //example.querySelector('#CardToken').innerText = result.token.id;
                example.classList.add('submitted');
                $('#CardToken').val(result.token.id);
                $('#payment-form').submit();

            } else {
                // Otherwise, un-disable inputs.
                updateCardErrors();
                updateCardHolderNameError();
                updateCardEmpty();
                postHeppened = true;
                enableInputs();
            }
        });
    });

    $("#terms").click(function () {
        if (postHeppened) {
            updateTermsError();
        }
    });
}

// slider
$('.slider-campaign').slick({
    infinite: true,
    slidesToShow: 1,
    dots: false,
    arrows: true
});
$('.slider-campaign__total-slides').text($(".slider-campaign").slick("getSlick").slideCount);
$('.slider-campaign').on('swipe', function () {
    sliderNav();
})
$('.slider-campaign .slick-prev').click(function (e) {
    sliderNav();
});

$('.slider-campaign .slick-next').click(function (e) {
    sliderNav();
});

// details modal
$('.instagrammer-choosing .fakeTable__box_body .details').click(function () {
   $('.details-modal').fadeIn();
   $('.modal__opacity').fadeIn();
});
$('.details-modal__close').click(function () {
    $('.details-modal').fadeOut();
    $('.modal__opacity').fadeOut();
});

function sliderNav() {
    var itemService = $('.slider-campaign .slider-campaign__slide.slick-active').attr('data-slick-index');
    itemService = parseInt(itemService);
    itemService = itemService + 1;
    $('.slider-campaign__current-slide').text(itemService);
}

// change navigation menu class Active
$('.nav-area__tabs li').on('click', function (event) {
    event.preventDefault();
    var tab_id = $(this).attr('data-tab');
    $('.nav-area__tabs li').removeClass("active");
    $('.nav-area__content').removeClass('active');
    $(this).addClass("active");
    $("#" + tab_id).addClass('active');
});


// copy identity when clicked on span

$(".copy_data_identy").on('click', function () {
    document.execCommand("copy");
});
// copy identity when clicked on button
$(".copy_data_identy_button").on('click', function () {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(this).parent().find("span").text()).select();
    document.execCommand("copy");
    $temp.remove();
});

$(".en_btn").on('click', function () {
    var $temp = $("<input>");
    $("body").append($temp);

    var selectLink = "";
    if ($("#newCopyInstagrammer").is(":visible")) {
        selectLink = $('#newCopyInstagrammer').val();
    } else {
        selectLink = $('#newCopyAdvertiser').val();
    }

    //var selectLink = $('#newCopy').val();
    // $temp.val(selectLink).select();
    // document.execCommand("copy");
    // $temp.remove();
    // show message, what link was be copied
    $('.en_btn').text('Copying');

    setTimeout(function () {
        $('.en_btn').text('Copied');
        //$('#newCopy').addClass('newInputFriend-copied');
    }, 1500);
});

$(".ru_btn").on('click', function () {
    var $temp = $("<input>");
    $("body").append($temp);

    var selectLink = "";
    if ($("#newCopyInstagrammer").is(":visible")) {
        selectLink = $('#newCopyInstagrammer').val();
    } else {
        selectLink = $('#newCopyAdvertiser').val();
    }

    //var selectLink = $('#newCopy').val();
    $temp.val(selectLink).select();
    document.execCommand("copy");
    $temp.remove();
    // show message, what link was be copied
    $('.ru_btn').text('Копируется');
    setTimeout(function () {
        $('.ru_btn').text('Скопировано');
    }, 1500);
});

$('.referral').click(function () {
    setTimeout(function () {
        $('.en_btn').text('Copy');
        $('.ru_btn').text('Копировать');
    }, 1500);
});

// Scroll top modal PayPal when focus on input
$('#newPhone').focus(function () {
    if ($(window).width() <= 760) {
        var position = $('.modal-dialog').height() + $(document).height();

        $('#js__modal-16').animate({ scrollTop: position }, 400);

    }
});

// Scroll top modal PayPal when focus on input
$('#smsCode').focus(function () {
    if ($(window).width() <= 760) {
        var position = $('.modal-dialog').height() + $(window).height();
        $('#js__modal-16').animate({ scrollTop: position }, 1000);
    }

});
// Scroll top modal PayPal when focus on input
$('#newPaypal').focus(function () {
    if ($(window).width() <= 760) {
        var position = $('.modal-dialog').height() + $(window).height();
        $('#js__modal-17').animate({ scrollTop: position }, 1000);
    }
});

// Scroll top modal phoneArea
$('#paypalArea').focus(function () {
    if ($(window).width() <= 760) {

        var position = $(".module-paypal").offset().top;
        $('html, body').animate({ scrollTop: position }, 400);
    }

});
// Scroll top modal phoneArea
$('#bankArea').focus(function () {
    if ($(window).width() <= 760) {
        var position = $(".module-bank-number").offset().top;
        $('html, body').animate({ scrollTop: position }, 400);

    }

});
// Scroll top modal phoneArea
$('#creditCardArea').focus(function () {
    if ($(window).width() <= 760) {
        var position = $(".module-credit-card").offset().top;
        $('html, body').animate({ scrollTop: position }, 400);

    }

});
// Scroll top modal-4
$('#addUserAdForm input').each(function () {
    $(this).focus(function () {
        if ($(window).width() <= 760) {
            var position = $(".js__modal-5").offset().top;
            $('html, body').animate({ scrollTop: position }, 400);

        }
    })
});

// copy identity when clicked on span
$(".copy_data_identy").on('click', function () {
    document.execCommand("copy");

});
// copy identity when clicked on button
$(".copy_data_identy_button").on('click', function () {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(this).parent().find("span").text()).select();
    document.execCommand("copy");
    $temp.remove();
});
// change pagination class Active
$('.pagination__pages a').on('click', function (event) {
    $('.pagination__pages a').removeClass("active");
    $(this).addClass("active");
});

$('.instagrammer_mobile_change').click(function () {
    $('#newPhone').val($('.mob_numb').text());
    $('#js__modal-16').modal('show');
});
$('.instagrammer_paypal_change').click(function () {
    $('#newPaypal').val($('.paypal_akk').text());
    $('#js__modal-17').modal('show');
});

$('.earn-more__header--button').click(function () {
    $('#js__modal-18').modal('show');
});

$('#referralInstagramer').click(function () {
    $('#newCopyInstagrammer').show();
    $('#newCopyAdvertiser').hide();
    $('#advertiserFacebookShare').show();
    $('#instagramerFacebookShare').hide();
    $('#js__modal-18').modal('show');
});
$('#referralAdvertiser').click(function () {
    $('#newCopyInstagrammer').hide();
    $('#newCopyAdvertiser').show();
    $('#advertiserFacebookShare').hide();
    $('#instagramerFacebookShare').show();
    $('#js__modal-18').modal('show');
});



// Script for Earn-more slider
$(document).ready(function () {

    var countSlideElement = $('.earn-more__slider-item').length;
    var currentlyNumberSlide = $('.currently').attr('data-slide');
    var nextSlideElement = $('.earn-more_slide-next');
    $('.earn-more_slide-now').text(currentlyNumberSlide);
    $('.earn-more_slide-sum').text(countSlideElement);

    nextSlideElement.on('click', function (event) {

        event.preventDefault();
        var countSlideElement = $('.earn-more__slider-item').length;
        var numberSlide = $(this).parents()[2].attributes[2].value;
        var nextSlider = Number(numberSlide) + 1;

        if (numberSlide) {
            $('.earn-more_slide-now').text(nextSlider);
            $('#slide-' + numberSlide).removeClass('currently').addClass('visuality-hidden');
            $('#slide-' + nextSlider).removeClass('visuality-hidden').addClass('currently');
        }

        if (nextSlider > countSlideElement) {
            $('#slide-1').removeClass('visuality-hidden').addClass('currently');
            $('.earn-more_slide-now').text(1);
        }

    })
});

//$(function () {
//    var imageType = 4;
//    var videoType = 5;

//    var rowTemplate = $('.row-template');
//    rowTemplate.remove();
//    $('#slider').slick('unslick');
//    var sliderItemTemplate = $('.slider-image-item');
//    sliderItemTemplate.remove();
//    var sliderVideoItemTemplate = $('.slider-video-item');
//    sliderVideoItemTemplate.remove();
//    var previewTemplate = $('#jp_container_preview');
//    previewTemplate.remove();
//    var currentItem;
//    var items = [];

//    updateContent(true);

//    $('.btn-deliver').click(function (e) {

//        e.preventDefault();
//        var form = $('#addUserAdForm');
//        form.find('#adId').val(currentItem.id);
//        form.find('#deliveryMethod').val($(this).data('deliveryMethod'));
//        form.submit();
//    });

//    var verifyingAdId = null;
//    $(document).on('click', '.verify', function () {
//        verifyingAdId = $(this).closest('.fakeTable__row').data('id');
//        $('#js__modal-4').modal('show');
//    });
//    $('.btn-perform-verify').click(function (e) {
//        e.preventDefault();
//        $.ajax({
//            url: '/Content/Verify',
//            data: {
//                adId: verifyingAdId
//            },
//            success: function (result) {
//                $('#js__modal-4').modal('hide');
//                var status = result.data;
//                var verify = $('.fakeTable__row[data-id=' + verifyingAdId + ']').find('.verify');
//                if (status == 'InProgress')
//                    verify.siblings('.process').show();
//                else if (status == 'Failed')
//                    verify.siblings('.failed').show();
//                else if (status == 'CampaignStopped')
//                    verify.siblings('.stopped').show();
//                verifyingAdId = null;
//                verify.remove();
//                updateContent();
//            }
//        });
//    });

//    function updateContent(initial) {
//        $.ajax({
//            url: '/Content/GetContent',
//            success: function (data) {
//                items = data;
//                if (!initial && items.length > 0 && $('.verify').length == 0) $('#keepEarning').show();
//                rerenderSlider();
//                if (initial) {

//                    var target = $.cookie('chooseModule');
//                    if (target)
//                        changeModule(target);
//                    var index = $.cookie('choosedItem') || 0;
//                    setCurrentItem(index);
//                } else {
//                    setCurrentItem(0);
//                }
//            }
//        });
//    }

//    function rerenderSlider() {
//        $('#slider').slick('unslick');
//        $('#slider').empty();
//        if (items.length > 0) {
//            for (var i = 0; i < items.length; i++) {
//                var sliderItem;
//                if (items[i].adType == imageType) {
//                    sliderItem = sliderItemTemplate.clone();
//                    $('#slider').append(sliderItem);
//                    sliderItem.children('img').attr('src', items[i].fileUrl);
//                }
//                if (items[i].adType == videoType) {
//                    sliderItem = sliderVideoItemTemplate.clone();
//                    var containerId = 'jp_container_' + (i + 1);
//                    sliderItem.find('.jp-video').attr('id', containerId);
//                    var playerId = 'jquery_jplayer_' + (i + 1);
//                    sliderItem.find('.jp-jplayer').attr('id', playerId).data('itemId', i);
//                    $('#slider').append(sliderItem);

//                    $('#' + playerId).jPlayer({
//                        ready: function () {
//                            $(this).jPlayer("setMedia", {
//                                title: items[$(this).data('itemId')].name,
//                                m4v: items[$(this).data('itemId')].fileUrl,
//                            });
//                        },
//                        play: function () { // To avoid multiple jPlayers playing together.
//                            $(this).jPlayer("pauseOthers");

//                        },
//                        swfPath: "../../dist/jplayer",
//                        supplied: "webmv, ogv, m4v",
//                        cssSelectorAncestor: "#" + containerId,
//                        globalVolume: true,
//                        useStateClassSkin: true,
//                        autoBlur: false,
//                        smoothPlayBar: true,
//                        keyEnabled: true
//                    });
//                }
//            }
//        }
//    }

//    function setCurrentItem(index) {
//        currentItem = items[index];
//        $.cookie('choosedItem', index, { path: '/' });
//        if (currentItem) {
//            var iOS = !!navigator.userAgent.match(/iP(hone|od|ad)/i);;
//            $('.preview').empty();
//            if (currentItem.adType == imageType) {
//                if (iOS) {
//                    $('.btn-direct-download').show();
//                }
//                $('.preview').css('background-image', 'url(' + currentItem.fileUrl + ')');
//            }
//            if (currentItem.adType == videoType) {
//                if (iOS) {
//                    $('.btn-direct-download').hide();

//                }
//                $('.preview').css('background-image', '');
//                var video = previewTemplate.clone();
//                video.find('.jp-jplayer').data('itemId', index);
//                $('.preview').append(video);

//                $('#jquery_jplayer_preview').jPlayer({
//                    ready: function () {
//                        $(this).jPlayer("setMedia", {
//                            title: items[$(this).data('itemId')].name,
//                            m4v: items[$(this).data('itemId')].fileUrl,
//                        });
//                    },
//                    play: function () { // To avoid multiple jPlayers playing together.
//                        $(this).jPlayer("pauseOthers");

//                    },
//                    swfPath: "../../dist/jplayer",
//                    supplied: "webmv, ogv, m4v",
//                    cssSelectorAncestor: "#jp_container_preview",
//                    globalVolume: true,
//                    useStateClassSkin: true,
//                    autoBlur: false,
//                    smoothPlayBar: true,

//                    keyEnabled: true
//                });
//            }
//            $('.preview__text').text(currentItem.caption);
//        }
//    }

//    function changeModule(target) {
//        $('.module').removeClass('active');
//        history.pushState({
//            module: target

//        }, target);
//        $(target).addClass('active');
//        $.cookie('chooseModule', target, { path: '/' });
//        $(target).setEqualHeight();

//        if (target == '.module-2') {
//            $('#slider').slick({
//                arrows: false,
//                dots: true,
//                infinite: true,
//                adaptiveHeight: true
//            });
//        }

//    }

//    //$.post('/api/Data/GetFollowersInfo', {
//    //	instagramLogin: '123213'
//    //}, function(data) {
//    //	if (data) {
//    //		$('.followers-count').text(data.FollowedBy);
//    //		$('#followersCount').val(data.FollowedBy);
//    //		$('.followers-balance').html('$' + data.Balance + $('.followers-balance').html());
//    //	}
//    //});

//    $(document).on('click', '.failed', function (e) {
//        e.preventDefault();
//        var adId = $(this).closest('.fakeTable__row').data('id');
//        var self = $(this);
//        $.ajax({
//            url: '/Content/ResetVerifyInfo',
//            data: {
//                adId: adId
//            },
//            success: function () {
//                self.hide();
//                //self.parent().prepend($('<a href="#"/>').attr('class', 'fakeTable__btn verify').text('Verify again'));
//            }
//        });
//    });

//    $('.btn-create-request').click(function () {
//        var area = $(this).closest('.module').find('.account-area');
//        if (area.val()) {
//            $.ajax({
//                url: '/Content/CreateWithdrawRequest',
//                data: {
//                    account: area.val(),
//                    type: area.data('withdrawType')
//                },
//                success: function () {
//                    changeModule('.module-1');
//                }
//            });
//        } else {
//            area.css('border-color', 'red');
//        }
//    });

//    $('.module__btn,.btn-change-module').click(function (e) {
//        var target = $(this).data('target');
//        if (target) {
//            e.preventDefault();
//            changeModule(target);
//        }
//    });

//    $('#slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
//        setCurrentItem(nextSlide);
//    });

//    history.pushState({
//        module: '.module-1'
//    }, ".module-1");

//    window.onpopstate = function (event) {
//        if (event.state != null) {
//            $('.module').removeClass('active');
//            $(event.state.module).addClass('active');
//        }
//    };

//    $('.btn-show-save-text-modal').click(function () {
//        $('#js__modal-5').modal('show');
//    });
//});



//$('#select-type').on('change', function (event) {
//	var selectedValue = event.target.value;
//    $('.filter-page__reload').addClass('active');
//	$('.fakeTable__row__money').each(function(index) {
//		var dataRowType = $(this).attr('data-payment-type');
//		if(selectedValue !== dataRowType) {
//			$(this).addClass('visuality-hidden__area');
//		} else {
//			$(this).removeClass('visuality-hidden__area');
//		}
//		if(selectedValue == 0) {
//			$(this).removeClass('visuality-hidden__area');
//		}
//	});
//})

var configObject = {
    autoClose: false,
    format: 'DD.MM.YYYY',
    separator: ' - ',
    language: 'auto',
    startOfWeek: 'sunday',// or monday
    getValue: function () {
        return $(this).val();
    },
    setValue: function (s) {
        if (!$(this).attr('readonly') && !$(this).is(':disabled') && s != $(this).val()) {
            $(this).val(s);
        }
    },
    startDate: false,
    endDate: false,
    time: {
        enabled: false

    },
    minDays: 0,
    maxDays: 0,
    showShortcuts: false,
    shortcuts:
		{
		    //'prev-days': [1,3,5,7],
		    //'next-days': [3,5,7],
		    //'prev' : ['week','month','year'],
		    //'next' : ['week','month','year']
		},
    customShortcuts: [],
    inline: false,
    container: 'body',
    alwaysOpen: false,
    singleDate: false,
    lookBehind: false,
    batchMode: false,
    duration: 200,
    stickyMonths: false,
    dayDivAttrs: [],
    dayTdAttrs: [],
    applyBtnClass: '',
    singleMonth: true,

    hoveringTooltip: function (days, startTime, hoveringTime) {
        return days > 1 ? days + ' ' + lang('days') : '';

    },
    showTopbar: false,
    swapTime: false,
    selectForward: false,
    selectBackward: false,
    showWeekNumbers: false,
    getWeekNumber: function (date) //date will be the first day of a week

    {
        return moment(date).format('w');
    },
    monthSelect: false,
    yearSelect: false,
    language: 'en'
}

// Filter for period data
//$('.filter-page__button').on('click',function (event) {
//   event.stopPropagation();

//   $('#choose-dates').focus();
//   $('#choose-dates').dateRangePicker(configObject).click().bind('datepicker-closed',function(event)
//   {

//       /* This event will be triggered after date range picker close animation */
//       var getDatePeriod = $('#choose-dates').val();
//       var slicedDatePeriodFrom = getDatePeriod.slice(0,10);
//       var slicedDatePeriodTo = getDatePeriod.slice(13,);
//       $('.filter-page__reload').addClass('active');
//       slicedDatePeriodFrom = slicedDatePeriodFrom.split('.');
//       slicedDatePeriodTo = slicedDatePeriodTo.split('.');
//       slicedDatePeriodFrom = new Date(slicedDatePeriodFrom[2]+'-'+slicedDatePeriodFrom[1]+'-'+slicedDatePeriodFrom[0]).valueOf();
//       slicedDatePeriodTo = new Date(slicedDatePeriodTo[2]+'-'+slicedDatePeriodTo[1]+'-'+slicedDatePeriodTo[0]).valueOf();
//       $('.filter-page__button').text(getDatePeriod);
//       if(getDatePeriod) {
//           $('.fakeTable__row__money').each(function () {
//               var dataRowDate = $(this).attr('data-payment-date');
//               dataRowDate = dataRowDate.split('.');
//               dataRowDate =  new Date(dataRowDate[2]+'-'+dataRowDate[1]+'-'+dataRowDate[0]).valueOf();
//               if (dataRowDate > slicedDatePeriodFrom && dataRowDate < slicedDatePeriodTo) {

//                   $(this).removeClass('visuality-hidden__area');
//               } else {
//                   $(this).addClass('visuality-hidden__area');
//               }
//           });
//       }
//   });
//   $('.month-wrapper').append($( '<div class="choose-date-triangle"></div>' ));
//});

// Reload filter
$('.filter-page__reload').on('click', function () {
    $('.fakeTable__row__money').each(function () {
        $(this).removeClass('visuality-hidden__area');
    });
    $(this).removeClass('active');
    $('.filter-page__button').text('Выберите дату').append("<img src='../static/image/datetimepicker-right.png' alt='choose datepicker'>");
});

// Script for close tooltip on iOS application
$(document).on('click touchstart', function () {
    //$('.btn_friends_copy').text('Copy');
    //$('.btn_friends_copy_ru').text('Копировать');
})



(function() {
	'use strict';


	/* Work with input "url" page - "Portal images" */
	$(function() {

		var inputValues = [];

		$(document).on('click', '.portal-images .setup-block__edit, .portal-images .setup-block__ok, .portal-images .setup-block__cancel ', function() {

			var setupBlockInput = $('.setup-block__input'),
				thisInputPath = $(this).closest('form').find('input'),
				inputIndex = setupBlockInput.index(thisInputPath),
				inputIndexValue = setupBlockInput.eq(inputIndex).val();


			if ($(this).attr('data-btn') === 'edit') {

				inputValues[inputIndex] = inputIndexValue;

				$(this).parent().removeClass('active');
				$(this).parents('form').addClass('active');
				$(this).parents('form').find('.setup-block__btns').addClass('active');
				$(this).closest('form').find('input').addClass('active').prop('disabled', false);
			} else if ($(this).attr('data-btn') === 'ok' || $(this).attr('data-btn') === 'cancel') {
				$(this).parent().removeClass('active');
				$(this).parents('form').removeClass('active');
				$(this).parents('form').find('.setup-block__btn').addClass('active');
				$(this).closest('form').find('input').removeClass('active').prop('disabled', true);

				if ($(this).attr('data-btn') === 'cancel') {
					$(this).closest('form').find('input').val(inputValues[inputIndex]);
				}
			}
		});
	});

	/* Work with input(load image) */
	$(function() {

		$(document).on('click', '.portal-images .setup-block__select-image', function() {
			$(this).siblings('input').trigger('click');
		});
	});
})();
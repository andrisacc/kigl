(function() {
	'use strict';

	$(function() {

		$(document).on('click', '.approval-account__buttons .approve', function(e) {
			e.preventDefault();

			if($(this).hasClass('ok') === false){
				$(this).addClass('ok').removeClass('disable');
				$(this).siblings().addClass('disable').removeClass('cancel');
				$(this).closest('.approval-account').addClass('ok').removeClass('cancel');
			} else if($(this).hasClass('ok') === true) {
				$(this).removeClass('ok');
				$(this).siblings().removeClass('disable');
				$(this).closest('.approval-account').removeClass('ok');
			}

		});

		$(document).on('click', '.approval-account__buttons .disapprove', function(e) {
			e.preventDefault();

			if($(this).hasClass('cancel') === false){
				$(this).addClass('cancel').removeClass('disable');
				$(this).siblings().addClass('disable').removeClass('ok');
				$(this).closest('.approval-account').addClass('cancel').removeClass('ok');
			} else if($(this).hasClass('cancel') === true) {
				$(this).removeClass('cancel');
				$(this).siblings().removeClass('disable');
				$(this).closest('.approval-account').removeClass('cancel');
			}

		});
	});
})();
﻿using System.Web.Mvc;
using Common.DAL;
using Common.DAL.Contracts;
using ControlPanel.Domain.Contracts;
using ControlPanel.Services;
using Kigl.Domain.Contracts;
using Kigl.Dropbox;
using Kigl.Instagram;
using Kigl.Services;
using Kigl.Stripe;
using Microsoft.Practices.Unity;

namespace Kigl
{
    public class UnityMvcConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            container.RegisterType<IDataContext, DataContext>();

            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IContentService, ContentService>();
            container.RegisterType<IInstagramParserService, InstagramParserService>();
            container.RegisterType<ISettingsService, SettingsService>();
            container.RegisterType<IAdvertiserService, AdvertisersService>();
            container.RegisterType<IAccountsService, AccountsService>();
            container.RegisterType<IAdsService, AdsService>();
            container.RegisterType<IImageService, ImageService>();
            container.RegisterType<IRatesService, RatesService>();
            container.RegisterType<IStatisticService, StatisticService>();
            container.RegisterType<IApprovalsService, ApprovalsService>();
            container.RegisterType<INetworkSettingsService, NetworkSettingsService>();
            container.RegisterType<IDropboxService, DropboxService>();
            container.RegisterType<IEmailService, EmailService>();
            container.RegisterType<IReportService, ReportService>();
            container.RegisterType<IStripeService, StripeService>();
            container.RegisterType<IPaymentService, PaymentService>();

            DependencyResolver.SetResolver(new Unity.Mvc5.UnityDependencyResolver(container));
        }
    }
}
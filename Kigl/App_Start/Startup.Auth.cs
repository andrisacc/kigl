﻿using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

namespace Kigl
{
	public partial class Startup
	{
        public void ConfigureAuth(IAppBuilder app)
        {
#if DEBUG
            var cookiesName = "KiglAuthDebug";
#else
            var cookiesName = "KiglAuthProd";
#endif

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "ApplicationCookie",
                LoginPath = new PathString("/Account/Advertisers"),
                CookieName = cookiesName,
            });
        }
    }
}
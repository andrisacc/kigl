﻿using System.Linq;
using AutoMapper;
using ControlPanel.Common.Enums;
using ControlPanel.Domain.Models;
using ControlPanel.Domain.Models.Statistic;
using Kigl.Models;
using Entities = Common.DAL.Entities;
namespace Kigl
{
    public class MapConfig
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Entities.Country, Country>()
                    .ForMember(item => item.Networks, expression => expression.ExplicitExpansion())
                    .MaxDepth(3);
                cfg.CreateMap<Entities.Currency, Currency>();
                cfg.CreateMap<Entities.City, City>();
                cfg.CreateMap<Entities.Network, Network>();
                cfg.CreateMap<Entities.Network, NetworkSettings>();
                cfg.CreateMap<Entities.Advertiser, Advertiser>()
                    .ForMember(item => item.NetworkIds, expression => expression.ExplicitExpansion());
                cfg.CreateMap<Entities.Account, Account>();
                cfg.CreateMap<Entities.User, User>()
                    .ForMember(item => item.UserName, expression => expression.MapFrom(item => item.Name ?? item.Username))
                    .ForMember(item => item.InstagramCategories, expression => expression.MapFrom(item => item.InstagramCategories.Select(category => category.InstagramCategory.Name)))
                    .ForMember(item => item.FollowersCnt, expression => expression.MapFrom(item => item.FollowersCount))
                    ;
                cfg.CreateMap<Entities.Ad, Ad>()
                    .ForMember(item => item.FileName, expression => expression.MapFrom(item => item.Files.FirstOrDefault(file => file.FileAdType == FileAdType.Large).FileName))
                    .ForMember(item => item.Files, expression => expression.ExplicitExpansion())
                    .ForMember(item => item.NetworkIds, expression => expression.ExplicitExpansion());
                cfg.CreateMap<Entities.Ad, Statistic>()
                    .ForMember(item => item.Files, expression => expression.ExplicitExpansion());
                cfg.CreateMap<Entities.File, File>().MaxDepth(3);
                cfg.CreateMap<Entities.Rates, Rates>();
                cfg.CreateMap<Entities.PortalImage, PortalImage>();
                cfg.CreateMap<Entities.InstagramCategory, InstagramCategory>();

                cfg.CreateMap<Entities.UserAd, UserAd>()
                    .ForMember(item => item.Name, expression => expression.MapFrom(item => item.User.Name));

                cfg.CreateMap<Account, EditAccountModel>();
                cfg.CreateMap<Rates, EditRatesModel>();
                cfg.CreateMap<Ad, EditAdModel>();
                cfg.CreateMap<Advertiser, EditAdvertiserModel>();

                cfg.CreateMap<Account, Entities.Account>()
                    .ForMember(item => item.Currency, expression => expression.Ignore())
                    .ForMember(item => item.Advertiser, expression => expression.Ignore());
                cfg.CreateMap<Advertiser, Entities.Advertiser>();
                cfg.CreateMap<Ad, Entities.Ad>()
                    .ForMember(item => item.StartDate, expression => expression.MapFrom(item => item.StartDateValue))
                    .ForMember(item => item.EndDate, expression => expression.MapFrom(item => item.EndDateValue));
                cfg.CreateMap<File, Entities.File>();
                cfg.CreateMap<Rates, Entities.Rates>();
                cfg.CreateMap<City, Entities.City>()
                    .ForMember(item => item.Country, expression => expression.Ignore());
                cfg.CreateMap<Network, Entities.Network>()
                    .ForMember(item => item.City, expression => expression.Ignore());
                cfg.CreateMap<PortalImage, Entities.PortalImage>();
            });
        }
    }
}
﻿using System.Configuration;
using System.Web.Http;
using Common.DAL;
using Common.DAL.Contracts;
using ControlPanel.Domain.Contracts;
using ControlPanel.Services;
using Kigl.Domain.Contracts;
using Kigl.Instagram;
using Kigl.Nexmo;
using Microsoft.Practices.Unity;
using Unity.WebApi;
using Kigl.Services;

namespace Kigl
{
    public class UnityWebApiConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            container.RegisterType<IDataContext, DataContext>();

            container.RegisterType<IInstagramParserService, InstagramParserService>();
            if (bool.Parse(ConfigurationManager.AppSettings["UseRealNexmo"]))
                container.RegisterType<INexmoService, NexmoService>();
            else
                container.RegisterType<INexmoService, NexmoTestService>();
            container.RegisterType<IDataService, DataService>();
            container.RegisterType<ISettingsService, SettingsService>();
            container.RegisterType<IUserService, UserService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}
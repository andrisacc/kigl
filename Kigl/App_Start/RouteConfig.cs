﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Kigl
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Account",
                "Account/{action}",
                new { controller = "Account", action = "Index" }
            );

            routes.MapRoute(
                "Ads",
                "Ads/{action}",
                new { controller = "Ads", action = "Index" }
            );

            routes.MapRoute(
                "Approvals",
                "Approvals/{action}",
                new { controller = "Approvals", action = "Index" }
            );

            routes.MapRoute(
                "BankAccounts",
                "BankAccounts/{action}",
                new { controller = "BankAccounts", action = "Index" }
            );

            routes.MapRoute(
                "Company",
                "Company/{action}",
                new { controller = "Company", action = "Index" }
            );

            routes.MapRoute(
                "Content",
                "Content/{action}",
                new { controller = "Content", action = "Index" }
            );

            routes.MapRoute(
                "Files",
                "Files/{action}",
                new { controller = "Files", action = "Index" }
            );

            routes.MapRoute(
                "Home",
                "Home/{action}",
                new { controller = "Home", action = "Index" }
            );

            routes.MapRoute(
                "NetworkSettings",
                "NetworkSettings/{action}",
                new { controller = "NetworkSettings", action = "Index" }
            );

            routes.MapRoute(
                "Rates",
                "Rates/{action}",
                new { controller = "Rates", action = "Index" }
            );

            routes.MapRoute(
                "Settings",
                "Settings/{action}",
                new { controller = "Settings", action = "Index" }
            );

            routes.MapRoute(
                "Statistic",
                "Statistic/{action}",
                new { controller = "Statistic", action = "Index" }
            );

            routes.MapRoute(
                "Users",
                "Users/{action}",
                new { controller = "Users", action = "Index" }
            );

            routes.MapRoute(
                "Info",
                "Info/{action}",
                new { controller = "Info", action = "Index" }
            );

            routes.MapRoute(
                "Advertiser",
                "Advertiser/{action}",
                new { controller = "Advertiser", action = "Index" }
            );

            routes.MapRoute(
                "Stripe",
                "Stripe/{action}",
                new { controller = "Stripe", action = "WebHook" }
            );

            routes.MapRoute(
                "RefAdv",
                "{lang}/RefAdv/{id}",
                new { controller = "RefAdv", action = "Index" }
            );

            routes.MapRoute(
                "RefInst",
                "{lang}/RefInst/{id}",
                new { controller = "RefInst", action = "Index" }
            );

            routes.MapRoute(
                "Localization",
                "{lang}/{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}

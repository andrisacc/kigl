﻿using System;
using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class EditAdModel : Ad
    {
        public Country[] Countries { get; set; }
        public InstagramCategory[] Categories { get; set; }
        public Guid? NetworkId { get; set; }
    }
}
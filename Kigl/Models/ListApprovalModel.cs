﻿using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class ListApprovalModel
    {
        public Approval[] Approvals { get; set; }
    }
}
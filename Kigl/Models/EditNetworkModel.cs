﻿using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class EditNetworkModel : Network
    {
        public City[] Cities { get; set; }
    }
}
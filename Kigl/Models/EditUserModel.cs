﻿using System;
using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class EditUserModel : User
    {
        public string RoleName { get; set; }
        public Guid[] InstagramCategories { get; set; }
    }
}
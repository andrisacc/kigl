﻿using System;
using ControlPanel.Domain.Models;
using Kigl.Common.Enums;

namespace Kigl.Models
{
    public class UserAdModel : Ad
    {
        public VerifyStatus VerifyStatus { get; set; }
        public Decimal UserAmount { get; set; }
    }
}
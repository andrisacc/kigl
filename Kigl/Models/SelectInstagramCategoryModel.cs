﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class SelectInstagramCategoryModel : InstagramCategory
    {
        public bool Selected { get; set; }
    }
}
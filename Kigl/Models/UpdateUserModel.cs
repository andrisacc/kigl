﻿namespace Kigl.Models
{
    public class UpdateUserModel
    {
        public string InstagramLogin { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Terms { get; set; }
    }
}
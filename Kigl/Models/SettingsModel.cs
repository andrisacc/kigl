﻿using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class SettingsModel
    {
        public Country[] Countries { get; set; }
        public City[] Cities { get; set; }
        public Currency[] Currencies { get; set; }
        public Network[] Networks { get; set; }
    }
}
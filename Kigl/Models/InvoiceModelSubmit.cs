﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kigl.Models
{
    public class InvoiceModelSubmit
    {
        public string Amount { get; set; }

        public string CardToken { get; set; }
    }
}
﻿using System;
using ControlPanel.Domain.Models;
using Kigl.Common.Enums;

namespace Kigl.Models
{
    public class RecoveryEnterPasswordModel
    {
        public String NewForgotPass { get; set; }

        public String RepeatNewForgotPass { get; set; }

        public Guid UserId { get; set; }
    }
}
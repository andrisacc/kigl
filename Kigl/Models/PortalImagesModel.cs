﻿using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class PortalImagesModel
    {
        public PortalImage Header { get; set; }
        public PortalImage Footer { get; set; }
    }
}
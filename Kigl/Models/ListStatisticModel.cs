﻿using ControlPanel.Domain.Models.Statistic;

namespace Kigl.Models
{
    public class ListStatisticModel
    {
        public string AdvertiserBrandName { get; set; }
        public decimal AccountBalance { get; set; }
        public bool IsApproved { get; set; }
        public InstagramStatistic[] InstagramStatistics { get; set; }
        public CountryStatistic[] Statistics { get; set; }
        public decimal Total { get; set; }
    }
}
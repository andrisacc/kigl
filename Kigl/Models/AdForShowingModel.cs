﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class AdForShowingModel : Ad
    {
        public string FileUrl { get; set; }
        public string ThumbnailUrl { get; set; }
        public string DownloadUrl { get; set; }
        public string[] CategoryNames { get; set; }
    }
}
﻿namespace Kigl.Models
{
    public class LoginRegistrationUserModel
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public string Email { get; set; }
        public string BrandName { get; set; }
        public string ContactPerson { get; set; }
        public string AccountLink { get; set; }

        public string LoginErrorMessage { get; set; }
        public string SignUpErrorMessage { get; set; }

        public bool IsSigningUp { get; set; }
    }
}
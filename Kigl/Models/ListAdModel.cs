﻿using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class ListAdModel
    {
        public Ad[] Banners { get; set; }
        public Ad[] Videos { get; set; }
    }
}
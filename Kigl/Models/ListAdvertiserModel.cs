﻿using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class ListAdvertiserModel
    {
        public Advertiser[] Advertisers { get; set; }
    }
}
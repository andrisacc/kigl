﻿using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class ListAccountModel
    {
        public Account[] Accounts { get; set; }
        public Currency[] Currencies { get; set; }
        public Advertiser[] Advertisers { get; set; }
    }
}
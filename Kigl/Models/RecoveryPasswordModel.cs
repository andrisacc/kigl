﻿using System;
using ControlPanel.Domain.Models;
using Kigl.Common.Enums;

namespace Kigl.Models
{
    public class RecoveryPasswordModel
    {
        public String ForgotEmail { get; set; }
    }
}
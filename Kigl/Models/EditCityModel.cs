﻿using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class EditCityModel : City
    {
        public Country[] Countries { get; set; }
    }
}
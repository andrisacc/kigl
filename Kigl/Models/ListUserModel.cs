﻿using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class ListUserModel
    {
        public User[] Users { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsAdvertiser { get; set; }
        public bool IsInstagramAdmin { get; set; }
        public Advertiser[] Advertisers { get; set; }
        public Network[] Networks { get; set; }
    }
}
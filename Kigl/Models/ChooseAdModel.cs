﻿using ControlPanel.Domain.Models;

namespace Kigl.Models
{
    public class ChooseAdModel
    {
        public bool UserHasCategories { get; set; }
        public bool HasAdForShowing { get; set; }
        public bool HasWithdraws { get; set; }
        public string DownloadUrl { get; set; }
        public string InstagramerName { get; set; }
        public string InstagramerUserName { get; set; }
        public string InstagramerEmail { get; set; }
        public string InstagramerPhoneNumber { get; set; }
        public string InstagramerPayPal { get; set; }
        public string AdvertiserReferralLink { get; set; }
        public string InstagramerReferralLink { get; set; }
        public UserAdModel[] UserAds { get; set; }
    }
}
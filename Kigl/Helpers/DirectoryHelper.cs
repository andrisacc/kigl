﻿using System;
using System.IO;
using System.Web.Hosting;
using ControlPanel.Common.Enums;

namespace Kigl.Helpers
{
    public class DirectoryHelper
    {
        private static string CreateIfNotExists(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return path;
        }

        private static string FilesDirectory => CreateIfNotExists(HostingEnvironment.MapPath("~/App_Data/Files"));

        public static string BannersDirectory => CreateIfNotExists(Path.Combine(FilesDirectory, "Banners"));

        public static string VideosDirectory => CreateIfNotExists(Path.Combine(FilesDirectory, "Videos"));

        public static string PortalImagesDirectory => CreateIfNotExists(Path.Combine(FilesDirectory, "PortalImages"));

        public static string GetAdDirectory(AdType adType)
        {
            switch (adType)
            {
                case AdType.Banner:
                case AdType.HeaderImage:
                case AdType.FooterImage:
                case AdType.InstagramImage:
                    return DirectoryHelper.BannersDirectory;
                case AdType.Video:
                case AdType.InstagramVideo:
                    return DirectoryHelper.VideosDirectory;
                default:
                    throw new ArgumentException("Unknown ad type", "adType");
            }
        }
    }
}
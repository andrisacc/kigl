﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using ControlPanel.Common.Enums;

namespace Kigl.Helpers
{
    public static class SwitchLanguageHelper
    {
        public class Language
        {
            public string Url { get; set; }
            public string ActionName { get; set; }
            public string ControllerName { get; set; }
            public RouteValueDictionary RouteValues { get; set; }
            public bool IsSelected { get; set; }

            public MvcHtmlString HtmlSafeUrl => MvcHtmlString.Create(Url);
        }

        public static Language LanguageUrl(this HtmlHelper helper, string cultureName,
            string languageRouteName = Constants.Lang, bool strictSelected = false)
        {

            cultureName = cultureName.ToLower();
            var indexOf = cultureName.IndexOf('-');
            if (indexOf > 0)
                cultureName = cultureName.Substring(0, indexOf);
            var routeValues = new RouteValueDictionary(helper.ViewContext.RouteData.Values);
            var queryString = helper.ViewContext.HttpContext.Request.QueryString;
            foreach (string key in queryString)
            {
                if (queryString[key] != null && !string.IsNullOrWhiteSpace(key))
                {
                    if (routeValues.ContainsKey(key))
                    {
                        routeValues[key] = queryString[key];
                    }
                    else
                    {
                        routeValues.Add(key, queryString[key]);
                    }
                }
            }
            var actionName = routeValues["action"].ToString();
            var controllerName = routeValues["controller"].ToString();
            routeValues[languageRouteName] = cultureName;
            var urlHelper = new UrlHelper(helper.ViewContext.RequestContext, helper.RouteCollection);
            var url = urlHelper.RouteUrl("Localization", routeValues);
            var currentLangName = Thread.CurrentThread.CurrentUICulture.Name.ToLower();
            var isSelected = strictSelected ? currentLangName == cultureName : currentLangName.StartsWith(cultureName);
            return new Language
            {
                Url = url,
                ActionName = actionName,
                ControllerName = controllerName,
                RouteValues = routeValues,
                IsSelected = isSelected
            };
        }

        public static MvcHtmlString LanguageSelectorLink(this HtmlHelper helper,
            string cultureName, string selectedText, string unselectedText,
            IDictionary<string, object> htmlAttributes, string languageRouteName = Constants.Lang, bool strictSelected = false)
        {
            var language = helper.LanguageUrl(cultureName, languageRouteName, strictSelected);
            var link = helper.RouteLink(language.IsSelected ? selectedText : unselectedText,
                "Localization", language.RouteValues, htmlAttributes);
            return link;
        }
    }
}
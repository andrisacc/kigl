﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using ControlPanel.Common.Enums;

namespace Kigl.Helpers
{
    public class GetLanguageHelper
    {
        public static string GetLanguage(RouteData routeData, HttpRequestBase request)
        {
            string result;
            if (!string.IsNullOrWhiteSpace(routeData.Values[Constants.Lang]?.ToString()) && routeData.Values[Constants.Lang]?.ToString().ToLower() != "referral")
            {
                result = routeData.Values[Constants.Lang].ToString();
            }
            else
            {
                var cookie = request.Cookies[Constants.CurrentCulture];
                var requestLanguage = request.UserLanguages != null && request.UserLanguages.Any()
                    ? request.UserLanguages[0]
                    : null;
                result = cookie != null ? cookie.Value : requestLanguage;
            }

            return String.IsNullOrEmpty(result)?"":result;
        }
    }
}
﻿$(function() {
    $(".select-lang").on('click', function (e) {
        $(".lang-list").toggleClass('active');
    });
    $(".lang-list span").on('click', function (e) {
        var txt = $(this).text();
        $(".select-lang").text(txt);
        $(".lang-list").removeClass('active');
    });
});
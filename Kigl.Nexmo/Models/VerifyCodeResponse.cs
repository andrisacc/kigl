﻿namespace Kigl.Nexmo.Models
{
    public class VerifyCodeResponse : BaseResponse
    {
        public string ErrorText { get; set; }
    }
}

﻿namespace Kigl.Nexmo.Models
{
    public class SendCodeResponse : BaseResponse
    {
        public string RequestId { get; set; }        
    }
}

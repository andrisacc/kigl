﻿using Kigl.Domain.Contracts;
using Kigl.Domain.Models.Nexmo;

namespace Kigl.Nexmo
{
    public class NexmoTestService : INexmoService
    {
        private string _requestId = "d3564954-9d1d-40cd-936a-3d45e1faa845";
        public string SendCode(string number)
        {
            return _requestId;
        }

        public VerifyResult VerifyCode(string requestId, string code)
        {
            if (code == "1111" && requestId == _requestId)
                return new VerifyResult
                {
                    Success = true
                };
            return new VerifyResult
            {
                Success = false,
                ErrorMessage = "Invalid code"
            };
        }
    }
}

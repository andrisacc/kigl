﻿using System.Net;
using Common.Logging;
using Kigl.Common.Helpers;
using Kigl.Domain.Contracts;
using Kigl.Domain.Models.Nexmo;
using Kigl.Nexmo.Models;
using Newtonsoft.Json;

namespace Kigl.Nexmo
{
    public class NexmoService : INexmoService
    {
        private static readonly ILog Log = LogManager.GetLogger<NexmoService>();

        private const string ApiBaseUrl = "https://api.nexmo.com/verify";

        //var json = "{\"request_id\":\"783416b9b07449079d4c0c422a590eab\",\"status\":\"0\"}";
        public string SendCode(string number)
        {
            using (var client = new WebClient())
            {
                Log.Info($"Sending code to number {number}");

                var settings = NexmoSection.GetSection();
                var json = client.DownloadString($"{ApiBaseUrl}/json?api_key={settings.ApiKey}&api_secret={settings.ApiSecret}&number={number}&brand={settings.Brand}");

                var response = JsonConvert.DeserializeObject<SendCodeResponse>(json, new JsonSerializerSettings
                {
                    ContractResolver = new UnderscorePropertyNamesContractResolver()
                });

                Log.Info($"Sending code response: requestId = {response.RequestId}, status = {response.Status}");
                return response.RequestId;
            }
        }

        //var json = "{\"status\":\"0\",\"error_text\":\"Invalid code\"}";
        public VerifyResult VerifyCode(string requestId, string code)
        {
            var result = new VerifyResult();
            using (var client = new WebClient())
            {
                var settings = NexmoSection.GetSection();

                Log.Info($"Verifying code: requestId = {requestId}, code = {code}");

                var json = client.DownloadString($"{ApiBaseUrl}/check/json?api_key={settings.ApiKey}&api_secret={settings.ApiSecret}&request_id={requestId}&code={code}");

                var response = JsonConvert.DeserializeObject<VerifyCodeResponse>(json, new JsonSerializerSettings
                {
                    ContractResolver = new UnderscorePropertyNamesContractResolver()
                });

                Log.Info($"Verifying code status: {response.Status}");
                if (!string.IsNullOrWhiteSpace(response.ErrorText))
                {
                    Log.Info($"Verifying code error: {response.ErrorText}");
                }

                if (response.Status == 0)
                    result.Success = true;
                else
                    result.ErrorMessage = response.ErrorText;
            }
            return result;
        }
    }
}

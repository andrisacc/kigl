﻿using System;
using System.Configuration;

namespace Kigl.Nexmo
{
    public class NexmoSection : ConfigurationSection
    {
        private const string SectionName = "NexmoSettings";

        private const string ApiKeyField = "ApiKey";
        private const string ApiSecretField = "ApiSecret";
        private const string BrandField = "Brand";

        public static NexmoSection GetSection()
        {
            var section = ConfigurationManager.GetSection(SectionName);
            if (section == null)
            {
                throw new ArgumentException($"No section {SectionName} found in config");
            }
            return (NexmoSection)section;
        }

        [ConfigurationProperty(ApiKeyField, IsRequired = true)]
        public string ApiKey
        {
            get { return this[ApiKeyField].ToString(); }
            set { this[ApiKeyField] = value; }
        }

        [ConfigurationProperty(ApiSecretField, IsRequired = true)]
        public string ApiSecret
        {
            get { return this[ApiSecretField].ToString(); }
            set { this[ApiSecretField] = value; }
        }

        [ConfigurationProperty(BrandField, IsRequired = true)]
        public string Brand
        {
            get { return this[BrandField].ToString(); }
            set { this[BrandField] = value; }
        }
    }
}
